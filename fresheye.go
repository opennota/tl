// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"fmt"
	"html"
	"html/template"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"unicode/utf8"

	"gitlab.com/opennota/fresheye"
)

type WordDescriptor struct {
	findex     int
	start, end int
}

type WordIterator struct {
	fragments          []Fragment
	text               string
	wordStart, wordEnd int
	prevEnd            int
	newParagraph       bool

	findex, cindex int
}

func russianLetter(r rune) bool {
	return r >= 'а' && r <= 'я' || r >= 'А' && r <= 'Я' || r == 'ё' || r == 'Ё'
}

func nextWord(s string, i int) (int, int) {
	start, end := -1, -1
	for i < len(s) {
		r, size := utf8.DecodeRuneInString(s[i:])
		if russianLetter(r) {
			start, end = i, i+size
			break
		}
		i += size
	}
	if start == -1 {
		return start, end
	}
	for i < len(s) {
		r, size := utf8.DecodeRuneInString(s[i:])
		i += size
		if !russianLetter(r) {
			break
		}
		end = i
	}
	return start, end
}

func (w *WordDescriptor) Start() int { return w.start }
func (w *WordDescriptor) End() int   { return w.end }

func NewWordIterator(fragments []Fragment) *WordIterator {
	return &WordIterator{
		fragments: fragments,
	}
}

func (w *WordIterator) Next() bool {
	findex, i, fragments := w.findex, w.cindex, w.fragments
	w.newParagraph = findex == 0 && i == 0
	var text string
	var start, end int
	for findex < len(fragments) {
		if len(fragments[findex].Versions) > 0 {
			text = fragments[findex].Versions[0].Text
			start, end = nextWord(text, i)
			if start >= 0 {
				break
			}
		}
		findex++
		w.newParagraph = true
		i = 0
	}
	w.findex = findex
	if start == -1 {
		w.text = ""
		return false
	}
	w.wordStart, w.wordEnd = start, end
	if w.newParagraph {
		w.prevEnd = 0
	} else {
		w.prevEnd = w.cindex
	}
	w.cindex = end
	w.text = text
	return true
}

func (w *WordIterator) Word() string         { return w.text[w.wordStart:w.wordEnd] }
func (w *WordIterator) Sep() string          { return w.text[w.prevEnd:w.wordStart] }
func (w *WordIterator) ParagraphStart() bool { return w.newParagraph }

func (w *WordIterator) Descriptor() fresheye.WordDescriptor {
	return &WordDescriptor{w.findex, w.wordStart, w.wordEnd}
}

func fresheyeRender(fragments []Fragment) func(int) template.HTML {
	it := NewWordIterator(fragments)
	bad := fresheye.Check(it, fresheye.ContextSize(20), fresheye.SensitivityThreshold(400))
	colorizer := fresheye.NewColorizer(400)
	colorizer.Colorize(bad)
	descriptors := fresheye.WordDescriptors(bad)
	sort.Slice(descriptors, func(i, j int) bool {
		d1 := descriptors[i].(*WordDescriptor)
		d2 := descriptors[j].(*WordDescriptor)
		if d1.findex != d2.findex {
			return d1.findex < d2.findex
		}
		return d1.start < d2.start
	})
	i := 0
	return func(findex int) template.HTML {
		if len(fragments[findex].Versions) == 0 {
			return ""
		}
		s := fragments[findex].Versions[0].Text
		j := 0
		var buf strings.Builder
		for i < len(descriptors) {
			d := descriptors[i].(*WordDescriptor)
			if d.findex > findex {
				break
			}
			if d.findex < findex {
				continue
			}

			c := colorizer.Color(d)
			from, to := d.start, d.end
			buf.WriteString(html.EscapeString(s[j:from]))
			buf.WriteString(fmt.Sprintf(`<span style="background-color: #%02x%02x%02x">`, c.R, c.G, c.B))
			buf.WriteString(s[from:to])
			buf.WriteString("</span>")
			j = to

			i++
		}
		buf.WriteString(html.EscapeString(s[j:]))
		s = nl2br.Replace(buf.String())
		s = insertSoftBreaks(s)
		return template.HTML(s) //nolint:gosec
	}
}

func (a *App) FreshEye(w http.ResponseWriter, r *http.Request) {
	type params struct {
		ContextSize          int
		SensitivityThreshold int
		ExcludeProperNames   bool
		Text                 string
	}
	if r.Method == "GET" {
		if err := fresheyeTmpl.Execute(w, params{
			ContextSize:          20,
			SensitivityThreshold: 400,
			ExcludeProperNames:   false,
		}); err != nil {
			logError(err)
		}
		return
	}

	err := r.ParseMultipartForm(1 * 1024 * 1024)
	if err != nil {
		internalError(w, err)
		return
	}

	contextSize, _ := strconv.Atoi(r.PostFormValue("contextSize"))
	if contextSize < 1 {
		contextSize = 1
	} else if contextSize > 50 {
		contextSize = 50
	}
	threshold, _ := strconv.Atoi(r.PostFormValue("sensitivityThreshold"))
	if threshold < 100 {
		threshold = 100
	} else if threshold > 1000 {
		threshold = 1000
	}
	excProper := r.PostFormValue("excludeProperNames") != ""
	text := r.PostFormValue("text")
	html := fresheye.ToColoredHTML(text, contextSize, threshold, excProper)

	if err := fresheyeTmpl.Execute(w, struct {
		params
		Text string
		HTML template.HTML
	}{
		params{
			ContextSize:          contextSize,
			SensitivityThreshold: threshold,
			ExcludeProperNames:   excProper,
		},
		text,
		template.HTML(html), //nolint:gosec
	}); err != nil {
		logError(err)
	}
}
