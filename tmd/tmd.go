// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"bufio"
	"bytes"
	"encoding/gob"
	"encoding/json"
	"flag"
	"html"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"
	"unicode"
	"unicode/utf8"

	"gitlab.com/opennota/csv"
	"gitlab.com/opennota/fasthash"
	"gitlab.com/opennota/substring"
	"gitlab.com/opennota/tl/diff"
	"gitlab.com/opennota/tl/fuzzy/inverted"
	"gitlab.com/opennota/tl/fuzzy/levenshtein"
	"gitlab.com/opennota/tl/fuzzy/segmenter"
	"gitlab.com/opennota/tl/fuzzy/tokenizer"
)

type docID = inverted.DocID

type server struct {
	Index     inverted.Index
	Filenames []string
	ID2D      map[docID]dd
}

type dd struct {
	Offset    int64
	FileIndex uint32
	NumTokens uint8
}

var (
	funcs = template.FuncMap{
		"renderWithRegexp": renderWithRegexp,
	}
	concordanceTmpl = template.Must(template.New("").Funcs(funcs).Parse(concordanceHTML))
	rBigWords       = regexp.MustCompile(`[^\s<>&;]{32,}`)
	r16Chars        = regexp.MustCompile(`.{16}`)
	nl2br           = strings.NewReplacer("\n", "<br>\n")
)

func insertSoftBreaks(s string) string {
	return rBigWords.ReplaceAllStringFunc(s, func(s string) string {
		return r16Chars.ReplaceAllStringFunc(s, func(s string) string {
			return s + "<wbr>"
		})
	})
}

func hashRow(row []string, buf []byte) (uint64, []byte) {
	buf = buf[:0]
	rbuf := make([]byte, 4)
	for i, s := range row {
		for _, r := range s {
			if unicode.IsLetter(r) {
				n := utf8.EncodeRune(rbuf, unicode.ToLower(r))
				buf = append(buf, rbuf[:n]...)
			} else if len(buf) > 0 && buf[len(buf)-1] != ' ' {
				buf = append(buf, ' ')
			}
		}
		if i == 0 {
			buf = bytes.TrimRight(buf, " ")
			buf = append(buf, ' ')
		}
	}
	buf = bytes.TrimRight(buf, " ")
	return fasthash.Hash64(0, buf), buf
}

func (srv *server) indexFile(filename string, seen map[uint64]struct{}) error {
	f, err := os.Open(filename)
	if err != nil {
		return err
	}
	br := bufio.NewReader(f)
	r := csv.NewReader(br)
	r.FieldsPerRecord = 2
	r.Comma = '\t'
	fileIndex := uint32(len(srv.Filenames))
	srv.Filenames = append(srv.Filenames, filename)
	buf := make([]byte, 1024)
	var h uint64
	for {
		row, err := r.Read()
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
		if row[0] == "" || row[1] == "" {
			continue
		}
		h, buf = hashRow(row, buf)
		if _, ok := seen[h]; ok {
			continue
		}
		seen[h] = struct{}{}
		tokens := tokenizer.Tokens(row[0])
		numTokens := len(tokens)
		if numTokens > 255 {
			numTokens = 255
		}
		sid := srv.Index.Add(tokenizer.UniqueInPlace(tokens))
		srv.ID2D[sid] = dd{
			FileIndex: fileIndex,
			Offset:    int64(r.CurrentRowOffset),
			NumTokens: uint8(numTokens),
		}
	}
	return nil
}

func (srv *server) readRows(fileIndex uint32, offsets []int64) ([][]string, error) {
	f, err := os.Open(srv.Filenames[fileIndex])
	if err != nil {
		return nil, err
	}
	defer f.Close()

	br := bufio.NewReader(f)
	sort.Slice(offsets, func(i, j int) bool { return offsets[i] < offsets[j] })
	rows := make([][]string, 0, len(offsets))

	for _, o := range offsets {
		if _, err := f.Seek(o, io.SeekStart); err != nil {
			return nil, err
		}
		br.Reset(f)
		r := csv.NewReader(br)
		r.FieldsPerRecord = 2
		r.Comma = '\t'
		row, err := r.Read()
		if err != nil {
			return nil, err
		}
		rows = append(rows, row)
	}
	return rows, nil
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func abs(a int) int {
	if a < 0 {
		return -a
	}
	return a
}

func computeSimilarity(a, b []string) int {
	return int(100 * (1 - float64(levenshtein.DistancePhrases(a, b))/float64(max(len(a), len(b)))))
}

func estimateSimilarity(l1, l2 int) int {
	return int(100 * (1 - float64(abs(l1-l2))/float64(max(l1, l2))))
}

func appendInt64Uniq(a []int64, v int64) []int64 {
	for _, x := range a {
		if x == v {
			return a
		}
	}
	return append(a, v)
}

func (srv *server) fuzzyHandler(w http.ResponseWriter, r *http.Request) {
	pct, _ := strconv.Atoi(r.FormValue("s"))
	if pct == 0 {
		pct = 70
	}

	sents := tokenizer.UniqueInPlace(segmenter.Segment(r.FormValue("q")))

	type foundDoc struct {
		i int
		dd
	}
	var foundDocs []foundDoc

	tokenizedSents := make([][]string, len(sents))
	for i, s := range sents {
		tokens := tokenizer.Tokens(s)
		tokenizedSents[i] = tokens
		for _, sid := range srv.Index.Query(tokenizer.Unique(tokens), pct) {
			d := srv.ID2D[sid]
			if bestEstimate := estimateSimilarity(len(tokens), int(d.NumTokens)); bestEstimate < pct {
				continue
			}
			foundDocs = append(foundDocs, foundDoc{i, d})
		}
	}

	m := make(map[uint32][]int64)
	for _, d := range foundDocs {
		m[d.FileIndex] = appendInt64Uniq(m[d.FileIndex], d.Offset)
	}

	type key struct {
		Offset    int64
		FileIndex uint32
	}
	fo2row := make(map[key][]string)
	for fi, offsets := range m {
		rows, err := srv.readRows(fi, offsets)
		if err != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			log.Println(err)
			return
		}
		for i, r := range rows {
			fo2row[key{offsets[i], fi}] = r
		}
	}

	type fuzzyResult struct {
		Text        string `json:"text"`
		Similarity  int    `json:"similarity"`
		HTML        string `json:"html"`
		Translation string `json:"translation"`
	}
	results := make([]fuzzyResult, 0, len(foundDocs))

	for _, d := range foundDocs {
		row := fo2row[key{d.Offset, d.FileIndex}]
		rtok := tokenizer.Tokens(row[0])
		sim := computeSimilarity(rtok, tokenizedSents[d.i])
		if sim < pct {
			continue
		}
		results = append(results, fuzzyResult{
			Text:        row[0],
			Similarity:  sim,
			HTML:        diff.HTML(row[0], sents[d.i]),
			Translation: row[1],
		})
	}

	sort.Slice(results, func(i, j int) bool {
		return results[i].Similarity > results[j].Similarity
	})

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	if err := json.NewEncoder(w).Encode(results); err != nil {
		log.Println(err)
	}
}

func renderWithRegexp(s string, r *regexp.Regexp) template.HTML {
	if r == nil {
		return template.HTML(html.EscapeString(s)) //nolint:gosec
	}
	idxs := r.FindAllStringIndex(s, -1)
	i := 0
	var buf strings.Builder
	for _, idx := range idxs {
		from, to := idx[0], idx[1]
		buf.WriteString(html.EscapeString(s[i:from]))
		buf.WriteString(`<mark>`)
		buf.WriteString(html.EscapeString(s[from:to]))
		buf.WriteString("</mark>")
		i = to
	}
	buf.WriteString(html.EscapeString(s[i:]))
	s = nl2br.Replace(buf.String())
	s = insertSoftBreaks(s)
	return template.HTML(s) //nolint:gosec
}

func regexpFromTokens(tokens []string) *regexp.Regexp {
	sort.Slice(tokens, func(i, j int) bool {
		if ll := len(tokens[i]) - len(tokens[j]); ll != 0 {
			return ll > 0
		}
		return tokens[i] < tokens[j]
	})
	for i, tok := range tokens {
		tokens[i] = regexp.QuoteMeta(tok)
	}
	return regexp.MustCompile(`(?i)\b(?:` + strings.Join(tokens, "|") + `)\b`)
}

func (srv *server) concordanceHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	query := r.FormValue("q")
	queryResult := srv.Index.Query(tokenizer.UniqueInPlace(tokenizer.Tokens(query)), 100)

	maxResults, _ := strconv.Atoi(r.FormValue("limit"))
	if maxResults <= 0 {
		maxResults = 100
	}
	from, _ := strconv.Atoi(r.FormValue("from"))
	if from >= len(queryResult) {
		w.Header().Set("Content-Type", "text/html")
		return
	}
	next := 0
	queryResult = queryResult[from:]
	if len(queryResult) > maxResults {
		queryResult = queryResult[:maxResults]
		next = from + maxResults
	}

	foundDocs := make([]dd, 0, len(queryResult))
	for _, id := range queryResult {
		foundDocs = append(foundDocs, srv.ID2D[id])
	}
	m := make(map[uint32][]int64)
	for _, d := range foundDocs {
		m[d.FileIndex] = appendInt64Uniq(m[d.FileIndex], d.Offset)
	}

	type key struct {
		Offset    int64
		FileIndex uint32
	}
	fo2row := make(map[key][]string)
	for fi, offsets := range m {
		rows, err := srv.readRows(fi, offsets)
		if err != nil {
			http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			log.Println(err)
			return
		}
		for i, r := range rows {
			fo2row[key{offsets[i], fi}] = r
		}
	}

	type concordanceResult struct {
		Text        string
		Translation string
		FullMatch   bool
	}
	results := make([]concordanceResult, 0, len(foundDocs))

	ma := substring.NewMatcher(query)
	for _, d := range foundDocs {
		row := fo2row[key{d.Offset, d.FileIndex}]
		results = append(results, concordanceResult{
			Text:        row[0],
			Translation: row[1],
			FullMatch:   ma.Match(row[0]),
		})
	}

	sort.SliceStable(results, func(i, j int) bool {
		if results[i].FullMatch == results[j].FullMatch {
			return len(results[i].Text) < len(results[j].Text)
		}
		return results[i].FullMatch && !results[j].FullMatch
	})

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	if err := concordanceTmpl.Execute(w, struct {
		Results []concordanceResult
		Regexp  *regexp.Regexp
		Query   string
		Next    int
	}{
		results,
		regexpFromTokens(tokenizer.UniqueInPlace(tokenizer.Tokens(query))),
		query,
		next,
	}); err != nil {
		log.Println(err)
	}
}

func (srv *server) persist(filename string) error {
	f, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer f.Close()
	bw := bufio.NewWriter(f)
	encoder := gob.NewEncoder(bw)
	if err := encoder.Encode(srv); err != nil {
		return err
	}
	if err := bw.Flush(); err != nil {
		return err
	}
	return f.Close()
}

func (srv *server) load(filename string) error {
	f, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer f.Close()
	br := bufio.NewReader(f)
	decoder := gob.NewDecoder(br)
	var s server
	if err := decoder.Decode(&s); err != nil {
		return err
	}
	*srv = s
	return nil
}

func main() {
	if len(os.Args) < 3 {
		log.Fatal("usage: tmd index|serve INDEX [OPTIONS] [TSV FILES]")
	}

	cmd := os.Args[1]
	indexName := os.Args[2]
	os.Args = append(os.Args[:1], os.Args[3:]...)

	addr := flag.String("http", ":3456", "HTTP service address")
	flag.Parse()

	srv := server{
		Index: inverted.NewIndex(),
		ID2D:  make(map[docID]dd),
	}

	switch cmd {
	case "index":
		start := time.Now()
		seen := make(map[uint64]struct{})
		for _, f := range flag.Args() {
			t := time.Now()
			if err := srv.indexFile(f, seen); err != nil {
				panic(err)
			}
			log.Printf("%s (%v)\n", f, time.Since(t))
		}
		if err := srv.persist(indexName); err != nil {
			panic(err)
		}
		log.Println("total indexing time is", time.Since(start))

	case "serve":
		start := time.Now()
		if err := srv.load(indexName); err != nil {
			panic(err)
		}
		log.Println("loaded index in", time.Since(start))
		http.HandleFunc("/fuzzy", srv.fuzzyHandler)
		http.HandleFunc("/plugins/concordance", srv.concordanceHandler)
		log.Fatal(http.ListenAndServe(*addr, nil))
	}
}

var concordanceHTML = `<div>
<table class="table">
  <thead>
  <tr>
    <th>Original</th>
    <th>Translation</th>
  </tr>
  </thead>
  <tbody>
    {{ range .Results }}
      <tr>
        <td>
          <p>{{ renderWithRegexp .Text $.Regexp }}</p>
        </td>
        <td>
          <p>{{ .Translation }}</p>
        </td>
      </tr>
    {{ end }}
  </tbody>
</table>
{{ if .Next }}
  <button class="btn btn-light concordance__btn-more" data-query="{{ .Query }}" data-next="{{ .Next }}">More</button>
{{ end }}
</div>`
