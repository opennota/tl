// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"sync"

	"gitlab.com/opennota/tl/fuzzy/cosine"
	"gitlab.com/opennota/tl/fuzzy/transformers"
)

const defaultRowsPerPage = 50

var (
	rowsPerPage = defaultRowsPerPage

	postMtx  sync.RWMutex
	left     [][]string
	right    [][]string
	nonce    uint64
	colorize bool

	rSpace = regexp.MustCompile(`\s+`)
)

func splitToWords(s string) [][]string {
	var ss [][]string
	for _, s := range rNewline.Split(s, -1) {
		s = strings.TrimSpace(s)
		if s != "" {
			ss = append(ss, rSpace.Split(s, -1))
		}
	}
	return ss
}

func loadCSV(r io.Reader, sep rune) error {
	cr := csv.NewReader(r)
	cr.Comma = sep
	cr.FieldsPerRecord = 2
	left = nil
	right = nil
	for {
		row, err := cr.Read()
		if err != nil {
			if err == io.EOF {
				break
			}
			return err
		}
		left = append(left, rSpace.Split(row[0], -1))
		right = append(right, rSpace.Split(row[1], -1))
	}
	return nil
}

func handleGetSimilarity(w http.ResponseWriter, r *http.Request) {
	a := r.Form["a[]"]
	b := r.Form["b[]"]
	if len(a) != len(b) {
		http.Error(w, "Bad request", http.StatusBadRequest)
		return
	}
	va, err := transformers.Embeddings(a)
	if err != nil {
		http.Error(w, "Bad gateway", http.StatusBadGateway)
		return
	}
	vb, err := transformers.Embeddings(b)
	if err != nil {
		http.Error(w, "Bad gateway", http.StatusBadGateway)
		return
	}
	sim := make([]float64, 0, len(a))
	for i, v := range va {
		sim = append(sim, cosine.Similarity(v, vb[i]))
	}
	w.Header().Add("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(sim); err != nil {
		logError(err)
	}
}

func (a *App) Aligner(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		what := r.FormValue("download")
		if what == "" {
			postMtx.Lock()
			nonce = 0
			postMtx.Unlock()
		}

		postMtx.RLock()
		defer postMtx.RUnlock()

		switch what {
		default:
			pageNumber, _ := strconv.Atoi(r.FormValue("page"))
			if pageNumber == 0 {
				pageNumber = 1
			}
			totalRows := max(len(left), len(right))
			offset := (pageNumber - 1) * rowsPerPage
			if totalRows > 0 && offset >= totalRows || totalRows == 0 && offset > 0 {
				http.Redirect(w, r, "/aligner", http.StatusSeeOther)
				return
			}
			totalPages := (totalRows + rowsPerPage - 1) / rowsPerPage

			uploadType := r.FormValue("type")
			if uploadType != "plaintext" && uploadType != "csv" {
				uploadType = "plaintext"
			}

			w.Header().Set("Content-Type", "text/html")
			if err := alignerTmpl.Execute(w, struct {
				Left        [][]string
				Right       [][]string
				PageNumber  int
				TotalPages  int
				Nonce       uint64
				Type        string
				RowsPerPage int
				WithST      bool
				Colorize    bool
			}{
				left[offset:min(offset+rowsPerPage, len(left))],
				right[offset:min(offset+rowsPerPage, len(right))],
				pageNumber,
				totalPages,
				1,
				uploadType,
				rowsPerPage,
				a.withSentenceTransformers,
				colorize,
			}); err != nil {
				logError(err)
			}

		case "left", "right":
			w.Header().Add("Content-Type", "text/plain; charset=utf-8")
			w.Header().Add("Content-Disposition", fmt.Sprintf(`attachment; filename="%s.txt"`, what))
			text := left
			if what == "right" {
				text = right
			}
			for _, ss := range text {
				fmt.Fprintln(w, strings.Join(ss, " "))
			}

		case "csv", "tsv":
			cw := csv.NewWriter(w)
			if what == "csv" {
				w.Header().Add("Content-Type", "text/csv; charset=utf-8")
			} else {
				w.Header().Add("Content-Type", "text/tab-separated-values; charset=utf-8")
				cw.Comma = '\t'
			}
			w.Header().Add("Content-Disposition", fmt.Sprintf(`attachment; filename="book.%s"`, what))
			for i := 0; i < max(len(left), len(right)); i++ {
				l, r := "", ""
				if i < len(left) {
					l = strings.Join(left[i], " ")
				}
				if i < len(right) {
					r = strings.Join(right[i], " ")
				}
				if err := cw.Write([]string{l, r}); err != nil {
					log.Print(err)
				}
			}
			cw.Flush()
			if err := cw.Error(); err != nil {
				log.Print(err)
			}
		}

	case "POST":
		op := r.FormValue("op")
		if op == "getsimilarity" {
			handleGetSimilarity(w, r)
			return
		}

		postMtx.Lock()
		defer postMtx.Unlock()

		expectedNonce := nonce + 1
		if clientNonce, _ := u64(r.FormValue("nonce")); clientNonce != expectedNonce {
			http.Error(w, "", http.StatusConflict)
			return
		}
		nonce++

		pageNumber, _ := strconv.Atoi(r.FormValue("page"))
		if pageNumber < 0 {
			pageNumber = 1
		}
		i, _ := strconv.Atoi(r.FormValue("row"))
		j, _ := strconv.Atoi(r.FormValue("word"))
		side := r.FormValue("side")
		offset := (pageNumber - 1) * rowsPerPage
		i += offset

		if op != "" {
			if offset > len(left) {
				left = append(left, make([][]string, offset-len(left))...)
			}
			if offset > len(right) {
				right = append(right, make([][]string, offset-len(right))...)
			}
		}

		switch op {
		default:
			err := r.ParseMultipartForm(32 * 1024 * 1024)
			if err != nil {
				log.Println(err)
			}
			if v := r.PostFormValue("rowsperpage"); v != "" {
				rowsPerPage, _ = strconv.Atoi(v)
				if rowsPerPage <= 0 {
					rowsPerPage = defaultRowsPerPage
				}
			}
			if f, _, _ := r.FormFile("csvfile"); f == nil {
				left = splitToWords(r.PostFormValue("left"))
				right = splitToWords(r.PostFormValue("right"))
			} else {
				sep := ','
				switch r.PostFormValue("separator") {
				case ";":
					sep = ';'
				case "\t":
					sep = '\t'
				}
				if err := loadCSV(f, sep); err != nil {
					log.Println(err)
				}
			}
			http.Redirect(w, r, "/aligner", http.StatusSeeOther)

		case "split":
			if side == "left" {
				left = append(left, nil)
				copy(left[i+2:], left[i+1:])
				left[i+1] = left[i][j:]
				left[i] = left[i][:j:j]
			} else {
				right = append(right, nil)
				copy(right[i+2:], right[i+1:])
				right[i+1] = right[i][j:]
				right[i] = right[i][:j:j]
			}
			w.WriteHeader(http.StatusNoContent)

		case "join":
			var joined, bottom []string
			if side == "left" {
				if i+1 < len(left) {
					joined = left[i+1]
					left[i] = append(left[i], left[i+1]...)
					left = append(left[:i+1], left[i+2:]...)
				}
			} else {
				if i+1 < len(right) {
					joined = right[i+1]
					right[i] = append(right[i], right[i+1]...)
					right = append(right[:i+1], right[i+2:]...)
				}
			}
			offset += rowsPerPage - 1

			s := left
			if side != "left" {
				s = right
			}
			if offset < len(s) {
				bottom = s[offset]
			}

			w.Header().Add("Content-Type", "application/json")
			_ = json.NewEncoder(w).Encode([][]string{joined, bottom})

		case "rm":
			if i < len(left) {
				left = append(left[:i], left[i+1:]...)
			}
			if i < len(right) {
				right = append(right[:i], right[i+1:]...)
			}
			offset += rowsPerPage - 1

			var s, t []string
			if offset < len(left) {
				s = left[offset]
			}
			if offset < len(right) {
				t = right[offset]
			}

			w.Header().Add("Content-Type", "application/json")
			_ = json.NewEncoder(w).Encode([][]string{s, t})

		case "edit":
			words := rSpace.Split(r.FormValue("text"), -1)
			if side == "left" {
				for i >= len(left) {
					left = append(left, nil)
				}
				left[i] = words
			} else {
				for i >= len(right) {
					right = append(right, nil)
				}
				right[i] = words
			}

			w.Header().Add("Content-Type", "application/json")
			_ = json.NewEncoder(w).Encode(words)

		case "swap":
			left, right = right, left

		case "clear":
			left = nil
			right = nil

		case "import":
			title := strings.TrimSpace(r.FormValue("title"))
			if title == "" {
				http.Error(w, "Title must not be empty!", http.StatusBadRequest)
				return
			}
			records := make([][]string, 0, len(left))
			for i, words := range left {
				if len(words) == 0 {
					continue
				}
				translation := ""
				if i < len(right) {
					translation = strings.Join(right[i], " ")
				}
				records = append(records, []string{
					strings.Join(words, " "),
					translation,
				})
			}
			bid, err := a.db.AddTranslatedBook(title, records)
			if err != nil {
				internalError(w, err)
				return
			}

			w.Header().Add("Content-Type", "application/json")
			_ = json.NewEncoder(w).Encode(bid)

		case "setcolorize":
			colorize = r.FormValue("value") == "true"
		}

	default:
		http.Error(w, "method now allowed", http.StatusMethodNotAllowed)
	}
}
