// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"encoding/json"
	"net/http"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/boltdb/bolt"
	"gitlab.com/opennota/substring"
	"gitlab.com/opennota/tl/diff"
	"gitlab.com/opennota/tl/fuzzy/cosine"
	"gitlab.com/opennota/tl/fuzzy/segmenter"
	"gitlab.com/opennota/tl/fuzzy/tm"
	"gitlab.com/opennota/tl/fuzzy/tokenizer"
	"gitlab.com/opennota/tl/fuzzy/transformers"
)

func setupTMIndex(db DB) (*tm.TranslationMemory, time.Duration, error) {
	start := time.Now()
	trm, err := tm.NewTranslationMemory(db.walkAllFragments)
	if err != nil {
		return nil, 0, err
	}
	return trm, time.Since(start), nil
}

func getBestMatch(s string, ss []string) (string, error) {
	emb, err := transformers.Embeddings(
		append([]string{s}, ss...),
	)
	if err != nil {
		return "", err
	}
	maxSim := 0.0
	bestMatch := ""
	for i := 1; i < len(emb); i++ {
		if sim := cosine.Similarity(emb[0], emb[i]); sim > maxSim {
			maxSim = sim
			bestMatch = ss[i-1]
		}
	}
	return bestMatch, nil
}

func (db *DB) walkBookFragments(bid uint64, fn func(tm.SegmentDescriptor)) error {
	return db.View(func(tx *bolt.Tx) error {
		var book Book
		b := tx.Bucket([]byte("index"))
		if found, err := unmarshal(b, bid, &book); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}
		fb := tx.Bucket([]byte("fragments")).Bucket(encode(bid))
		for _, fid := range book.FragmentsIDs {
			var text string
			if found, err := unmarshal(fb, fid, &struct {
				Text *string
			}{&text}); err != nil {
				return err
			} else if !found {
				continue
			}
			fn(tm.SegmentDescriptor{BookID: bid, FragmentID: fid, Text: text})
		}
		return nil
	})
}

func (db *DB) walkAllFragments(fn func(tm.SegmentDescriptor)) error {
	return db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("index"))
		c := b.Cursor()
		for k, v := c.First(); k != nil; k, v = c.Next() {
			var bid uint64
			var fids []uint64
			if err := json.Unmarshal(v, &struct {
				ID           *uint64
				FragmentsIDs *[]uint64 `json:"fragments_ids"`
			}{
				&bid, &fids,
			}); err != nil {
				return err
			}
			fb := tx.Bucket([]byte("fragments")).Bucket(encode(bid))
			for _, fid := range fids {
				var text string
				if found, err := unmarshal(fb, fid, &struct {
					Text *string
				}{&text}); err != nil {
					return err
				} else if !found {
					continue
				}
				fn(tm.SegmentDescriptor{BookID: bid, FragmentID: fid, Text: text})
			}
		}
		return nil
	})
}

func (a *App) FuzzyMatches(w http.ResponseWriter, r *http.Request) {
	if a.tm == nil {
		http.NotFound(w, r)
		return
	}

	pct, _ := strconv.Atoi(r.FormValue("s"))
	if pct == 0 {
		pct = 70
	}
	translationRequired := r.FormValue("t") == "1"
	bid, _ := strconv.Atoi(r.FormValue("b"))
	fid, _ := strconv.Atoi(r.FormValue("f"))

	type fuzzyResult struct {
		tm.QueryResult
		HTML         string   `json:"html"`
		Translations []string `json:"translations,omitempty"`
	}

	sents := segmenter.Segment(r.FormValue("q"))
	results := []fuzzyResult{}
	for i, s := range sents {
		res := a.tm.Query(s, pct, false)
		for _, r := range res {
			var fz fuzzyResult

			if r.FragmentID == uint64(fid) && r.BookID == uint64(bid) {
				continue
			}
			vs, err := a.db.FetchVersions(r.BookID, r.FragmentID)
			if err != nil {
				logError(err)
				continue
			}
			if len(vs) == 0 && translationRequired {
				continue
			}
			for _, v := range vs {
				ss := segmenter.Segment(v.Text)
				if len(ss) == 0 {
					continue
				}
				if len(ss) == 1 {
					fz.Translations = append(fz.Translations, v.Text)
					continue
				}
				if !a.withSentenceTransformers {
					if len(ss) == len(sents) {
						fz.Translations = append(fz.Translations, ss[i])
					} else {
						fz.Translations = append(fz.Translations, v.Text)
					}
					continue
				}
				bestMatch, err := getBestMatch(s, ss)
				if err != nil {
					logError(err)
					continue
				}
				fz.Translations = append(fz.Translations, bestMatch)
			}

			fz.QueryResult = r
			fz.HTML = diff.HTML(r.Text, s)
			results = append(results, fz)
		}
	}

	sort.Slice(results, func(i, j int) bool {
		return results[i].Similarity > results[j].Similarity
	})

	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	if err := json.NewEncoder(w).Encode(results); err != nil {
		logError(err)
	}
}

func regexpFromTokens(tokens []string) *regexp.Regexp {
	sort.Slice(tokens, func(i, j int) bool {
		if ll := len(tokens[i]) - len(tokens[j]); ll != 0 {
			return ll > 0
		}
		return tokens[i] < tokens[j]
	})
	for i, tok := range tokens {
		tokens[i] = regexp.QuoteMeta(tok)
	}
	return regexp.MustCompile(`(?i)\b(?:` + strings.Join(tokens, "|") + `)\b`)
}

func (a *App) ConcordancePlugin(w http.ResponseWriter, r *http.Request) {
	type concordanceResult struct {
		tm.QueryResult
		Translations []string
		FullMatch    bool
	}
	results := make([]concordanceResult, 0, 10)

	query := r.FormValue("q")
	res := a.tm.Query(query, 0, true)
	m := substring.NewMatcher(query)
	tokens := tokenizer.Tokens(query)
	for _, r := range res {
		var translations []string
		vs, err := a.db.FetchVersions(r.BookID, r.FragmentID)
		if err != nil {
			logError(err)
			continue
		}
		if len(vs) == 0 {
			continue
		}
		for _, v := range vs {
			ss := segmenter.Segment(v.Text)
			if len(ss) == 0 {
				continue
			}
			if len(ss) == 1 || !a.withSentenceTransformers {
				translations = append(translations, v.Text)
				continue
			}
			bestMatch, err := getBestMatch(r.Text, ss)
			if err != nil {
				logError(err)
				continue
			}
			translations = append(translations, bestMatch)
		}
		if len(translations) == 0 {
			continue
		}
		results = append(results, concordanceResult{
			r,
			translations,
			m.Match(r.Text),
		})
	}

	sort.SliceStable(results, func(i, j int) bool {
		if results[i].FullMatch == results[j].FullMatch {
			return len(results[i].Text) < len(results[j].Text)
		}
		return results[i].FullMatch && !results[j].FullMatch
	})

	w.Header().Set("Content-Type", "text/html")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	if err := concordanceTmpl.Execute(w, struct {
		Results []concordanceResult
		Regexp  *regexp.Regexp
	}{
		results,
		regexpFromTokens(tokens),
	}); err != nil {
		logError(err)
	}
}
