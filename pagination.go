// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"fmt"
	"html"
	"html/template"
	"net/url"
	"strings"
)

type query struct {
	url.Values
}

func (v query) Page(n int) template.URL {
	if n == 1 {
		v.Values.Del("page")
	} else {
		v.Values.Set("page", fmt.Sprint(n))
	}
	return template.URL(v.Encode()) //nolint:gosec
}

type Pagination struct {
	url          *url.URL
	PageNumber   int
	TotalItems   int
	itemsPerPage int
}

func (p Pagination) TotalPages() int {
	return (p.TotalItems + p.itemsPerPage - 1) / p.itemsPerPage
}

func (p Pagination) Render() template.HTML {
	if p.TotalItems <= p.itemsPerPage {
		return ""
	}
	path := html.EscapeString(p.url.Path)
	q := query{p.url.Query()}
	tp := p.TotalPages()
	var buf strings.Builder
	buf.WriteString(`<div class="btn-group btn-group-xs ml-auto" style="height:21px;">`)
	buf.WriteString(`<ul class="pagination pagination-xs">`)
	if p.PageNumber > 2 {
		qry := q.Page(1)
		if qry == "" {
			fmt.Fprintf(&buf, `<li class="page-item"><a class="page-link" href="%s">1</a></li>`, path)
		} else {
			fmt.Fprintf(&buf, `<li class="page-item"><a class="page-link" href="%s?%s">1</a></li>`, path, qry)
		}
	}
	const jumpBtn = `<li class="page-item"><a class="page-link pagination__jump-to-page">…</a></li>`
	if p.PageNumber > 3 {
		buf.WriteString(jumpBtn)
	}
	if p.PageNumber > 1 {
		qry := q.Page(p.PageNumber - 1)
		if qry == "" {
			fmt.Fprintf(&buf, `<li class="page-item"><a class="page-link" rel="prev" href="%s">%d</a></li>`, path, p.PageNumber-1)
		} else {
			fmt.Fprintf(&buf, `<li class="page-item"><a class="page-link" rel="prev" href="%s?%s">%d</a></li>`, path, qry, p.PageNumber-1)
		}
	}
	fmt.Fprintf(&buf, `<li class="page-item disabled"><a class="page-link">%d</a></li>`, p.PageNumber)
	if p.PageNumber < tp {
		fmt.Fprintf(&buf, `<li class="page-item"><a class="page-link" rel="next" href="%s?%s">%d</a></li>`, path, q.Page(p.PageNumber+1), p.PageNumber+1)
	}
	if p.PageNumber+2 < tp {
		buf.WriteString(jumpBtn)
	}
	if p.PageNumber+1 < tp {
		rel := `rel="next" `
		if p.PageNumber < tp {
			rel = ""
		}
		fmt.Fprintf(&buf, `<li class="page-item"><a class="page-link" %shref="%s?%s">%d</a></li>`, rel, path, q.Page(tp), tp)
	}
	buf.WriteString(`</ul></div>`)
	return template.HTML(buf.String()) //nolint:gosec
}

func (p Pagination) RenderPrevNextButtons() template.HTML {
	if p.TotalItems <= p.itemsPerPage {
		return ""
	}
	path := html.EscapeString(p.url.Path)
	q := query{p.url.Query()}
	var buf strings.Builder
	buf.WriteString(`<ul class="pagination justify-content-end bottom-pager">`)
	if p.PageNumber < 2 {
		buf.WriteString(`<li class="page-item disabled"><a class="page-link page-link-prev">Previous</a></li>`)
	} else {
		qry := q.Page(p.PageNumber - 1)
		if qry == "" {
			fmt.Fprintf(&buf, `<li class="page-item"><a class="page-link page-link-prev" rel="prev" href="%s">Previous</a></li>`, path)
		} else {
			fmt.Fprintf(&buf, `<li class="page-item"><a class="page-link page-link-prev" rel="prev" href="%s?%s">Previous</a></li>`, path, qry)
		}
	}
	buf.WriteByte(' ')
	if p.PageNumber >= p.TotalPages() {
		buf.WriteString(`<li class="page-item disabled"><a class="page-link page-link-next">Next</a></li>`)
	} else {
		fmt.Fprintf(&buf, `<li class="page-item"><a class="page-link page-link-next" rel="next" href="%s?%s">Next</a></li>`, path, q.Page(p.PageNumber+1))
	}
	buf.WriteString(`</ul></div>`)
	return template.HTML(buf.String()) //nolint:gosec
}
