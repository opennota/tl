module gitlab.com/opennota/tl

require (
	github.com/PuerkitoBio/goquery v1.8.0
	github.com/boltdb/bolt v1.3.1
	github.com/chromedp/cdproto v0.0.0-20220228194516-ead8cf59b1fa
	github.com/chromedp/chromedp v0.7.8
	github.com/dchest/stemmer v0.0.0-20161207102402-66719a20c4b5
	github.com/felixge/httpsnoop v1.0.2 // indirect
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/securecookie v1.1.1
	github.com/gorilla/sessions v1.2.1
	github.com/jmoiron/sqlx v1.3.4
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/microcosm-cc/bluemonday v1.0.18
	gitlab.com/opennota/csv v1.0.0
	gitlab.com/opennota/dkv v1.0.3
	gitlab.com/opennota/fasthash v1.0.0
	gitlab.com/opennota/fresheye v1.0.2
	gitlab.com/opennota/morph v1.0.5
	gitlab.com/opennota/mt v1.0.2
	gitlab.com/opennota/stemka v1.0.0
	gitlab.com/opennota/substring v1.0.0
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f // indirect
	golang.org/x/sys v0.0.0-20220227234510-4e6760a101f9 // indirect
	golang.org/x/time v0.0.0-20220224211638-0e9765cccd65 // indirect
	golang.org/x/tools v0.1.9 // indirect
	lukechampine.com/uint128 v1.2.0 // indirect
	modernc.org/sqlite v1.14.7
)

go 1.16

// +heroku goVersion go1.17
