tl [![License](http://img.shields.io/:license-agpl3-blue.svg)](http://www.gnu.org/licenses/agpl-3.0.html) [![Pipeline status](https://gitlab.com/opennota/tl/badges/master/pipeline.svg)](https://gitlab.com/opennota/tl/commits/master)
==

![Screencast](/screencast.gif)

## Install

    go install gitlab.com/opennota/tl@latest

(You'll need Go 1.16 or newer.)

Or [download a pre-compiled binary](https://gitlab.com/opennota/tl/-/releases).

## Use

    tl -http :1337 -db /path/to/tl.db

- `-http :1337` - listen on port 1337 (by default port 3000 or $PORT on localhost)
- `-db /path/to/tl.db` - path to the translations database (by default `tl.db` in the current directory)

## Donate

**Bitcoin (BTC):** `1PEaahXKwJvNJGJa2PXtPFLNYYigmdLXct`

**Ethereum (ETH):** `0x83e9607E693467Cb344244Df10f66c036eC3Dc53`
