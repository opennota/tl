// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package diff

import (
	"bytes"
	"html"
	"strings"
	"unicode"
	"unicode/utf8"
)

const (
	diffNone = iota
	diffAdded
	diffRemoved
)

type comparer struct {
	ignoreWhitespace bool
	tokenize         func(string) []string
}

type component struct {
	value  string
	status int
}

type cpath struct {
	newPos int
	comp   []component
}

func clonePath(p *cpath) *cpath {
	newp := cpath{
		newPos: p.newPos,
		comp:   make([]component, len(p.comp)),
	}
	copy(newp.comp, p.comp)
	return &newp
}

func pushComponent(comp *[]component, value string, status int) {
	if len(*comp) > 0 {
		last := (*comp)[len(*comp)-1]
		if last.status == status {
			(*comp)[len(*comp)-1] = component{
				value:  last.value + value,
				status: status,
			}
			return
		}
	}

	*comp = append(*comp, component{
		value:  value,
		status: status,
	})
}

func (c *comparer) extractCommon(basePath *cpath, new, old []string, diagonalPath int) int {
	newPos := basePath.newPos
	oldPos := newPos - diagonalPath
	for newPos+1 < len(new) && oldPos+1 < len(old) && c.equals(new[newPos+1], old[oldPos+1]) {
		newPos++
		oldPos++
		pushComponent(&basePath.comp, new[newPos], diffNone)
	}
	basePath.newPos = newPos
	return oldPos
}

func allWhitespace(s string) bool {
	for _, r := range s {
		if !unicode.IsSpace(r) {
			return false
		}
	}
	return true
}

func tokenize1(s string) (tokens []string) {
	var buf bytes.Buffer
	previousIsLetter := false
	previousIsSpace := false
	for _, r := range s {
		isSpace := unicode.IsSpace(r)
		isLetter := !isSpace && unicode.IsLetter(r)
		if isLetter != previousIsLetter || isSpace != previousIsSpace {
			if buf.Len() > 0 {
				tokens = append(tokens, buf.String())
				buf.Reset()
			}
		}
		buf.WriteRune(r)
		previousIsLetter = isLetter
		previousIsSpace = isSpace
	}

	if buf.Len() > 0 {
		tokens = append(tokens, buf.String())
	}
	return
}

func tokenize2(s string) (tokens []string) {
	tokens = make([]string, 0, utf8.RuneCountInString(s))
	for _, r := range s {
		tokens = append(tokens, string(r))
	}
	return tokens
}

func (c *comparer) equals(a, b string) bool {
	if c.ignoreWhitespace &&
		allWhitespace(a) &&
		allWhitespace(b) {
		return true
	}
	return a == b
}

func (c *comparer) diff(a, b string) []component {
	if a == b {
		return []component{{value: b}}
	}
	if b == "" {
		return []component{{value: a, status: diffRemoved}}
	}
	if a == "" {
		return []component{{value: b, status: diffAdded}}
	}

	tokenize := c.tokenize
	if tokenize == nil {
		tokenize = tokenize1
	}

	old := tokenize(a)
	new := tokenize(b)

	maxEditLength := len(old) + len(new)
	bestPath := map[int]*cpath{
		0: {newPos: -1},
	}
	oldPos := c.extractCommon(bestPath[0], new, old, 0)
	if bestPath[0].newPos+1 >= len(new) && oldPos+1 >= len(old) {
		return bestPath[0].comp
	}

	for editLength := 1; editLength <= maxEditLength; editLength++ {
		for diagonalPath := -editLength; diagonalPath <= editLength; diagonalPath += 2 {
			addPath := bestPath[diagonalPath-1]
			removePath := bestPath[diagonalPath+1]
			oldPos = 0
			if removePath != nil {
				oldPos = removePath.newPos
			}
			oldPos -= diagonalPath
			if addPath != nil {
				delete(bestPath, diagonalPath-1)
			}

			canAdd := addPath != nil && addPath.newPos+1 < len(new)
			canRemove := removePath != nil && 0 <= oldPos && oldPos < len(old)
			if !canAdd && !canRemove {
				delete(bestPath, diagonalPath)
				continue
			}

			var basePath *cpath
			if !canAdd || (canRemove && addPath.newPos < removePath.newPos) {
				basePath = clonePath(removePath)
				pushComponent(&basePath.comp, old[oldPos], diffRemoved)
			} else {
				basePath = clonePath(addPath)
				basePath.newPos++
				pushComponent(&basePath.comp, new[basePath.newPos], diffAdded)
			}

			oldPos = c.extractCommon(basePath, new, old, diagonalPath)

			if basePath.newPos+1 >= len(new) && oldPos+1 >= len(old) {
				return basePath.comp
			}

			bestPath[diagonalPath] = basePath
		}
	}
	return nil
}

func numDifferences(comps []component) int {
	n := 0
	for _, c := range comps {
		if c.status != diffNone {
			n++
		}
	}
	return n
}

func joinComponents(comps []component) []component {
	cs := make([]component, 0, len(comps))
	for i := 0; i < len(comps); i++ {
		if comps[i].status == diffNone {
			cs = append(cs, comps[i])
			continue
		}
		run := 1
		for k := i + 1; k < len(comps) && (comps[k].status != diffNone || allWhitespace(comps[k].value)); k++ {
			run++
		}
		if i+run-1 < len(comps) && comps[i+run-1].status == diffNone {
			run--
		}
		if run == 1 {
			cs = append(cs, comps[i])
			continue
		}
		remove := ""
		for k := i; k < i+run; k++ {
			if comps[k].status == diffRemoved || comps[k].status == diffNone {
				remove += comps[k].value
			}
		}
		add := ""
		for k := i; k < i+run; k++ {
			if comps[k].status == diffAdded || comps[k].status == diffNone {
				add += comps[k].value
			}
		}
		if remove != "" {
			cs = append(cs, component{remove, diffRemoved})
		}
		if add != "" {
			cs = append(cs, component{add, diffAdded})
		}
		i += run - 1
	}
	return cs
}

func HTML(a, b string) string {
	cmp := comparer{}
	comps := cmp.diff(a, b)
	comps = joinComponents(comps)
	var buf strings.Builder
	for _, c := range comps {
		switch c.status {
		default:
			buf.WriteString(html.EscapeString(c.value))
		case diffAdded:
			buf.WriteString(`<span class="diff__added0">`)
			buf.WriteString(html.EscapeString(c.value))
			buf.WriteString("</span>")
		case diffRemoved:
			buf.WriteString(`<span class="diff__removed2">`)
			buf.WriteString(html.EscapeString(c.value))
			buf.WriteString("</span>")
		}
	}
	return buf.String()
}

var nl2br = strings.NewReplacer("\n", "<br>\n")

func TwoSideHTML(a, b string) (string, string) {
	cmp := comparer{}
	comps := cmp.diff(a, b)
	l := make([]string, 0, len(comps))
	r := make([]string, 0, len(comps))
	cmp2 := comparer{tokenize: tokenize2}
	for i := 0; i < len(comps); i++ {
		c := comps[i]
		switch c.status {
		case diffAdded:
			if i+1 < len(comps) && comps[i+1].status == diffRemoved {
				comps2 := cmp2.diff(comps[i+1].value, c.value)
				if numDifferences(comps2) < 3 {
					l = append(l, `<span class="diff__removed0">`)
					r = append(r, `<span class="diff__added0">`)
					for _, c2 := range comps2 {
						v2 := html.EscapeString(c2.value)
						switch c2.status {
						case diffAdded:
							r = append(r, `<span class="diff__added1">`, v2, `</span>`)
						case diffRemoved:
							l = append(l, `<span class="diff__removed1">`, v2, `</span>`)
						default:
							l = append(l, v2)
							r = append(r, v2)
						}
					}
					l = append(l, `</span>`)
					r = append(r, `</span>`)
					i++
					break
				}
			}
			v := html.EscapeString(c.value)
			r = append(r, `<span class="diff__added1">`, v, `</span>`)
		case diffRemoved:
			v := html.EscapeString(c.value)
			l = append(l, `<span class="diff__removed1">`, v, `</span>`)
		default:
			v := html.EscapeString(c.value)
			l = append(l, v)
			r = append(r, v)
		}
	}
	return nl2br.Replace(strings.Join(l, "")), nl2br.Replace(strings.Join(r, ""))
}
