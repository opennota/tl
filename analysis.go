// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"net/http"
	"strconv"

	"gitlab.com/opennota/tl/analysis"
)

func scoreFuncByName(name string) (string, analysis.ScoreFunc) {
	switch name {
	case "MI":
		return name, analysis.MI
	case "MI3":
		return name, analysis.MI3
	default:
		return "MI3", analysis.MI3
	}
}

func (a *App) Analysis(w http.ResponseWriter, r *http.Request) {
	db, err := analysis.New("file:kwic?mode=memory&cache=shared")
	if err != nil {
		internalError(w, err)
		return
	}
	defer db.Close()

	if r.Method == "POST" {
		if r.URL.Path == "/analysis/new" {
			if err := db.Drop(); err != nil {
				logError(err)
			}
		} else {
			if err := r.ParseMultipartForm(32 * 1024 * 1024); err != nil {
				internalError(w, err)
				return
			}
			if err := db.Add(r.PostFormValue("text")); err != nil {
				internalError(w, err)
				return
			}
		}
		http.Redirect(w, r, "/analysis", http.StatusSeeOther)
		return
	}

	wc, err := db.WordCount()
	if err != nil {
		internalError(w, err)
		return
	}
	tab := r.FormValue("tab")
	if tab != "" && wc == 0 {
		http.Redirect(w, r, "/analysis", http.StatusSeeOther)
		return
	}

	var words []analysis.WordWithFrequency
	var minFreq, minLength, ngramLength int
	var morph bool
	var ngrams []analysis.NgramWithScore
	var q1, q2 string
	var left, right int
	var coll []analysis.Collocation
	var score string
	var scoreFunc analysis.ScoreFunc
	switch tab {
	case "words":
		n, err := strconv.Atoi(r.FormValue("minFreq"))
		l, _ := strconv.Atoi(r.FormValue("minLength"))
		m := r.FormValue("morph") != ""
		if err == nil {
			if m {
				words, err = db.Norms(n, l)
			} else {
				words, err = db.Words(n, l)
			}
			if err != nil {
				internalError(w, err)
				return
			}
		}
		minFreq, minLength, morph = n, l, m
	case "ngrams":
		n, _ := strconv.Atoi(r.FormValue("length"))
		score, scoreFunc = scoreFuncByName(r.FormValue("score"))
		if n > 0 {
			var err error
			ngrams, err = db.Ngrams(n, scoreFunc)
			if err != nil {
				internalError(w, err)
				return
			}
		}
		ngramLength = n
	case "coll":
		q1 = r.FormValue("q1")
		q2 = r.FormValue("q2")
		left, _ = strconv.Atoi(r.FormValue("l"))
		right, _ = strconv.Atoi(r.FormValue("r"))
		score, scoreFunc = scoreFuncByName(r.FormValue("score"))
		if left > 0 || right > 0 {
			var err error
			coll, err = db.Collocations(q1, q2, left, right, scoreFunc)
			if err != nil {
				internalError(w, err)
				return
			}
		} else {
			left, right = 0, 1
		}
	}

	w.Header().Set("Content-Type", "text/html")
	if err := analysisTmpl.Execute(w, struct {
		Tab                string
		WordCount          int
		Words              []analysis.WordWithFrequency
		MinFreq, MinLength int
		Morphology         bool
		NgramLength        int
		Score              string
		Ngrams             []analysis.NgramWithScore
		Coll               []analysis.Collocation
		Q1, Q2             string
		Left, Right        int
	}{
		tab,
		wc,
		words,
		minFreq, minLength,
		morph,
		ngramLength,
		score,
		ngrams,
		coll,
		q1, q2,
		left, right,
	}); err != nil {
		logError(err)
	}
}
