// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package cosine

import "testing"

func fabs(f float64) float64 {
	if f < 0 {
		return -f
	}
	return f
}

func TestSimilarity(t *testing.T) {
	a := []float32{1.2, 5.6, 7.8}
	b := []float32{1.1, 3.3, 9.9}
	want := 0.9554702879901009
	if got := Similarity(a, b); fabs(want-got) > 1e-8 {
		t.Errorf("Similarity(%v, %v) = %.16f, want %.16f", a, b, got, want)
	}
}
