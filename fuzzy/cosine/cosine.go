// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Package cosine provides a function for computing cosine similarity.
package cosine

import "math"

// Similarity returns cosine similarity between the two vectors.
func Similarity(a, b []float32) float64 {
	if len(a) != len(b) {
		panic("vector lengths must be equal!")
	}
	dot := 0.0
	d1 := 0.0
	d2 := 0.0
	for i := 0; i < len(a); i++ {
		ai, bi := float64(a[i]), float64(b[i])
		dot += ai * bi
		d1 += ai * ai
		d2 += bi * bi
	}
	return dot / (math.Sqrt(d1) * math.Sqrt(d2))
}
