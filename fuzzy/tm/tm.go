// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Package tm implements Translation Memory.
package tm

import (
	"sort"
	"sync"

	"gitlab.com/opennota/tl/fuzzy/inverted"
	"gitlab.com/opennota/tl/fuzzy/levenshtein"
	"gitlab.com/opennota/tl/fuzzy/segmenter"
	"gitlab.com/opennota/tl/fuzzy/tokenizer"
)

type sid = inverted.DocID

type SegmentDescriptor struct {
	BookID     uint64 `json:"book_id,omitempty"`
	FragmentID uint64 `json:"fragment_id,omitempty"`
	Text       string `json:"text"`
}

type TranslationMemory struct {
	m     sync.RWMutex
	index inverted.Index
	d2sid map[SegmentDescriptor]sid
	sid2d map[sid]SegmentDescriptor
	buf   []rune
}

type QueryResult struct {
	SegmentDescriptor
	Similarity int `json:"similarity"`
}

type SegmentWalkFn func(func(SegmentDescriptor)) error

func NewTranslationMemory(walk SegmentWalkFn) (*TranslationMemory, error) {
	m := TranslationMemory{
		index: inverted.NewIndex(),
		d2sid: make(map[SegmentDescriptor]sid),
		sid2d: make(map[sid]SegmentDescriptor),
	}
	if err := walk(m.internalAdd); err != nil {
		return nil, err
	}

	return &m, nil
}

func (m *TranslationMemory) internalAdd(sd SegmentDescriptor) {
	sents := segmenter.Segment(sd.Text)
	for _, s := range sents {
		d := sd
		d.Text = s
		if _, ok := m.d2sid[d]; ok {
			continue
		}
		var tokens []string
		tokens, m.buf = tokenizer.TokensReuseSlice(s, m.buf)
		sid := m.index.Add(tokenizer.UniqueInPlace(tokens))
		m.d2sid[d] = sid
		m.sid2d[sid] = d
	}
}

func (m *TranslationMemory) Add(sd SegmentDescriptor) {
	m.m.Lock()
	defer m.m.Unlock()
	m.internalAdd(sd)
}

func (m *TranslationMemory) Delete(sd SegmentDescriptor) {
	m.m.Lock()
	defer m.m.Unlock()

	sents := segmenter.Segment(sd.Text)
	for _, s := range sents {
		d := sd
		d.Text = s
		sid, ok := m.d2sid[d]
		if !ok {
			continue
		}
		var tokens []string
		tokens, m.buf = tokenizer.TokensReuseSlice(s, m.buf)
		m.index.Delete(tokenizer.UniqueInPlace(tokens), sid)
		delete(m.sid2d, sid)
		delete(m.d2sid, d)
	}
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func computeSimilarity(a, b []string) int {
	return int(100 * (1 - float64(levenshtein.DistancePhrases(a, b))/float64(max(len(a), len(b)))))
}

func (m *TranslationMemory) Query(text string, similarity int, matchAll bool) []QueryResult {
	m.m.RLock()
	defer m.m.RUnlock()

	q := tokenizer.Tokens(text)
	var res []QueryResult //nolint:prealloc
	qs := similarity
	if matchAll {
		qs = 100
	}
	for _, sid := range m.index.Query(q, qs) {
		d := m.sid2d[sid]
		var tokens []string
		tokens, m.buf = tokenizer.TokensReuseSlice(d.Text, m.buf)
		sim := computeSimilarity(q, tokens)
		if sim < similarity {
			continue
		}
		res = append(res, QueryResult{d, sim})
	}
	sort.Slice(res, func(i, j int) bool {
		if c := res[i].Similarity - res[j].Similarity; c != 0 {
			return c > 0
		}
		if res[i].BookID-res[j].BookID != 0 {
			return res[i].BookID < res[j].BookID
		}
		return res[i].FragmentID < res[j].FragmentID
	})
	return res
}
