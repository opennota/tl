#!/usr/bin/python3
# coding: utf-8
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import json

from http import server

parser = argparse.ArgumentParser()
parser.add_argument('-p', '--port', type=int, help='A port to listen on (default: random)')
parser.add_argument('-m', '--model', type=str, help='A model to use (e.g. distiluse-base-multilingual-cased)')
args = parser.parse_args()

from sentence_transformers import SentenceTransformer
model = SentenceTransformer(args.model)
cache = {}

class Handler(server.BaseHTTPRequestHandler):
    def do_POST(self):
        data = self.rfile.read(int(self.headers['Content-Length']))
        sentences = json.loads(data)
        self.send_response(200)
        self.send_header('Content-Type', 'application/octet-stream')
        self.end_headers()
        while len(sentences) > 0:
            while len(sentences) > 0 and sentences[0] in cache:
                vector = cache[sentences[0]]
                sentences = sentences[1:]
                self.wfile.write(vector)
            n = 0
            while n < len(sentences) and not (sentences[n] in cache):
                n += 1
            if n > 0:
                vectors = model.encode(sentences[:n])
                for (i, v) in enumerate(vectors):
                    cache[sentences[i]] = v
                self.wfile.write(vectors)
                sentences = sentences[n:]

srv = server.HTTPServer(('localhost', args.port or 0), Handler)
print('http://localhost:{}'.format(srv.server_port))
srv.serve_forever()
