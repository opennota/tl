// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Package transformers provides an interface to a Python server which computes multilingual sentence embeddings using UKPLab/sentence-transformers.
package transformers

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"net/http"
)

// python3 -m ensurepip
// python3 -m pip install -U sentence-transformers
// python3 server.py -p 3124 -m LaBSE
const serverURL = "http://localhost:3124/"

// Embedding returns an embedding for p.
func Embedding(p string) ([]float32, error) {
	vectors, err := Embeddings([]string{p})
	if err != nil {
		return nil, err
	}
	return vectors[0], nil
}

// Embeddings returns embeddings for the sentences in p.
func Embeddings(p []string) ([][]float32, error) {
	var buf bytes.Buffer
	if err := json.NewEncoder(&buf).Encode(p); err != nil {
		return nil, err
	}

	resp, err := http.Post(serverURL, "application/json", &buf)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	buf.Reset()
	if _, err := buf.ReadFrom(resp.Body); err != nil {
		return nil, err
	}

	vlen := buf.Len() / len(p) / 4
	data := make([]float32, vlen*len(p))
	if err := binary.Read(&buf, binary.LittleEndian, &data); err != nil {
		return nil, err
	}

	v := make([][]float32, 0, len(p))
	for i := 0; i < vlen*len(p); i += vlen {
		v = append(v, data[i:i+vlen])
	}

	return v, nil
}
