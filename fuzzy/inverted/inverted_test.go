// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package inverted

import (
	"math/rand"
	"reflect"
	"sort"
	"sync"
	"testing"
	"time"
)

func TestQuery(t *testing.T) {
	index := make(Index)
	if id := index.Add([]string{"a", "b"}); id != 0 {
		t.Errorf("Add returned invalid ID: %d", id)
	}
	if id := index.Add([]string{"b", "c", "d"}); id != 1 {
		t.Errorf("Add returned invalid ID: %d", id)
	}
	testCases := []struct {
		tokens     []string
		similarity int
		want       []DocID
	}{
		{[]string{"a", "b"}, 50, []DocID{0, 1}},
		{[]string{"a", "b"}, 100, []DocID{0}},
		{[]string{"b", "c", "d"}, 30, []DocID{0, 1}},
		{[]string{"b", "c", "d"}, 70, []DocID{1}},
		{[]string{"b", "c", "d"}, 100, []DocID{1}},
		{[]string{"a", "c", "d"}, 30, []DocID{0, 1}},
		{[]string{"a", "c", "d"}, 66, []DocID{1}},
		{[]string{"a", "c", "d"}, 100, []DocID{}},
		{[]string{"e"}, 100, nil},
		{[]string{"a", "e"}, 50, []DocID{0}},
	}
	for _, tc := range testCases {
		got := index.Query(tc.tokens, tc.similarity)
		if !reflect.DeepEqual(got, tc.want) {
			t.Errorf("Query(%v, %d) = %v, want %v", tc.tokens, tc.similarity, got, tc.want)
		}
	}
}

func TestDelete(t *testing.T) {
	index := make(Index)
	if id := index.Add([]string{"a", "b"}); id != 0 {
		t.Errorf("Add returned invalid ID: %d", id)
	}
	if id := index.Add([]string{"b", "c", "d"}); id != 1 {
		t.Errorf("Add returned invalid ID: %d", id)
	}
	if ids := index.Query([]string{"a"}, 1.0); len(ids) != 1 || ids[0] != 0 {
		t.Error("Query failed")
	}
	index.Delete([]string{"a", "b"}, 0)
	if ids := index.Query([]string{"a"}, 1.0); len(ids) > 0 {
		t.Error("Query unexpectedly succeeded")
	}
	if id := index.Add([]string{"b", "e"}); id != 2 {
		t.Errorf("Add returned invalid ID: %d", id)
	}
	if id := index.Add([]string{"b", "f"}); id != 3 {
		t.Errorf("Add returned invalid ID: %d", id)
	}
	if ids := index.Query([]string{"b"}, 1.0); len(ids) != 3 {
		t.Error("Query failed")
	}
	index.Delete([]string{"b", "e"}, 2)
	index.Delete([]string{"b", "f"}, 3)
	if ids := index.Query([]string{"b"}, 1.0); len(ids) != 1 {
		t.Error("Query failed")
	}
}

func TestIntersectPartially(t *testing.T) {
	intersect := func(lists [][]DocID, n int) []DocID {
		result := intersectPartially(nil, lists, n)
		sort.Slice(result, func(i, j int) bool { return result[i] < result[j] })
		return result
	}

	testCases := []struct {
		n    int
		want []DocID
	}{
		{1, []DocID{1, 2, 3, 4, 5, 6, 7}},
		{2, []DocID{1, 2, 3, 4, 6}},
		{3, []DocID{1, 2, 3, 4}},
		{4, []DocID{1, 2, 3}},
		{5, []DocID{1}},
		{6, nil},
		{7, nil},
	}
	for _, tc := range testCases {
		lists := [][]DocID{
			{1},
			{1, 2},
			{1, 2, 3},
			{1, 2, 3, 4},
			{1, 2, 3, 4, 5},
			{6},
			{3, 4, 6},
			{7},
		}
		got := intersect(lists, tc.n)
		if !reflect.DeepEqual(got, tc.want) {
			t.Errorf("intersect(lists, %d) = %v; want %v", tc.n, got, tc.want)
		}
	}
}

//nolint:gosec
func TestIntersectDnC(t *testing.T) {
	rand.Seed(time.Now().UnixNano())
	for k := 0; k < 10000; k++ {
		n1, n2 := rand.Intn(3)+2, rand.Intn(3)+2
		var ids1, ids2 []DocID
		for i := 0; i < 64; i++ {
			if rand.Intn(n1) == 0 {
				continue
			}
			ids1 = append(ids1, DocID(i))
		}
		for i := 0; i < 64; i++ {
			if rand.Intn(n2) == 0 {
				continue
			}
			ids2 = append(ids2, DocID(i))
		}
		is1 := intersect(nil, ids1, ids2)
		is2 := intersectDnC(nil, ids1, ids2)
		if !reflect.DeepEqual(is1, is2) {
			t.Fatalf("intersectDnC(%v, %v) = %v; want %v", ids1, ids2, is2, is1)
		}
	}
}

//nolint:gosec
func TestBinarySearch(t *testing.T) {
	for i := 0; i < 100000; i++ {
		arr := make([]DocID, rand.Intn(32)+1)
		for i := range arr {
			arr[i] = DocID(rand.Int31n(64))
		}
		needle := arr[rand.Intn(len(arr))] + DocID(rand.Int31n(2))
		sort.Slice(arr, func(i, j int) bool { return arr[i] < arr[j] })

		i1 := sort.Search(len(arr), func(i int) bool { return arr[i] >= needle })
		i2 := binarySearch(needle, arr)
		if i1 != i2 {
			t.Fatalf("binarySearch(%d, %v) = %d; want %d", needle, arr, i1, i2)
		}
	}
}

var (
	once     sync.Once
	needle   DocID
	haystack []DocID
)

//nolint:gosec
func prepareBenchmark() {
	const n = 1024 * 1024
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < n; i++ {
		haystack = append(haystack, DocID(rand.Int31()))
	}
	sort.Slice(haystack, func(i, j int) bool { return haystack[i] < haystack[j] })
	needle = haystack[n/2]
}

func BenchmarkBinarySearch(b *testing.B) {
	b.StopTimer()
	once.Do(prepareBenchmark)
	b.StartTimer()

	for i := 0; i < b.N; i++ {
		binarySearch(needle, haystack)
	}
}

func BenchmarkSortSearch(b *testing.B) {
	b.StopTimer()
	once.Do(prepareBenchmark)
	b.StartTimer()

	for i := 0; i < b.N; i++ {
		sort.Search(len(haystack), func(i int) bool { return haystack[i] >= needle })
	}
}
