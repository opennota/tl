// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Package inverted implements an inverted index.
package inverted

import (
	"math"
	"sort"
)

type DocID uint32

type Index map[string][]DocID

const allDocIDsKey = ""

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func NewIndex() Index {
	return make(Index)
}

func (idx Index) Add(tokens []string) DocID {
	id := DocID(len(idx[allDocIDsKey]))
	for _, t := range tokens {
		if t == "" {
			continue
		}
		idxt := idx[t]
		if l := len(idxt); l == 0 || idxt[l-1] != id {
			idx[t] = append(idxt, id)
		}
	}
	idx[allDocIDsKey] = append(idx[allDocIDsKey], id)
	return id
}

func binarySearch(id DocID, ids []DocID) int {
	lo, hi := 0, len(ids)
	for lo < hi {
		m := lo + (hi-lo)>>1
		if ids[m] < id {
			lo = m + 1
		} else {
			hi = m
		}
	}
	return lo
}

func (idx Index) Delete(tokens []string, id DocID) {
	for _, t := range tokens {
		ids := idx[t]
		if len(ids) == 1 && ids[0] == id {
			delete(idx, t)
			continue
		}
		i := binarySearch(id, ids)
		if i < len(ids) && ids[i] == id {
			copy(ids[i:], ids[i+1:])
			idx[t] = ids[:len(ids)-1]
		}
	}
}

func intersect(result, a, b []DocID) []DocID {
	var i, j int
outer:
	for i < len(a) && j < len(b) {
		if a[i] == b[j] {
			result = append(result, a[i])
			i++
			j++
			if i == len(a) || j == len(b) {
				break outer
			}
		}

		for a[i] < b[j] {
			i++
			if i == len(a) {
				break outer
			}
		}

		for b[j] < a[i] {
			j++
			if j == len(b) {
				break outer
			}
		}
	}
	return result
}

func intersectDnC(result, a, b []DocID) []DocID {
	if len(a) == 0 || len(b) == 0 {
		return result
	}

	m := len(a) / 2
	v := a[m]
	i := binarySearch(v, b)
	result = intersectDnC(result, a[:m], b[:i])
	if i < len(b) {
		if b[i] == v {
			result = append(result, v)
			i++
		}
		result = intersectDnC(result, a[m+1:], b[i:])
	}
	return result
}

func appendUniq(a []DocID, x DocID) []DocID {
	for _, v := range a {
		if v == x {
			return a
		}
	}
	return append(a, x)
}

func intersectPartially(result []DocID, lists [][]DocID, n int) []DocID {
	sort.Slice(lists, func(i, j int) bool { return len(lists[i]) < len(lists[j]) })
	for len(lists) >= n {
	second:
		for _, id := range lists[0] {
			noMatch := 0
			for _, l := range lists[1:] {
				i := binarySearch(id, l)
				if i < len(l) && l[i] == id {
					continue
				}
				if noMatch++; len(lists)-noMatch < n {
					continue second
				}
			}
			result = appendUniq(result, id)
		}
		lists = lists[1:]
	}
	return result
}

func (idx Index) matchAll(tokens []string) []DocID {
	sort.Slice(tokens, func(i, j int) bool {
		a, b := tokens[i], tokens[j]
		return len(idx[a]) < len(idx[b])
	})
	docs := idx[tokens[0]]
	if len(docs) == 0 {
		return nil
	}
	result := make([]DocID, len(docs))
	for _, t := range tokens[1:] {
		if len(idx[t])/(len(docs)+1) > 25 {
			result = intersect(result[:0], docs, idx[t])
		} else {
			result = intersectDnC(result[:0], idx[t], docs)
		}
		docs = result
	}
	return docs
}

func (idx Index) matchSome(tokens []string, minTokens int) []DocID {
	lists := make([][]DocID, 0, len(tokens))
	l := 0
	for _, t := range tokens {
		l += len(idx[t])
		if len(idx[t]) > 0 {
			lists = append(lists, idx[t])
		}
	}
	l = min(l, len(idx[allDocIDsKey]))

	if len(lists) == 0 {
		return nil
	}

	return intersectPartially(make([]DocID, 0, l), lists, minTokens)
}

func (idx Index) Query(tokens []string, pct int) []DocID {
	if len(tokens) == 0 {
		return nil
	}
	n := int(math.Ceil(float64(pct) * float64(len(tokens)) / 100))
	var ids []DocID
	if n >= len(tokens) {
		ids = idx.matchAll(tokens)
	} else {
		ids = idx.matchSome(tokens, n)
	}
	sort.Slice(ids, func(i, j int) bool {
		return ids[i] < ids[j]
	})
	return ids
}
