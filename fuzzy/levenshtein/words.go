// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package levenshtein

func runeSlicesEqual(a, b []rune) bool {
	for i, x := range a {
		if x != b[i] {
			return false
		}
	}
	return true
}

// DistanceWordsMax2 returns 2-bounded Levenshtein distance between a and b.
func DistanceWordsMax2(a, b string) int {
	x := []rune(a)
	y := []rune(b)
	if len(x) > len(y) {
		x, y = y, x
	}
	if n := len(y) - len(x); n > 2 {
		return 3
	}

	for len(x) > 0 && x[0] == y[0] {
		x, y = x[1:], y[1:]
	}

	for len(x) > 0 && x[len(x)-1] == y[len(y)-1] {
		x, y = x[:len(x)-1], y[:len(y)-1]
	}

	switch {
	case len(y) == 0:
		return 0
	case len(y) == 1:
		return 1
	case len(y) == 2:
		return 2
	case len(y) == 3 && len(x) < 3:
		switch len(x) {
		case 1:
			if x[0] == y[1] {
				return 2
			}
		case 2:
			if x[0] == y[1] || x[1] == y[1] {
				return 2
			}
		}
	case len(y) == len(x):
		if runeSlicesEqual(x[1:len(x)-1], y[1:len(y)-1]) ||
			runeSlicesEqual(x[1:], y[:len(y)-1]) ||
			runeSlicesEqual(x[:len(x)-1], y[1:]) {
			return 2
		}
	case len(y)-len(x) == 1:
		z := y[1 : len(y)-1]
		if runeSlicesEqual(x[:len(x)-1], z) || runeSlicesEqual(x[1:], z) {
			return 2
		}
	default:
		if runeSlicesEqual(x, y[1:len(y)-1]) {
			return 2
		}
	}

	return 3
}

// DistanceWordsMax returns k-bounded Levenshtein distance between a and b.
func DistanceWordsMax(a, b string, k int) int {
	d, _ := DistanceWordsMaxReuseSlice(a, b, k, nil)
	return d
}

// DistanceWordsMaxReuseSlice is like DistanceWordsMax, but reuses the distance slice.
func DistanceWordsMaxReuseSlice(a, b string, k int, d []int) (int, []int) {
	x, y := []rune(a), []rune(b)
	if len(x) < len(y) {
		x, y = y, x
	}
	if len(x)-len(y) > k {
		return k + 1, d
	}

	for len(y) > 0 && x[0] == y[0] {
		x, y = x[1:], y[1:]
	}

	for len(y) > 0 && x[len(x)-1] == y[len(y)-1] {
		x, y = x[:len(x)-1], y[:len(y)-1]
	}

	if len(y) == 0 {
		return len(x), d
	}

	if cap(d) < len(y)+1 {
		d = make([]int, len(y)+1)
	}
	d = d[:len(y)+1]

	_ = d[len(y)]
	for i := 1; i <= len(y); i++ {
		d[i] = i
	}
	_ = d[len(y)]
	for i := 1; i <= len(x); i++ {
		d[0] = i
		lastdiag := i - 1
		rowmin := k + 1
		for j := 1; j <= len(y); j++ {
			olddiag := d[j]
			inc := 0
			if x[i-1] != y[j-1] {
				inc++
			}
			min := d[j] + 1
			if m := d[j-1] + 1; m < min {
				min = m
			}
			if m := lastdiag + inc; m < min {
				min = m
			}
			if min < rowmin {
				rowmin = min
			}
			d[j] = min
			lastdiag = olddiag
		}
		if rowmin > k {
			return k + 1, d
		}
	}
	m := d[len(y)]
	if m > k {
		return k + 1, d
	}
	return m, d
}
