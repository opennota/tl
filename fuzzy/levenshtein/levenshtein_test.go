// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package levenshtein

import (
	"strings"
	"testing"
)

func TestDistancePhrases(t *testing.T) {
	testCases := []struct {
		a, b string
		want int
	}{
		{"", "", 0},
		{"a", "a", 0},
		{"a", "a a", 1},
		{"a", "a a b", 2},
		{"a", "b", 1},
		{"a a a", "a b a", 1},
		{"a b", "a b", 0},
		{"a", "", 1},
		{"a a", "a", 1},
		{"a a a", "a", 2},
		{"a a a", "a", 2},
		{"k i t t e n", "s i t t i n g", 3},
		{"f l a w", "l a w n", 2},
		{"S a t u r d a y", "S u n d a y", 3},
		{"a b", "a c d b", 2},
	}

	for _, tc := range testCases {
		x := strings.Split(tc.a, " ")
		y := strings.Split(tc.b, " ")
		d := DistancePhrases(x, y)
		if d != tc.want {
			t.Errorf("DistancePhrases(%v, %v) = %d, want %d", tc.a, tc.b, d, tc.want)
		}
	}
}

var benchmarkCases = [][]string{
	{"don't", "dont"},
	{"don't", "don’t"},
	{"annoy", "annoyed"},
	{"frequent", "infrequent"},
	{"loop", "leap"},
	{"bed", "bread"},
	{"thread", "threads"},
	{"morals", "amoral"},
	{"benchmark", "alongside"},
	{"abcdef", "bcdefg"},
}

func BenchmarkDistancePhrases(b *testing.B) {
	b.StopTimer()
	cases := make([][][]string, 0, len(benchmarkCases))
	for _, tc := range benchmarkCases {
		cases = append(cases, [][]string{
			strings.Split(tc[0], ""),
			strings.Split(tc[1], ""),
		})
	}
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		tc := cases[i%len(cases)]
		DistancePhrases(tc[0], tc[1])
	}
}
