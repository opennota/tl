// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package levenshtein

// DistancePhrases returns the Levenshtein distance between the two string slices.
func DistancePhrases(s, t []string) int {
	d, _ := DistancePhrasesReuseSlice(s, t, nil)
	return d
}

// DistancePhrasesReuseSlice is like DistancePhrases, but reuses the distance slice.
func DistancePhrasesReuseSlice(x, y []string, d []int) (int, []int) {
	if len(x) < len(y) {
		x, y = y, x
	}

	if len(y) == 0 {
		return len(x), d
	}

	for len(y) > 0 && x[0] == y[0] {
		x, y = x[1:], y[1:]
	}

	for len(y) > 0 && x[len(x)-1] == y[len(y)-1] {
		x, y = x[:len(x)-1], y[:len(y)-1]
	}

	if len(y) == 0 {
		return len(x), d
	}

	if cap(d) < len(y)+1 {
		d = make([]int, len(y)+1)
	}
	d = d[:len(y)+1]

	_ = d[len(y)]
	for i := 1; i <= len(y); i++ {
		d[i] = i
	}
	_ = d[len(y)]
	for i := 1; i <= len(x); i++ {
		d[0] = i
		lastdiag := i - 1
		for j := 1; j <= len(y); j++ {
			olddiag := d[j]
			inc := 0
			if x[i-1] != y[j-1] {
				inc++
			}
			min := d[j] + 1
			if m := d[j-1] + 1; m < min {
				min = m
			}
			if m := lastdiag + inc; m < min {
				min = m
			}
			d[j] = min
			lastdiag = olddiag
		}
	}
	return d[len(y)], d
}
