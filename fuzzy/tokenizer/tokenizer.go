// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Package tokenizer provides a way to extract tokens (words) from a sentence.
package tokenizer

import (
	"sort"
	"strings"
	"unicode"
	"unicode/utf8"
)

func hasLettersOrDigits(s string) bool {
	for _, r := range s {
		if unicode.In(r, unicode.Letter, unicode.Digit) {
			return true
		}
	}
	return false
}

func nonword(r rune) bool {
	return !unicode.In(r, unicode.Letter, unicode.Digit)
}

func trim(s string) string {
	return strings.TrimFunc(s, nonword)
}

func findNextToken(s string, buf []rune) (string, []rune, int) {
	buf = buf[:0]
	for i, r := range s {
		if r == '…' || unicode.In(r, unicode.Zs, unicode.Pd) {
			if len(buf) > 0 {
				return string(buf), buf, i + utf8.RuneLen(r)
			}
			continue
		}
		buf = append(buf, unicode.ToLower(r))
	}
	return string(buf), buf, len(s)
}

// TokensReuseSlice returns lowercase tokens (words) containing in s.
func TokensReuseSlice(s string, buf []rune) ([]string, []rune) {
	var ws []string
	var w string
	var i int
	for len(s) > 0 {
		w, buf, i = findNextToken(s, buf)
		s = s[i:]
		w = trim(w)
		if !hasLettersOrDigits(w) {
			continue
		}
		ws = append(ws, w)
	}
	return ws, buf
}

// Tokens returns lowercase tokens (words) containing in s.
func Tokens(s string) []string {
	ws, _ := TokensReuseSlice(s, nil)
	return ws
}

// Unique returns a slice of sorted unique tokens.
func Unique(tokens []string) []string {
	u := make([]string, len(tokens))
	copy(u, tokens)
	sort.Strings(u)
	v := u[:1]
	for i := 1; i < len(u); i++ {
		if u[i] == v[len(v)-1] {
			continue
		}
		v = append(v, u[i])
	}
	return v
}

// UniqueInPlace returns a slice of sorted unique tokens, reusing the tokens slice.
func UniqueInPlace(tokens []string) []string {
	sort.Strings(tokens)
	v := tokens[:1]
	for i := 1; i < len(tokens); i++ {
		if tokens[i] == v[len(v)-1] {
			continue
		}
		v = append(v, tokens[i])
	}
	return v
}
