// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package tokenizer

import "testing"

func slicesEqual(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i, s := range a {
		if s != b[i] {
			return false
		}
	}
	return true
}

func TestTokens(t *testing.T) {
	const text = "Three sentences. Sentences three. +Don't_—do–this at home. («Oops-poops!») '' 2020 1980s"
	words := Tokens(text)
	want := []string{"three", "sentences", "sentences", "three", "don't", "do", "this", "at", "home", "oops", "poops", "2020", "1980s"}
	if !slicesEqual(words, want) {
		t.Errorf("Tokens(%q):\nwant: %#v\n got: %#v", text, want, words)
	}
}

func TestUnique(t *testing.T) {
	input := []string{"three", "sentences", "don't", "do", "this", "at", "home", "oops", "2020", "1980s"}
	words := Unique(input)
	want := []string{"1980s", "2020", "at", "do", "don't", "home", "oops", "sentences", "this", "three"}
	if !slicesEqual(words, want) {
		t.Errorf("UniqueTokens(%v):\nwant: %#v\n got: %#v", input, want, words)
	}
}
