// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package segmenter

// omegat/src/org/omegat/core/segmentation/defaultRules.srx
var rules = []rule{
	// default OmegaT rules
	{false, `...`, `\s\P{Lu}`},
	{true, `^\s*\p{Nd}+[\p{Nd}\.\)\]]+`, `\s+\p{Lu}`},
	{true, `[.?!]+`, `\s`},

	// English OmegaT rules
	{false, `etc.`, `\s+\P{Lu}`},
	{false, `Dr.`, `\s`},
	{false, `U.K.`, `\s`},
	{false, `M.`, `\s`},
	{false, `Mr.`, `\s`},
	{false, `Mrs.`, `\s`},
	{false, `Ms.`, `\s`},
	{false, `Prof.`, `\s`},
	{false, `(?i)e\.g\.`, `\s`},
	{false, `(?i)i\.e\.`, `\s`},
	{false, `resp.`, `\s`},
	{false, `\stel\.`, `\s`},
	{false, `(?i)fig\.`, `\s`},
	{false, `St.`, `\s`},
	{false, `\s[A-Z]\.`, `\s`},
	{false, `[apAP]\.?[mM]\.`, `\s[a-z]`},
	{false, `Mt.`, `\s`},
	{false, `No.`, `\s\d`},
	{false, `[Aa]pprox\.`, `\s`},
	{false, `\d\smi?n\.`, `\s`},
	{false, `\d\ssec\.`, `\s`},
	{false, `\s[vV][sS]?\.`, `\s`},

	// Russian OmegaT rules.
	{false, `(?i)т\.[ек]\.`, `\s`},
	{false, `рт\.\s?ст\.`, `\s`},
}
