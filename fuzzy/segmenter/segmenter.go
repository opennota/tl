// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Package segmenter provides a way to split texts into sentences.
package segmenter

import (
	"regexp"
	"sort"
	"strings"
	"unicode"
	"unicode/utf8"
)

type findAllStringIndexer interface {
	FindAllStringIndex(string, int) [][]int
}

type matchStringer interface {
	MatchString(s string) bool
}

type rule struct {
	isBreak bool
	before  string
	after   string
}

type compiledRule struct {
	isBreak     bool
	beforeBreak findAllStringIndexer
	afterBreak  matchStringer
}

type str string

func (s str) FindAllStringIndex(a string, _ int) [][]int {
	var locs [][]int
	i := 0
	for {
		idx := strings.Index(a, string(s))
		if idx == -1 {
			break
		}
		end := idx + len(s)
		locs = append(locs, []int{i + idx, i + end})
		i += end
		a = a[end:]
	}
	return locs
}

type spaceMatcher bool

func (m spaceMatcher) MatchString(s string) bool {
	for _, r := range s {
		return unicode.IsSpace(r)
	}
	return false
}

var compiledRules = compile(rules)

func compile(rules []rule) []compiledRule {
	compiled := make([]compiledRule, 0, len(rules))
	for _, r := range rules {
		c := compiledRule{
			isBreak: r.isBreak,
		}
		if strings.ContainsAny(r.before, `\?*+[`) {
			c.beforeBreak = regexp.MustCompile(r.before)
		} else {
			c.beforeBreak = str(r.before)
		}
		if r.after != "" {
			if r.after == `\s` {
				c.afterBreak = spaceMatcher(true)
			} else {
				c.afterBreak = regexp.MustCompile("^" + r.after)
			}
		}
		compiled = append(compiled, c)
	}
	return compiled
}

func (r *compiledRule) getBreaks(s string) []int {
	var breaks []int
	locs := r.beforeBreak.FindAllStringIndex(s, -1)
	for _, loc := range locs {
		if r.afterBreak == nil || r.afterBreak.MatchString(s[loc[1]:]) {
			breaks = append(breaks, loc[1])
		}
	}
	return breaks
}

func adjustLoc(s string, start, end int) [2]int {
	i := start
	for {
		r, size := utf8.DecodeRuneInString(s[i:])
		if size == 0 || !unicode.IsSpace(r) {
			break
		}
		i += size
	}
	j := end
	for {
		r, size := utf8.DecodeLastRuneInString(s[:j])
		if size == 0 || !unicode.IsSpace(r) {
			break
		}
		j -= size
	}
	return [2]int{i, j}
}

func getBreaks(s string) []int {
	breakPositions := make(map[int]struct{})
	nobreakPositions := make(map[int]struct{})
	for _, r := range compiledRules {
		ps := r.getBreaks(s)
		m := nobreakPositions
		if r.isBreak {
			m = breakPositions
		}
		for _, p := range ps {
			m[p] = struct{}{}
		}
	}
	for p := range nobreakPositions {
		delete(breakPositions, p)
	}
	bp := make([]int, 0, len(breakPositions))
	for p := range breakPositions {
		bp = append(bp, p)
	}
	sort.Ints(bp)
	return bp
}

func hasLettersOrDigits(s string) bool {
	for _, r := range s {
		if unicode.In(r, unicode.Letter, unicode.Digit) {
			return true
		}
	}
	return false
}

// Segment breaks a text into sentences.
func Segment(s string) []string {
	br := getBreaks(s)
	ss := make([]string, 0, len(br))
	i := 0
	for _, p := range br {
		st := strings.TrimSpace(s[i:p])
		i = p
		if hasLettersOrDigits(st) {
			ss = append(ss, st)
		}
	}
	st := strings.TrimSpace(s[i:])
	if hasLettersOrDigits(st) {
		ss = append(ss, st)
	}
	return ss
}

// SegmentWithLocations breaks a text into sentences. It also returns the locations of the sentences in the original string.
func SegmentWithLocations(s string) ([]string, [][2]int) {
	br := getBreaks(s)
	ss := make([]string, 0, len(br))
	locs := make([][2]int, 0, len(br))
	i := 0
	for _, p := range br {
		st := strings.TrimSpace(s[i:p])
		if hasLettersOrDigits(st) {
			ss = append(ss, st)
			locs = append(locs, adjustLoc(s, i, p))
		}
		i = p
	}
	st := strings.TrimSpace(s[i:])
	if hasLettersOrDigits(st) {
		ss = append(ss, st)
		locs = append(locs, adjustLoc(s, i, len(s)))
	}
	return ss, locs
}
