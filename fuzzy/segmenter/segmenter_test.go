// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package segmenter

import "testing"

func TestSegment(t *testing.T) {
	text := "This is a sentence.  This is another one ... continued.  Shouldn't break after etc. here."
	sentences := Segment(text)
	want := []string{
		"This is a sentence.",
		"This is another one ... continued.",
		"Shouldn't break after etc. here.",
	}
	if len(sentences) != len(want) {
		t.Fatalf("want %d sentences, got %d", len(want), len(sentences))
	}
	for i, s := range sentences {
		if s != want[i] {
			t.Errorf("want sentence #%d to be %s, got %s", i, want[i], s)
		}
	}
}

func TestSegmentWithLocations(t *testing.T) {
	text := "  This is a sentence.  This is another one ... continued.  Shouldn't break after etc. here.  "
	sentences, locs := SegmentWithLocations(text)
	want := []string{
		"This is a sentence.",
		"This is another one ... continued.",
		"Shouldn't break after etc. here.",
	}
	if len(sentences) != len(want) {
		t.Fatalf("want %d sentences, got %d", len(want), len(sentences))
	}
	if len(locs) != len(want) {
		t.Fatalf("want %d locations, got %d", len(want), len(locs))
	}
	for i, s := range sentences {
		if s != want[i] {
			t.Errorf("want sentence #%d to be %s, got %s", i, want[i], s)
		}
		if text[locs[i][0]:locs[i][1]] != s {
			t.Errorf("invalid location")
		}
	}
}
