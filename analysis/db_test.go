// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package analysis_test

import (
	"reflect"
	"testing"

	"gitlab.com/opennota/tl/analysis"
)

func Test(t *testing.T) {
	db, err := analysis.New("file::memory:?cache=shared")
	if err != nil {
		t.Fatal(err)
	}
	if err := db.Add("Четыре чёрненьких чумазеньких чертёнка чертили чёрными чернилами чертёж."); err != nil {
		t.Fatal(err)
	}
	if got, err := db.WordCount(); err != nil {
		t.Fatal(err)
	} else if got != 8 {
		t.Fatalf("want %d words, got %d", 8, got)
	}
	f1, err := db.Words(0, 0)
	if err != nil {
		t.Fatal(err)
	}
	if want := []analysis.WordWithFrequency{{"черненьких", 1}, {"чернилами", 1}, {"черными", 1}, {"чертеж", 1}, {"чертенка", 1}, {"чертили", 1}, {"четыре", 1}, {"чумазеньких", 1}}; !reflect.DeepEqual(f1, want) {
		t.Errorf("Words: want %v, got %v", want, f1)
	}
}
