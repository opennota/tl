// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package analysis

import (
	"database/sql"
	"fmt"
	"sort"
	"strconv"
	"strings"
	"unicode"
	"unicode/utf8"

	"github.com/jmoiron/sqlx"
	_ "modernc.org/sqlite" //nolint:revive
)

type DB struct {
	sdb *sqlx.DB
}

type WordWithFrequency struct {
	Word string
	Freq int
}

type NgramWithScore struct {
	Ngram string
	Freq  int
	Score float64
}

type Collocation struct {
	W1, W2 string
	Freq   int
	Score  float64
}

func New(dsn string) (_ *DB, err error) {
	var db *sqlx.DB
	db, err = sqlx.Connect("sqlite", dsn)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err != nil {
			db.Close()
		}
	}()
	if _, err := db.Exec(`
				CREATE TABLE IF NOT EXISTS corpus (
					w5_id   INTEGER,
					w5_fpos INTEGER,
					w1      INTEGER,
					w2      INTEGER,
					w3      INTEGER,
					w4      INTEGER,
					w5      INTEGER,
					w6      INTEGER,
					w7      INTEGER,
					w8      INTEGER,
					w9      INTEGER
				);
				CREATE TABLE IF NOT EXISTS lexicon (
					word_id INTEGER PRIMARY KEY AUTOINCREMENT,
					w       TEXT COLLATE BINARY,
					norm    TEXT COLLATE BINARY,
					pos     INTEGER,
					UNIQUE (w)
				);
				CREATE INDEX IF NOT EXISTS corpus_w1_idx
					ON corpus (w1);
				CREATE INDEX IF NOT EXISTS corpus_w2_idx
					ON corpus (w2);
				CREATE INDEX IF NOT EXISTS corpus_w3_idx
					ON corpus (w3);
				CREATE INDEX IF NOT EXISTS corpus_w4_idx
					ON corpus (w4);
				CREATE INDEX IF NOT EXISTS corpus_w5_idx
					ON corpus (w5);
				CREATE INDEX IF NOT EXISTS corpus_w6_idx
					ON corpus (w6);
				CREATE INDEX IF NOT EXISTS corpus_w7_idx
					ON corpus (w7);
				CREATE INDEX IF NOT EXISTS corpus_w8_idx
					ON corpus (w8);
				CREATE INDEX IF NOT EXISTS corpus_w9_idx
					ON corpus (w9);
				CREATE INDEX IF NOT EXISTS lexicon_norm_idx
					ON lexicon (norm);
				CREATE INDEX IF NOT EXISTS lexicon_pos_idx
					ON lexicon (pos);

	`); err != nil {
		return nil, err
	}
	return &DB{
		sdb: db,
	}, err
}

func (db *DB) Close() error { return db.sdb.Close() }

func (db *DB) Drop() error {
	_, err := db.sdb.Exec(`
			DROP TABLE corpus;
			DROP TABLE lexicon;
	`)
	return err
}

var normalizer = strings.NewReplacer("ё", "е", "\u0301", "")

func (db *DB) Add(text string) error {
	tx, err := db.sdb.Beginx()
	if err != nil {
		return err
	}
	defer tx.Rollback() //nolint:errcheck

	rowidStmt, err := tx.Preparex("SELECT last_insert_rowid()")
	if err != nil {
		return err
	}
	defer rowidStmt.Close()

	lexiconInsertStmt, err := tx.Preparex("INSERT INTO lexicon (w, norm, pos) VALUES (?, ?, ?)")
	if err != nil {
		return err
	}
	defer lexiconInsertStmt.Close()

	corpusInsertStmt, err := tx.Preparex("INSERT INTO corpus (w5_id, w5_fpos, w1, w2, w3, w4, w5, w6, w7, w8, w9) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
	if err != nil {
		return err
	}
	defer corpusInsertStmt.Close()

	lexiconSelectStmt, err := tx.Preparex("SELECT word_id FROM lexicon WHERE w = ?")
	if err != nil {
		return err
	}
	defer lexiconSelectStmt.Close()

	var n int
	_ = tx.Get(&n, "SELECT MAX(w5_id) FROM corpus")

	s := NewWordScanner(strings.NewReader(text))
	var w [9]int
	var p [9]int64
	wordIDs := make(map[string]int)
	i := 4
	for s.Scan() {
		fpos, _, word := s.Word()
		lcWord := normalizer.Replace(strings.ToLower(word))
		wordID, ok := wordIDs[lcWord]
		if !ok {
			err := lexiconSelectStmt.Get(&wordID, lcWord)
			if err == sql.ErrNoRows {
				norm, pos := parseWord(lcWord)
				if _, err := lexiconInsertStmt.Exec(lcWord, norm, pos); err != nil {
					return err
				}
				if err := rowidStmt.Get(&wordID); err != nil {
					return err
				}
			} else if err != nil {
				return err
			}
			wordIDs[lcWord] = wordID
		}
		w[i] = wordID
		p[i] = fpos
		if i < 8 {
			i++
		} else {
			n++
			if _, err := corpusInsertStmt.Exec(n, p[4], w[0], w[1], w[2], w[3], w[4], w[5], w[6], w[7], w[8]); err != nil {
				return err
			}
			copy(w[:], w[1:])
			copy(p[:], p[1:])
			w[8] = 0
		}
	}
	if err := s.Err(); err != nil {
		return err
	}
	for w[4] != 0 {
		n++
		if _, err := corpusInsertStmt.Exec(n, p[4], w[0], w[1], w[2], w[3], w[4], w[5], w[6], w[7], w[8]); err != nil {
			return err
		}
		copy(w[:], w[1:])
		copy(p[:], p[1:])
		w[8] = 0
	}

	return tx.Commit()
}

func (db *DB) WordCount() (int, error) {
	var size int
	if err := db.sdb.Get(&size, "SELECT COUNT(*) FROM corpus"); err != nil {
		return 0, err
	}
	return size, nil
}

func sortedWords(words []WordWithFrequency) []WordWithFrequency {
	sort.Slice(words, func(i, j int) bool {
		if d := words[i].Freq - words[j].Freq; d != 0 {
			return d > 0
		}
		return words[i].Word < words[j].Word
	})
	return words
}

func (db *DB) Words(minFrequency, minLength int) ([]WordWithFrequency, error) {
	var result []WordWithFrequency
	cond := ""
	if minLength > 1 {
		cond = fmt.Sprintf("AND length(l.w) >= %d", minLength)
	}
	if err := db.sdb.Select(&result, "SELECT l.w AS word, COUNT(c.w5_id) AS freq FROM lexicon l, corpus c WHERE l.word_id = c.w5 "+cond+" GROUP BY 1 HAVING COUNT(*) >= ?", minFrequency); err != nil {
		return nil, err
	}

	return sortedWords(result), nil
}

func (db *DB) Norms(minFrequency, minLength int) ([]WordWithFrequency, error) {
	var result []WordWithFrequency
	cond := ""
	if minLength > 1 {
		cond = fmt.Sprintf("AND length(l.w) >= %d", minLength)
	}
	if err := db.sdb.Select(&result, "SELECT l.norm AS word, COUNT(c.w5_id) AS freq FROM lexicon l, corpus c WHERE l.word_id = c.w5 "+cond+" GROUP BY 1 HAVING COUNT(*) >= ?", minFrequency); err != nil {
		return nil, err
	}

	return sortedWords(result), nil
}

func (db *DB) WordFrequencyMap() (map[string]int, error) {
	wordFreq := make(map[string]int)
	rows, err := db.sdb.Queryx("SELECT l.w, COUNT(c.w5_id) FROM lexicon l, corpus c WHERE l.word_id = c.w5 GROUP BY 1")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var w string
		var f int
		if err := rows.Scan(&w, &f); err != nil {
			return nil, err
		}
		wordFreq[w] = f
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return wordFreq, nil
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func (db *DB) Ngrams(n int, scoreFunc ScoreFunc) ([]NgramWithScore, error) {
	corpusSize, err := db.WordCount()
	if err != nil {
		return nil, err
	}
	wordFreq, err := db.WordFrequencyMap()
	if err != nil {
		return nil, err
	}

	var cols []string
	var from []string
	var where []string
	var groupby []string
	for i, off := 1, max(5-n, 0); i <= n; i++ {
		s := strconv.Itoa(i)
		alias := "l" + s
		cols = append(cols, alias+".w")
		from = append(from, alias)
		where = append(where, alias+".word_id = c.w"+strconv.Itoa(i+off))
		groupby = append(groupby, s)
	}

	sql := "SELECT " + strings.Join(cols, ", ") + ", COUNT(c.w5_id) FROM lexicon " +
		strings.Join(from, ", lexicon ") + ", corpus c WHERE " + strings.Join(where, " AND ") +
		" GROUP BY " + strings.Join(groupby, ", ") + " HAVING COUNT(*) >= 2"
	rows, err := db.sdb.Queryx(sql)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var result []NgramWithScore
	for rows.Next() {
		slice, err := rows.SliceScan()
		if err != nil {
			return nil, err
		}
		f := int(slice[len(slice)-1].(int64))
		wf := 1
		s := ""
		for i := 0; i < n; i++ {
			if i > 0 {
				s += " "
			}
			w := slice[i].(string)
			wf *= wordFreq[w]
			s += w
		}
		result = append(result, NgramWithScore{s, f, scoreFunc(corpusSize, f, wf, 1)})
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	sort.Slice(result, func(i, j int) bool { return result[i].Score > result[j].Score })

	return result, nil
}

func firstLetter(s string) rune {
	for _, r := range s {
		return r
	}
	return utf8.RuneError
}

func patternToSubquery(p string, ignoreOneLetterWords bool) (sq string, args []interface{}) {
	if p == "" || p == "*" {
		if ignoreOneLetterWords {
			return "length(%.w) > 1", nil
		}
		return "1=1", nil
	}
	if pos := posMapEx[p]; pos > 0 {
		sq = "%.pos & ? != 0 AND %.pos != ?"
		if ignoreOneLetterWords {
			sq += " AND length(%.w) > 1"
		}
		args = append(args, pos, Unknown)
	} else if unicode.IsUpper(firstLetter(p)) {
		sq = "%.norm = ?"
		args = append(args, strings.ToLower(p))
	} else {
		sq = "%.w = ?"
		args = append(args, p)
	}

	return sq, args
}

func (db *DB) Collocations(pat1, pat2 string, left, right int, scoreFunc ScoreFunc) ([]Collocation, error) {
	corpusSize, err := db.WordCount()
	if err != nil {
		return nil, err
	}
	wordFreq, err := db.WordFrequencyMap()
	if err != nil {
		return nil, err
	}

	sqlFmt := `
		SELECT l1.w AS w1, l2.w AS w2, COUNT(*) AS freq
		FROM corpus c, lexicon l1, lexicon l2
		WHERE c.w5 = l1.word_id
			AND (%s)
			AND (%s)
			AND (%s)
		GROUP BY l1.w, l2.w`
	var args []interface{}
	sq1, arg := patternToSubquery(pat1, true)
	args = append(args, arg...)
	sq2, arg := patternToSubquery(pat2, true)
	args = append(args, arg...)
	var sq3 []string
	for i := 5 - left; i <= 5+right; i++ {
		if i == 5 {
			continue
		}
		sq3 = append(sq3, fmt.Sprintf("l2.word_id = c.w%d", i))
	}
	sql := fmt.Sprintf(sqlFmt,
		strings.ReplaceAll(sq1, "%", "l1"),
		strings.ReplaceAll(sq2, "%", "l2"),
		strings.Join(sq3, " OR "),
	)
	var result []Collocation
	if err := db.sdb.Select(&result, sql, args...); err != nil {
		return nil, err
	}
	span := left + right
	for i, c := range result {
		result[i].Score = scoreFunc(corpusSize, c.Freq, wordFreq[c.W1]*wordFreq[c.W2], span)
	}
	sort.Slice(result, func(i, j int) bool { return result[i].Score > result[j].Score })
	return result, nil
}
