// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package analysis

import (
	"strings"
	"sync"

	"github.com/dchest/stemmer/porter2"
	"gitlab.com/opennota/morph"
	"gitlab.com/opennota/stemka"
)

var (
	once     sync.Once
	useMorph bool
)

func initMorph() {
	err := morph.Init()
	useMorph = err == nil || err == morph.ErrAlreadyInitialized
}

const (
	Noun uint32 = 1 << iota //  NOUN  СУЩ      имя существительное
	AdjF                    //  ADJF  ПРИЛ     имя прилагательное (полное)
	AdjS                    //  ADJS  КР_ПРИЛ  имя прилагательное (краткое)
	Comp                    //  COMP  КОМП     компаратив
	Verb                    //  VERB  ГЛ       глагол (личная форма)
	Infn                    //  INFN  ИНФ      глагол (инфинитив)
	PrtF                    //  PRTF  ПРИЧ     причастие (полное)
	PrtS                    //  PRTS  КР_ПРИЧ  причастие (краткое)
	Grnd                    //  GRND  ДЕЕПР    деепричастие
	Numr                    //  NUMR  ЧИСЛ     числительное
	Advb                    //  ADVB  Н        наречие
	Npro                    //  NPRO  МС       местоимение-существительное
	Pred                    //  PRED  ПРЕДК    предикатив
	Prep                    //  PREP  ПР       предлог
	Conj                    //  CONJ  СОЮЗ     союз
	Prcl                    //  PRCL  ЧАСТ     частица
	Intj                    //  INTJ  МЕЖД     междометие

	Adj      = AdjF | AdjS
	Prt      = PrtF | PrtS
	VerbPlus = Verb | Infn | PrtF | PrtS | Grnd
	Unknown  = AdjF | AdjS | Comp | Verb | Infn | PrtF | PrtS | Grnd | Numr | Advb | Npro | Pred | Prep | Conj | Prcl | Intj
)

var posMap = map[string]uint32{
	"NOUN": Noun,
	"ADJF": AdjF,
	"ADJS": AdjS,
	"COMP": Comp,
	"VERB": Verb,
	"INFN": Infn,
	"PRTF": PrtF,
	"PRTS": PrtS,
	"GRND": Grnd,
	"NUMR": Numr,
	"ADVB": Advb,
	"NPRO": Npro,
	"PRED": Pred,
	"PREP": Prep,
	"CONJ": Conj,
	"PRCL": Prcl,
	"INTJ": Intj,
}

var posMapEx = map[string]uint32{
	"NOUN": Noun,
	"ADJF": AdjF,
	"ADJS": AdjS,
	"COMP": Comp,
	"VERB": Verb | Infn,
	"INFN": Infn,
	"PRTF": PrtF,
	"PRTS": PrtS,
	"GRND": Grnd,
	"NUMR": Numr,
	"ADVB": Advb,
	"NPRO": Npro,
	"PRED": Pred,
	"PREP": Prep,
	"CONJ": Conj,
	"PRCL": Prcl,
	"INTJ": Intj,

	"N":     Noun,
	"ADJ":   Adj,
	"ADJ+":  Adj | Prt,
	"V":     Verb | Infn,
	"V+":    VerbPlus,
	"VERB+": VerbPlus,
	"INF":   Infn,
	"PRT":   Prt,
	"NUM":   Numr,
	"ADV":   Advb,
	"INT":   Intj,
}

var yoReplacer = strings.NewReplacer("ё", "е")

func russianWord(w string) bool {
	for _, r := range w {
		return r >= 'а' && r <= 'я' || r >= 'А' && r <= 'Я'
	}
	panic("unreachable")
}

func parseWord(word string) (string, uint32) {
	if russianWord(word) {
		return parseRuWord(word)
	}
	return porter2.Stemmer.Stem(word), 0
}

func parseRuWord(word string) (string, uint32) {
	once.Do(initMorph)

	var norms, tags []string
	if useMorph {
		_, norms, tags = morph.XParse(word)
	}
	if len(norms) == 0 {
		return stemka.MinStem(word), Unknown
	}
	var pos uint32
	for _, t := range tags {
		if i := strings.IndexAny(t, " ,"); i > 0 {
			pos |= posMap[t[:i]]
		} else {
			pos |= posMap[t]
		}
	}
	return yoReplacer.Replace(norms[0]), pos
}
