// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package analysis

import (
	"bufio"
	"io"
	"unicode"
)

func NewWordScanner(r io.Reader) *WordScanner {
	var rs io.RuneScanner
	if rr, ok := r.(io.RuneScanner); ok {
		rs = rr
	} else {
		rs = bufio.NewReader(r)
	}
	return &WordScanner{rs: rs}
}

type WordScanner struct {
	rs  io.RuneScanner
	pos int64
	wp  int64
	buf []rune
	err error
}

func (s *WordScanner) Scan() bool {
	if s.err != nil {
		return false
	}
	s.buf = s.buf[:0]

	st := 0
	for {
		r, size, err := s.rs.ReadRune()
		if err != nil {
			s.err = err
			return err == io.EOF && len(s.buf) > 0
		}
		isLetter := unicode.IsLetter(r)

		switch st {
		case 0:
			if isLetter {
				s.wp = s.pos
				s.buf = append(s.buf, r)
				st = 1
			}
		case 1:
			if r != 0x301 /* stress mark */ && !isLetter {
				if err := s.rs.UnreadRune(); err != nil {
					s.err = err
					return false
				}
				return true
			}
			s.buf = append(s.buf, r)
		}

		s.pos += int64(size)
	}
}

func (s *WordScanner) Word() (int64, int64, string) {
	return s.wp, s.pos, string(s.buf)
}

func (s *WordScanner) Err() error {
	if s.err == io.EOF {
		return nil
	}
	return s.err
}
