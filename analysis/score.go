// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package analysis

import "math"

type ScoreFunc func(int, int, int, int) float64

// https://www.english-corpora.org/mutualInformation.asp
//
// In our corpora, Mutual Information is calculated as follows:
//
// MI = log ( (AB * sizeCorpus) / (A * B * span) ) / log (2)
//
// Suppose we are calculating the MI for the collocate color near purple in BNC.
//
// A = frequency of node word (e.g. purple): 1262
// B = frequency of collocate (e.g. color): 115
// AB = frequency of collocate near the node word (e.g. color near purple): 24
// sizeCorpus= size of corpus (# words; in this case the BNC): 96,263,399
// span = span of words (e.g. 3 to left and 3 to right of node word): 6
// log (2) is literally the log10 of the number 2: .30103
//
// MI = 11.37 = log ( (24 * 96,263,399) / (1262 * 115 * 6) ) / .30103
func MI(size, ab, amulb, span int) float64 {
	return math.Log2(float64(ab) * float64(size) / float64(amulb*span))
}

func MI3(size, ab, amulb, span int) float64 {
	return math.Log2(math.Pow(float64(ab), 3) * float64(size) / float64(amulb*span))
}
