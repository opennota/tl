// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/boltdb/bolt"
	"github.com/gorilla/mux"
	"github.com/gorilla/securecookie"
	"github.com/gorilla/sessions"
	"gitlab.com/opennota/tl/diff"
	"gitlab.com/opennota/tl/fuzzy/tm"
)

type App struct {
	db       DB
	tm       *tm.TranslationMemory
	tmServer string

	withSentenceTransformers bool
}

var (
	secret = securecookie.GenerateRandomKey(64)
	store  = sessions.NewCookieStore(secret)

	rNewline = regexp.MustCompile(`[\r\n]+`)
)

func logError(err error) {
	log.Println("ERR", err)
}

func internalError(w http.ResponseWriter, err error) {
	logError(err)
	http.Error(w, "internal server error", http.StatusInternalServerError)
}

func u64(s string) (uint64, error) {
	return strconv.ParseUint(s, 10, 64)
}

func split(s string) []string {
	lines := rNewline.Split(s, -1)
	result := lines[:0]
	for _, l := range lines {
		l = strings.TrimSpace(l)
		if l == "" {
			continue
		}
		result = append(result, l)
	}
	return result
}

func (a *App) Index(w http.ResponseWriter, r *http.Request) {
	c, err := r.Cookie("sort-by")
	sortBy := "by-activity"
	if err == nil {
		sortBy = c.Value
	}

	var books []Book
	if sortBy == "by-title" {
		books, err = a.db.BooksByTitle()
	} else {
		books, err = a.db.BooksByActivity()
	}
	if err != nil {
		internalError(w, err)
		return
	}

	w.Header().Set("Content-Type", "text/html")
	if err := indexTmpl.Execute(w, struct {
		Books  []Book
		SortBy string
	}{
		books,
		sortBy,
	}); err != nil {
		logError(err)
	}
}

func divRoundUp(a, b int) int {
	c := a / b
	if a%b != 0 {
		c++
	}
	return c
}

func redirectToPageNumber(w http.ResponseWriter, r *http.Request, page int) {
	q := r.URL.Query()
	if page > 1 {
		q["page"] = []string{strconv.Itoa(page)}
	} else {
		delete(q, "page")
	}
	if len(q) > 0 {
		http.Redirect(w, r, r.URL.Path+"?"+q.Encode(), http.StatusSeeOther)
	} else {
		http.Redirect(w, r, r.URL.Path, http.StatusSeeOther)
	}
}

func (a *App) Book(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	bid, err := u64(vars["book_id"])
	if err != nil {
		http.NotFound(w, r)
		return
	}

	switch r.Method {
	case "GET":
		page := 1
		if p := r.FormValue("page"); p != "" {
			page, err = strconv.Atoi(p)
			if err != nil || page <= 0 {
				redirectToPageNumber(w, r, 1)
				return
			}
		}
		const size = 50
		off := (page - 1) * size

		var book Book
		var err error
		switch r.FormValue("f") {
		case "u":
			book, err = a.db.BookWithTranslations(bid, off, size, fUntranslated)
		case "c":
			book, err = a.db.BookWithTranslations(bid, off, size, fCommented)
		case "s", "-s":
			book, err = a.db.BookWithTranslations(bid, off, size, fStarred, r.FormValue("f"))
		case "m":
			book, err = a.db.BookWithTranslations(bid, off, size, fMarked)
		case "2":
			book, err = a.db.BookWithTranslations(bid, off, size, fWithTwoOrMoreVersions)
		case "o":
			what := r.FormValue("to")
			if what == "" {
				http.Redirect(w, r, r.URL.Path, http.StatusFound)
				return
			}
			book, err = a.db.BookWithTranslations(bid, off, size, fOriginalContains, what)
		case "t":
			what := r.FormValue("tt")
			if what == "" {
				http.Redirect(w, r, r.URL.Path, http.StatusFound)
				return
			}
			book, err = a.db.BookWithTranslations(bid, off, size, fTranslationContains, what)
		case "w":
			book, err = a.db.BookWithTranslations(bid, off, size, fTranslatedWhen, r.FormValue("when"))
		default:
			book, err = a.db.BookWithTranslations(bid, off, size, fNone)
			if err == nil && book.LastVisitedPage != page {
				if err := a.db.UpdateLastVisitedPage(bid, page); err != nil {
					logError(err)
				}
			}
		}
		if err != nil {
			switch err {
			case ErrNotFound:
				http.NotFound(w, r)
			case ErrInvalidOffset:
				redirectToPageNumber(w, r, divRoundUp(len(book.FragmentsIDs), size))
			default:
				internalError(w, err)
			}
			return
		}

		pg := Pagination{
			url:          r.URL,
			PageNumber:   page,
			TotalItems:   len(book.FragmentsIDs),
			itemsPerPage: size,
		}

		c, err := r.Cookie("show-orig-toolbox")
		showOrigToolbox := err == nil && c.Value == "1"
		if err := bookTmpl.Execute(w, struct {
			Book
			Pagination
			Query           query
			URL             string
			TMServer        string
			TermsRegexp     *regexp.Regexp
			ShowOrigToolbox bool
			WithTM          bool
			WithST          bool
		}{
			book,
			pg,
			query{r.URL.Query()},
			r.URL.String(),
			a.tmServer,
			book.Glossary.TermsRegexp(),
			showOrigToolbox,
			a.tm != nil || a.tmServer != "",
			a.withSentenceTransformers,
		}); err != nil {
			logError(err)
		}

	case "POST":
		title := strings.TrimSpace(r.FormValue("title"))
		if title == "" {
			http.Error(w, "Title must not be empty!", http.StatusBadRequest)
			return
		}
		err := a.db.UpdateBookTitle(bid, title)
		if err != nil {
			if err == ErrNotFound {
				http.Error(w, "Book not found", 404)
			} else {
				internalError(w, err)
			}
			return
		}

	case "DELETE":
		if a.tm != nil {
			if err := a.db.walkBookFragments(bid, func(sd tm.SegmentDescriptor) {
				a.tm.Delete(sd)
			}); err != nil {
				logError(err)
			}
		}

		err := a.db.RemoveBook(bid)
		if err != nil {
			if err == ErrNotFound {
				http.Error(w, "Book not found", 404)
			} else {
				internalError(w, err)
			}
			return
		}
		http.Redirect(w, r, "/", http.StatusSeeOther)
	}
}

func (a *App) NeighborBooks(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	bid, err := u64(vars["book_id"])
	if err != nil {
		http.NotFound(w, r)
		return
	}
	prev, next, err := a.db.NeighborBooks(bid)
	if err != nil {
		if err == ErrNotFound {
			http.Error(w, "Book not found", 404)
		} else {
			internalError(w, err)
		}
		return
	}
	type nb struct {
		URL   string `json:"url"`
		Title string `json:"title"`
	}
	var data struct {
		Prev *nb `json:"prev,omitempty"`
		Next *nb `json:"next,omitempty"`
	}
	q, _ := url.ParseQuery(r.FormValue("q"))
	delete(q, "page")
	query := ""
	if len(q) > 0 {
		query = "?" + q.Encode()
	}
	if prev != nil {
		data.Prev = &nb{
			fmt.Sprintf("/book/%d%s", prev.ID, query),
			prev.Title,
		}
	}
	if next != nil {
		data.Next = &nb{
			fmt.Sprintf("/book/%d%s", next.ID, query),
			next.Title,
		}
	}
	w.Header().Set("Content-Type", "application/json")
	_ = json.NewEncoder(w).Encode(data)
}

func validUploadType(tp string) bool {
	switch tp {
	case "plaintext", "csv", "json", "notabenoid":
		return true
	}
	return false
}

func (a *App) AddBook(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		sess, _ := store.Get(r, "tl_sess")
		errors := sess.Flashes()
		title, _ := sess.Values["title"].(string)
		delete(sess.Values, "title")
		_ = sess.Save(r, w)
		uploadType := r.FormValue("type")
		if !validUploadType(uploadType) {
			uploadType = "plaintext"
		}

		w.Header().Set("Content-Type", "text/html")
		if err := addTmpl.Execute(w, struct {
			Errors []interface{}
			Title  string
			Type   string
		}{
			errors,
			title,
			uploadType,
		}); err != nil {
			logError(err)
		}

	case "POST":
		if r.URL.Path == "/add/notabenoid" {
			a.ImportFromNotabenoid(w, r)
			return
		}

		err := r.ParseMultipartForm(32 * 1024 * 1024)
		if err != nil {
			internalError(w, err)
			return
		}

		title := strings.TrimSpace(r.PostFormValue("title"))
		if title == "" && r.URL.Path != "/add/json" {
			sess, _ := store.Get(r, "tl_sess")
			sess.AddFlash("Title must not be empty!")
			_ = sess.Save(r, w)
			http.Redirect(w, r, r.URL.Path, http.StatusSeeOther)
			return
		}

		var bid uint64
		switch r.URL.Path {
		case "/add":
			content := strings.TrimSpace(r.PostFormValue("content"))
			if content == "" {
				sess, _ := store.Get(r, "tl_sess")
				sess.AddFlash("Content must not be empty!")
				sess.Values["title"] = title
				_ = sess.Save(r, w)
				http.Redirect(w, r, r.URL.Path, http.StatusSeeOther)
				return
			}

			autotranslate := r.PostFormValue("autotranslate") != ""
			bid, err = a.db.AddBook(title, split(content), autotranslate)
			if err != nil {
				internalError(w, err)
				return
			}

		case "/add/csv":
			f, _, err := r.FormFile("csvfile")
			if err != nil {
				internalError(w, err)
				return
			}
			defer f.Close()

			csvr := csv.NewReader(f)
			switch r.FormValue("separator") {
			case ";":
				csvr.Comma = ';'
			case "\t":
				csvr.Comma = '\t'
			}
			csvr.FieldsPerRecord = 2
			records, err := csvr.ReadAll()
			if err != nil {
				sess, _ := store.Get(r, "tl_sess")
				sess.AddFlash("Could not parse the file.")
				sess.Values["title"] = title
				_ = sess.Save(r, w)
				http.Redirect(w, r, "/add?type="+r.FormValue("type"), http.StatusSeeOther)
				return
			}

			bid, err = a.db.AddTranslatedBook(title, records)
			if err != nil {
				internalError(w, err)
				return
			}

		case "/add/json":
			f, _, err := r.FormFile("jsonfile")
			if err != nil {
				internalError(w, err)
				return
			}
			defer f.Close()

			data, err := ioutil.ReadAll(f)
			if err != nil {
				internalError(w, err)
				return
			}

			bid, err = a.db.ImportBookFromJSON(data)
			if err != nil {
				internalError(w, err)
				return
			}
		}

		if a.tm != nil {
			go func() {
				if err := a.db.walkBookFragments(bid, func(sd tm.SegmentDescriptor) {
					a.tm.Add(sd)
				}); err != nil {
					logError(err)
				}
			}()
		}

		http.Redirect(w, r, "/book/"+fmt.Sprint(bid), http.StatusSeeOther)
	}
}

func (a *App) ReadBook(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	bid, err := u64(vars["book_id"])
	if err != nil {
		http.NotFound(w, r)
		return
	}

	book, err := a.db.BookWithTranslations(bid, 0, -1, fNone)
	if err != nil {
		if err == ErrNotFound {
			http.NotFound(w, r)
		} else {
			internalError(w, err)
		}
		return
	}

	var render func(int) template.HTML
	if r.FormValue("fe") != "" {
		render = fresheyeRender(book.Fragments)
	}

	if err := readTmpl.Execute(w, struct {
		Book
		URL            string
		FreshEyeRender func(int) template.HTML
	}{
		book,
		r.FormValue("url"),
		render,
	}); err != nil {
		logError(err)
	}
}

func (a *App) ExportBook(w http.ResponseWriter, r *http.Request) {
	format := r.FormValue("f")
	switch format {
	default:
		http.NotFound(w, r)
		return
	case "plaintext", "plaintext-orig", "csv", "tsv", "jsonl", "json":
	}

	vars := mux.Vars(r)
	bid, err := u64(vars["book_id"])
	if err != nil {
		http.NotFound(w, r)
		return
	}

	if format == "json" {
		data, err := a.db.ExportBookToJSON(bid)
		if err != nil {
			if err == ErrNotFound {
				http.NotFound(w, r)
			} else {
				internalError(w, err)
			}
			return
		}
		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		w.Header().Set("Content-Disposition", `attachment; filename="book.json"`)
		w.Header().Set("Content-Length", fmt.Sprint(len(data)))
		_, _ = w.Write(data)
		return
	}

	book, err := a.db.BookWithTranslations(bid, 0, -1, fNone)
	if err != nil {
		if err == ErrNotFound {
			http.NotFound(w, r)
		} else {
			internalError(w, err)
		}
		return
	}

	switch format {
	case "plaintext":
		w.Header().Set("Content-Type", "text/plain; charset=utf-8")
		w.Header().Set("Content-Disposition", `attachment; filename="book.txt"`)
		for _, f := range book.Fragments {
			t := f.Text
			if len(f.Versions) > 0 {
				t = f.Versions[0].Text
			}
			fmt.Fprintln(w, t)
			_, _ = w.Write([]byte{'\n'})
		}
	case "plaintext-orig":
		w.Header().Set("Content-Type", "text/plain; charset=utf-8")
		w.Header().Set("Content-Disposition", `attachment; filename="book-orig.txt"`)
		for _, f := range book.Fragments {
			fmt.Fprintln(w, f.Text)
			_, _ = w.Write([]byte{'\n'})
		}
	case "csv", "tsv":
		cw := csv.NewWriter(w)
		if format == "csv" {
			w.Header().Set("Content-Type", "text/csv; charset=utf-8")
		} else {
			w.Header().Set("Content-Type", "text/tab-separated-values; charset=utf-8")
			cw.Comma = '\t'
		}
		w.Header().Set("Content-Disposition", fmt.Sprintf(`attachment; filename="book.%s"`, format))
		for _, f := range book.Fragments {
			t := ""
			if len(f.Versions) > 0 {
				t = f.Versions[0].Text
			}
			_ = cw.Write([]string{f.Text, t})
		}
		cw.Flush()
	case "jsonl":
		w.Header().Set("Content-Type", "application/jsonl; charset=utf-8")
		w.Header().Set("Content-Disposition", `attachment; filename="book.jsonl"`)
		enc := json.NewEncoder(w)
		for _, f := range book.Fragments {
			t := ""
			if len(f.Versions) > 0 {
				t = f.Versions[0].Text
			}
			_ = enc.Encode(struct {
				Source      string `json:"source"`
				Translation string `json:"translation"`
			}{
				f.Text,
				t,
			})
		}
	}
}

func (a *App) BookStats(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	bid, err := u64(vars["book_id"])
	if err != nil {
		http.NotFound(w, r)
		return
	}
	stats, err := a.db.BookStats(bid)
	if err != nil {
		if err == ErrNotFound {
			http.NotFound(w, r)
		} else {
			internalError(w, err)
		}
		return
	}
	w.Header().Set("Content-Type", "application/json")
	_ = json.NewEncoder(w).Encode(stats)
}

func (a *App) BookHeatmap(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	bid, err := u64(vars["book_id"])
	if err != nil {
		http.NotFound(w, r)
		return
	}
	year, _ := u64(r.FormValue("year"))
	if year == 0 {
		year = uint64(time.Now().Year())
	}
	book, weekday, heatmap, err := a.db.BookHeatmap(bid, int(year))
	if err != nil {
		if err == ErrNotFound {
			http.NotFound(w, r)
		} else {
			internalError(w, err)
		}
		return
	}
	w.Header().Set("Content-Type", "text/html")
	if err := heatmapTmpl.Execute(w, struct {
		*Book
		Weekday int
		Days    []DayProgress
		Year    int
	}{
		&book,
		weekday,
		heatmap,
		int(year),
	}); err != nil {
		log.Println(err)
	}
}

func (a *App) Fragment(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	bid, err := u64(vars["book_id"])
	if err != nil {
		http.Error(w, "Invalid book ID", http.StatusBadRequest)
		return
	}
	fid, err := u64(vars["fragment_id"])
	if err != nil {
		http.Error(w, "Invalid fragment ID", http.StatusBadRequest)
		return
	}

	book, err := a.db.BookByID(bid)
	if err != nil {
		if err == ErrNotFound {
			http.NotFound(w, r)
		} else {
			internalError(w, err)
		}
		return
	}

	index := idx(book.FragmentsIDs, fid)
	if index == -1 {
		http.NotFound(w, r)
		return
	}

	http.Redirect(w, r, fmt.Sprintf("/book/%d?page=%d#f%d", bid, index/50+1, fid), http.StatusFound)
}

func (a *App) AddFragment(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	bid, err := u64(vars["book_id"])
	if err != nil {
		http.Error(w, "Invalid book ID", http.StatusBadRequest)
		return
	}
	after, _ := u64(r.FormValue("after"))
	before, _ := u64(r.FormValue("before"))
	text := r.FormValue("text")
	if trimmed := strings.TrimSpace(text); trimmed == "" {
		http.Error(w, "Text must not be empty!", http.StatusBadRequest)
		return
	}

	f, err := a.db.AddFragment(bid, after, before, text)
	if err != nil {
		if err == ErrNotFound {
			http.Error(w, "Book or fragment not found", 404)
		} else {
			internalError(w, err)
		}
		return
	}

	if a.tm != nil {
		a.tm.Add(tm.SegmentDescriptor{BookID: bid, FragmentID: f.ID, Text: f.Text})
	}

	w.Header().Set("Content-Type", "application/json")
	_ = json.NewEncoder(w).Encode(struct {
		Fragment
		SeqNum int           `json:"seq_num"`
		Text   template.HTML `json:"text"`
	}{
		f,
		f.SeqNum,
		render(f.Text),
	})
}

func (a *App) UpdateFragment(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	bid, err := u64(vars["book_id"])
	if err != nil {
		http.Error(w, "Invalid book ID", http.StatusBadRequest)
		return
	}
	fid, err := u64(vars["fragment_id"])
	if err != nil {
		http.Error(w, "Invalid fragment ID", http.StatusBadRequest)
		return
	}
	text := r.FormValue("text")
	if trimmed := strings.TrimSpace(text); trimmed == "" {
		http.Error(w, "Text must not be empty!", http.StatusBadRequest)
		return
	}

	oldText, err := a.db.UpdateFragment(bid, fid, text)
	if err != nil {
		if err == ErrNotFound {
			http.Error(w, "Fragment not found", 404)
		} else {
			internalError(w, err)
		}
		return
	}

	if a.tm != nil {
		a.tm.Delete(tm.SegmentDescriptor{BookID: bid, FragmentID: fid, Text: oldText})
		a.tm.Add(tm.SegmentDescriptor{BookID: bid, FragmentID: fid, Text: text})
	}

	w.Header().Set("Content-Type", "application/json")
	_ = json.NewEncoder(w).Encode(struct {
		Text template.HTML `json:"text"`
	}{
		render(text),
	})
}

func (a *App) RemoveFragment(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	bid, err := u64(vars["book_id"])
	if err != nil {
		http.Error(w, "Invalid book ID", http.StatusBadRequest)
		return
	}
	fid, err := u64(vars["fragment_id"])
	if err != nil {
		http.Error(w, "Invalid fragment ID", http.StatusBadRequest)
		return
	}

	text, fragmentsTranslated, err := a.db.RemoveFragment(bid, fid)
	if err != nil {
		if err == ErrNotFound {
			http.Error(w, "Fragment not found", 404)
		} else {
			internalError(w, err)
		}
		return
	}

	if a.tm != nil {
		a.tm.Delete(tm.SegmentDescriptor{BookID: bid, FragmentID: fid, Text: text})
	}

	w.Header().Set("Content-Type", "application/json")
	_ = json.NewEncoder(w).Encode(struct {
		FragmentsTranslated int `json:"fragments_translated"`
	}{
		fragmentsTranslated,
	})
}

func (a *App) StarFragment(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	bid, err := u64(vars["book_id"])
	if err != nil {
		http.Error(w, "Invalid book ID", http.StatusBadRequest)
		return
	}
	fid, err := u64(vars["fragment_id"])
	if err != nil {
		http.Error(w, "Invalid fragment ID", http.StatusBadRequest)
		return
	}

	switch r.Method {
	case "POST":
		err = a.db.StarFragment(bid, fid)
	case "DELETE":
		err = a.db.UnstarFragment(bid, fid)
	}
	if err != nil {
		internalError(w, err)
		return
	}

	w.WriteHeader(http.StatusNoContent)
}

func (a *App) CommentFragment(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	bid, err := u64(vars["book_id"])
	if err != nil {
		http.Error(w, "Invalid book ID", http.StatusBadRequest)
		return
	}
	fid, err := u64(vars["fragment_id"])
	if err != nil {
		http.Error(w, "Invalid fragment ID", http.StatusBadRequest)
		return
	}
	text := r.FormValue("text")

	err = a.db.CommentFragment(bid, fid, text)
	if err != nil {
		if err == ErrNotFound {
			http.Error(w, "Fragment not found", 404)
		} else {
			internalError(w, err)
		}
		return
	}

	w.Header().Set("Content-Type", "application/json")
	_ = json.NewEncoder(w).Encode(struct {
		Text string `json:"text"`
	}{
		text,
	})
}

func (a *App) Translate(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	bid, err := u64(vars["book_id"])
	if err != nil {
		http.Error(w, "Invalid book ID", http.StatusBadRequest)
		return
	}
	fid, err := u64(vars["fragment_id"])
	if err != nil {
		http.Error(w, "Invalid fragment ID", http.StatusBadRequest)
		return
	}
	vid, err := u64(r.FormValue("version_id"))
	if err != nil {
		http.Error(w, "Invalid version ID", http.StatusBadRequest)
		return
	}
	text := r.FormValue("text")
	if trimmed := strings.TrimSpace(text); trimmed == "" {
		http.Error(w, "Text must not be empty!", http.StatusBadRequest)
		return
	}

	v, fragmentsTranslated, err := a.db.Translate(bid, fid, vid, text)
	if err != nil {
		if err == ErrNotFound {
			http.Error(w, "Fragment or version not found", 404)
		} else {
			internalError(w, err)
		}
		return
	}

	w.Header().Set("Content-Type", "application/json")
	_ = json.NewEncoder(w).Encode(struct {
		TranslationVersion
		ID                  uint64        `json:"id"`
		Text                template.HTML `json:"text"`
		FragmentsTranslated int           `json:"fragments_translated"`
	}{
		v,
		v.ID,
		renderWithMarks(v.Text, v.Marks),
		fragmentsTranslated,
	})
}

func (a *App) RemoveVersion(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	bid, err := u64(vars["book_id"])
	if err != nil {
		http.Error(w, "Invalid book ID", http.StatusBadRequest)
		return
	}
	fid, err := u64(vars["fragment_id"])
	if err != nil {
		http.Error(w, "Invalid fragment ID", http.StatusBadRequest)
		return
	}
	vid, err := u64(vars["version_id"])
	if err != nil {
		http.Error(w, "Invalid version ID", http.StatusBadRequest)
		return
	}

	fragmentsTranslated, err := a.db.RemoveVersion(bid, fid, vid)
	if err != nil {
		if err == ErrNotFound {
			http.Error(w, "Version not found", 404)
		} else {
			internalError(w, err)
		}
		return
	}

	w.Header().Set("Content-Type", "application/json")
	_ = json.NewEncoder(w).Encode(struct {
		FragmentsTranslated int `json:"fragments_translated"`
	}{
		fragmentsTranslated,
	})
}

func (a *App) MarkVersion(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	bid, err := u64(vars["book_id"])
	if err != nil {
		http.Error(w, "Invalid book ID", http.StatusBadRequest)
		return
	}
	fid, err := u64(vars["fragment_id"])
	if err != nil {
		http.Error(w, "Invalid fragment ID", http.StatusBadRequest)
		return
	}
	vid, err := u64(vars["version_id"])
	if err != nil {
		http.Error(w, "Invalid version ID", http.StatusBadRequest)
		return
	}
	mark := strings.TrimSpace(r.FormValue("mark"))
	if mark == "" && r.Method == "DELETE" { // Go doesn't parse DELETE requests bodies.
		data, err := ioutil.ReadAll(r.Body)
		if err != nil {
			internalError(w, err)
			return
		}
		vs, err := url.ParseQuery(string(data))
		if err != nil {
			internalError(w, err)
			return
		}
		if v := vs["mark"]; len(v) > 0 {
			mark = v[0]
		}
	}
	if mark == "" {
		http.Error(w, "Empty mark", http.StatusBadRequest)
		return
	}

	var vers TranslationVersion
	switch r.Method {
	case "POST":
		vers, err = a.db.MarkVersion(bid, fid, vid, mark)
	case "DELETE":
		vers, err = a.db.UnmarkVersion(bid, fid, vid, mark)
	default:
		http.Error(w, "method not allowed", http.StatusMethodNotAllowed)
		return
	}
	if err != nil {
		switch err {
		case ErrNotFound:
			http.Error(w, "Version not found", 404)
		case ErrMarkNotFound:
			http.Error(w, "Marked text not found", http.StatusNotModified)
		default:
			internalError(w, err)
		}
		return
	}

	w.Header().Set("Content-Type", "application/json")
	_ = json.NewEncoder(w).Encode(struct {
		Text template.HTML `json:"text"`
	}{
		renderWithMarks(vers.Text, vers.Marks),
	})
}

func (a *App) Backup(w http.ResponseWriter, r *http.Request) {
	if err := a.db.View(func(tx *bolt.Tx) error {
		w.Header().Set("Content-Type", "application/octet-stream")
		w.Header().Set("Content-Disposition", `attachment; filename="tl.db"`)
		w.Header().Set("Content-Length", strconv.Itoa(int(tx.Size())))
		_, err := tx.WriteTo(w)
		return err
	}); err != nil {
		logError(err)
	}
}

func (a *App) Glossary(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	bid, err := u64(vars["book_id"])
	if err != nil {
		http.NotFound(w, r)
		return
	}

	switch r.Method {
	case "GET":
		book, err := a.db.BookByID(bid)
		if err != nil {
			if err == ErrNotFound {
				http.NotFound(w, r)
			} else {
				internalError(w, err)
			}
			return
		}
		if err := glossaryTmpl.Execute(w, struct {
			Book  Book
			URL   string
			Terms []Term
		}{
			book,
			r.FormValue("url"),
			book.Glossary.AlphabeticalTerms(),
		}); err != nil {
			logError(err)
		}

	case "POST":
		term := strings.TrimSpace(r.PostFormValue("term"))
		termOrig := strings.TrimSpace(r.PostFormValue("termOrig"))
		termDef := strings.TrimSpace(r.PostFormValue("def"))
		if termOrig == "" && (term == "" || termDef == "") {
			http.Error(w, "bad request", http.StatusBadRequest)
			return
		}
		if err := a.db.UpdateTerm(bid, term, termOrig, termDef); err != nil {
			if err == ErrNotFound {
				http.NotFound(w, r)
			} else {
				internalError(w, err)
			}
			return
		}

		if v := r.PostFormValue("fragmentsToReload"); v != "" {
			var fragmentsToReload []uint64
			_ = json.NewDecoder(strings.NewReader(v)).Decode(&fragmentsToReload)
			book, text, err := a.db.FragmentsText(bid, fragmentsToReload)
			if err != nil {
				if err == ErrNotFound {
					http.NotFound(w, r)
				} else {
					internalError(w, err)
				}
				return
			}
			r := book.Glossary.TermsRegexp()
			for i, s := range text {
				text[i] = string(renderUsingGlossary(s, r, book.Glossary))
			}
			w.Header().Set("Content-Type", "application/json")
			if err := json.NewEncoder(w).Encode(text); err != nil {
				logError(err)
			}
		}
	}
}

func (a *App) SearchAndReplace(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	bid, err := u64(vars["book_id"])
	if err != nil {
		http.NotFound(w, r)
		return
	}

	search := r.FormValue("search")
	replace := r.FormValue("replace-with")
	if r.FormValue("confirm") != "" {
		if err := a.db.SearchAndReplace(bid, search, replace); err != nil {
			internalError(w, err)
		} else {
			http.Redirect(w, r, fmt.Sprintf("/book/%d", bid), http.StatusSeeOther)
		}
		return
	}

	book, err := a.db.BookWithTranslations(bid, 0, -1, fNone)
	if err != nil {
		if err == ErrNotFound {
			http.NotFound(w, r)
		} else {
			internalError(w, err)
		}
		return
	}
	type previewRow struct {
		SeqNum int
		ID     uint64
		Before template.HTML
		After  template.HTML
	}
	var preview []previewRow
	for _, f := range book.Fragments {
		for _, v := range f.Versions {
			if !strings.Contains(v.Text, search) {
				continue
			}
			replaced := strings.ReplaceAll(v.Text, search, replace)
			before, after := diff.TwoSideHTML(v.Text, replaced)
			preview = append(preview, previewRow{
				f.SeqNum,
				f.ID,
				template.HTML(before), //nolint:gosec
				template.HTML(after),  //nolint:gosec
			})
		}
	}

	w.Header().Set("Content-Type", "text/html")
	if err := snrTmpl.Execute(w, struct {
		*Book
		Search      string
		ReplaceWith string
		Preview     []previewRow
	}{
		&book,
		search,
		replace,
		preview,
	}); err != nil {
		logError(err)
	}
}

func (a *App) DownloadGlossary(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	bid, err := u64(vars["book_id"])
	if err != nil {
		http.NotFound(w, r)
		return
	}

	book, err := a.db.BookByID(bid)
	if err != nil {
		if err == ErrNotFound {
			http.NotFound(w, r)
		} else {
			internalError(w, err)
		}
		return
	}

	var records [][]string
	if book.Glossary != nil {
		for term, d := range book.Glossary {
			records = append(records, []string{term, d.Def})
		}
		sort.Slice(records, func(i, j int) bool {
			return records[i][0] < records[j][0]
		})
	}

	wr := csv.NewWriter(w)
	format := r.FormValue("f")
	if format == "tsv" {
		w.Header().Set("Content-Type", "text/tab-separated-values; charset=utf-8")
		w.Header().Set("Content-Disposition", `attachment; filename="glossary.tsv"`)
		wr.Comma = '\t'
	} else {
		w.Header().Set("Content-Type", "text/csv; charset=utf-8")
		w.Header().Set("Content-Disposition", `attachment; filename="glossary.csv"`)
	}
	if err := wr.WriteAll(records); err != nil {
		logError(err)
	}
}

func (a *App) JSONListOfBooks(w http.ResponseWriter, r *http.Request) {
	books, err := a.db.Books()
	if err != nil {
		internalError(w, err)
		return
	}

	type book struct {
		ID    uint64 `json:"id"`
		Title string `json:"title"`
	}
	jsonBooks := make([]book, 0, len(books))
	for _, b := range books {
		jsonBooks = append(jsonBooks, book{b.ID, b.Title})
	}
	sort.Slice(jsonBooks, func(i, j int) bool {
		return jsonBooks[i].Title < jsonBooks[j].Title
	})

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(jsonBooks); err != nil {
		logError(err)
	}
}

func (a *App) MergeGlossary(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	bid, err := u64(vars["book_id"])
	if err != nil {
		http.NotFound(w, r)
		return
	}
	bidFrom, err := u64(r.PostFormValue("from"))
	if err != nil {
		http.Error(w, "400 Bad Request", http.StatusBadRequest)
		return
	}
	strategy := r.PostFormValue("mergeStrategy")

	if err := a.db.MergeGlossary(bidFrom, bid, strategy == "replace"); err != nil {
		if err == ErrNotFound {
			http.NotFound(w, r)
		} else {
			internalError(w, err)
		}
		return
	}
	w.WriteHeader(http.StatusNoContent)
}
