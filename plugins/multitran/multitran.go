// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package multitran

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/microcosm-cc/bluemonday"
	"gitlab.com/opennota/tl/plugins"
)

const multitranBaseURL = "https://www.multitran.com/c/m.exe?l1=1&l2=2&s="

func internalError(w http.ResponseWriter, err error) {
	log.Println("ERR", err)
	http.Error(w, "internal server error", http.StatusInternalServerError)
}

func handler(w http.ResponseWriter, r *http.Request) {
	query := strings.ToLower(strings.TrimSpace(r.FormValue("query")))
	query = plugins.StressRemover.Replace(query)
	if query == "" {
		http.NotFound(w, r)
		return
	}
	key := "M:" + plugins.YoReplacer.Replace(query)
	data, _ := plugins.Cache.Get(key)
	if data == nil {
		url := multitranBaseURL + url.QueryEscape(query)
		req, _ := http.NewRequest("GET", url, nil)
		req.Header.Add("Cookie", "langs=1 2")
		resp, err := plugins.DefaultClient.Do(req)
		if err != nil {
			internalError(w, err)
			return
		}
		defer resp.Body.Close()

		if resp.StatusCode == 404 {
			http.NotFound(w, r)
			return
		}

		if resp.StatusCode != 200 {
			_, _ = io.Copy(ioutil.Discard, resp.Body)
			internalError(w, plugins.HTTPStatus{StatusCode: resp.StatusCode, URL: url})
			return
		}

		data, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			internalError(w, err)
			return
		}

		_ = plugins.Cache.Put(key, data)
	}

	utf8r := bytes.NewReader(data)
	d, err := goquery.NewDocumentFromReader(utf8r)
	if err != nil {
		internalError(w, err)
		return
	}

	sel := d.Find("form#translation")
	var tbl *goquery.Selection
	for i := 0; i < 5; i++ {
		sel = sel.Next()
		if goquery.NodeName(sel) == "table" {
			tbl = sel
			break
		}
	}
	if tbl == nil {
		http.NotFound(w, r)
		return
	}

	d.Find(`span[style]`).Each(func(_ int, sel *goquery.Selection) {
		if sel.AttrOr("style", "") == "color:gray" {
			sel.SetAttr("class", "text-muted")
		}
		sel.RemoveAttr("style")
	})
	d.Find("td.gray > a:first-child").Each(func(_ int, sel *goquery.Selection) {
		sel.RemoveAttr("href")
		sel.Get(0).Data = "b"
	})

	html, _ := goquery.OuterHtml(sel)

	policy := bluemonday.NewPolicy()
	policy.AllowElements("table", "tbody", "tr", "td", "em", "i", "span", "b")
	policy.AllowAttrs("class").OnElements("span")

	w.Header().Add("Content-Type", "encoding/json")
	_ = json.NewEncoder(w).Encode(struct {
		HTML string `json:"html"`
	}{
		policy.Sanitize(html),
	})
}

func init() { plugins.Register("multitran", handler) }
