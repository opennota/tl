// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package academic

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"sync"

	"github.com/PuerkitoBio/goquery"
	"github.com/microcosm-cc/bluemonday"
	"gitlab.com/opennota/morph"
	"gitlab.com/opennota/tl/plugins"
)

const (
	synonymsBaseURL = "https://dic.academic.ru/dic.nsf/dic_synonims/"
	synSeekBaseURL  = "https://dic.academic.ru/seek4term.php?json=true&limit=20&did=dic_synonims&q="
)

var (
	rSynonymsURL = regexp.MustCompile(`^` + regexp.QuoteMeta(synonymsBaseURL) + `(\d+)/`)

	once     sync.Once
	useMorph bool
)

type seekResult struct {
	ID    int    `json:"id"`
	Value string `json:"value"`
}

func initMorph() {
	err := morph.Init()
	useMorph = err == nil || err == morph.ErrAlreadyInitialized
}

func seekSynonym(query string) ([]seekResult, error) {
	url := synSeekBaseURL + url.QueryEscape(query)
	resp, err := plugins.DefaultClient.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		_, _ = io.Copy(ioutil.Discard, resp.Body)
		return nil, plugins.HTTPStatus{StatusCode: resp.StatusCode, URL: url}
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	rd := bytes.NewReader(data)
	var results []seekResult
	if err := json.NewDecoder(rd).Decode(&struct {
		Results *[]seekResult
	}{
		&results,
	}); err != nil {
		return nil, err
	}

	return results, nil
}

func internalError(w http.ResponseWriter, err error) {
	log.Println("ERR", err)
	http.Error(w, "internal server error", http.StatusInternalServerError)
}

func appendUniq(a []string, s string) []string {
	for _, x := range a {
		if x == s {
			return a
		}
	}
	return append(a, s)
}

func lookUp(word string) ([]byte, error) {
	const keyPrefix = "a:s:"
	key := keyPrefix + word
	data, _ := plugins.Cache.Get(key)
	if data == nil && strings.Contains(word, "ё") {
		fallbackKey := keyPrefix + plugins.YoReplacer.Replace(word)
		data, _ = plugins.Cache.Get(fallbackKey)
	}

	var results []seekResult

	if data == nil {
		var err error
		results, err = seekSynonym(word)
		if err != nil {
			return nil, err
		}
		if len(results) == 0 {
			return nil, nil
		}

		url := synonymsBaseURL + fmt.Sprint(results[0].ID)
		resp, err := plugins.DefaultClient.Get(url)
		if err != nil {
			return nil, err
		}
		defer resp.Body.Close()

		if resp.StatusCode != 200 {
			_, _ = io.Copy(ioutil.Discard, resp.Body)
			return nil, plugins.HTTPStatus{StatusCode: resp.StatusCode, URL: url}
		}

		data, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}

		_ = plugins.Cache.Put(key, data)
	}

	return data, nil
}

func handler(w http.ResponseWriter, r *http.Request) {
	once.Do(initMorph)

	query := strings.ToLower(strings.TrimSpace(r.FormValue("query")))
	query = plugins.StressRemover.Replace(query)
	if query == "" {
		http.NotFound(w, r)
		return
	}
	wordsToLookUp := []string{query}
	if useMorph && r.FormValue("exact") == "" && !strings.Contains(query, " ") {
		_, norms, _ := morph.XParse(query)
		for _, n := range norms {
			wordsToLookUp = appendUniq(wordsToLookUp, n)
		}
	}

	var data []byte
	var err error
	var seeAlso []string
	var i int
	var word string
	for i, word = range wordsToLookUp {
		data, err = lookUp(word)
		if err == nil && len(data) > 0 {
			seeAlso = wordsToLookUp[i+1:]
			break
		}
	}
	if err != nil {
		internalError(w, err)
		return
	}
	if len(data) == 0 {
		http.NotFound(w, r)
		return
	}

	d, err := goquery.NewDocumentFromReader(bytes.NewReader(data))
	if err != nil {
		internalError(w, err)
		return
	}

	policy := bluemonday.NewPolicy()
	policy.AllowElements("div", "em", "span", "strong", "u")
	policy.AllowAttrs("data-id").OnElements("a")
	policy.AllowAttrs("class").OnElements("div", "span")

	var entries []string
	d.Find(`div[itemtype$="/term-def.xml"]`).Each(func(_ int, sel *goquery.Selection) {
		sel.Find("[href]").Each(func(_ int, sel *goquery.Selection) {
			href, _ := sel.Attr("href")
			m := rSynonymsURL.FindStringSubmatch(href)
			if m != nil {
				sel.SetAttr("data-id", m[1])
			}
		})
		sel.Find("[style]").Each(func(_ int, sel *goquery.Selection) {
			style, _ := sel.Attr("style")
			switch style {
			case "color: darkgray;", "color: tomato;":
				sel.SetAttr("class", "text-muted")
			case "margin-left:5px", "color: saddlebrown;":
			}
		})
		html, _ := sel.Find("dd").First().Html()
		entries = append(entries, policy.Sanitize(html))
	})

	w.Header().Add("Content-Type", "encoding/json")
	_ = json.NewEncoder(w).Encode(struct {
		Word    string   `json:"word"`
		HTML    string   `json:"html"`
		SeeAlso []string `json:"see_also"`
	}{
		word,
		strings.Join(entries, ""),
		seeAlso,
	})
}

func init() { plugins.Register("academic", handler) }
