// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package reverso

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"sync"

	"github.com/PuerkitoBio/goquery"
	"github.com/microcosm-cc/bluemonday"
	"gitlab.com/opennota/tl/plugins"
)

const (
	reversoBaseURL   = "https://context.reverso.net/translation"
	reversoUserAgent = "Mozilla/5.0 (X11; Linux x86_64; rv:80.0) Gecko/20100101 Firefox/80.0"
)

func internalError(w http.ResponseWriter, err error) {
	log.Println("ERR", err)
	http.Error(w, "internal server error", http.StatusInternalServerError)
}

var once sync.Once

func logIn() error {
	email := os.Getenv("TL_REVERSO_EMAIL")
	password := os.Getenv("TL_REVERSO_PASSWORD")
	if email == "" || password == "" {
		return fmt.Errorf("no email/password provided; set TL_REVERSO_EMAIL and TL_REVERSO_PASSWORD environment variables")
	}
	resp, err := plugins.DefaultClient.Get("https://account.reverso.net/Account/Login")
	if err != nil {
		return fmt.Errorf("could not get login page: %v", err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return fmt.Errorf("could not get login page")
	}
	d, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		return fmt.Errorf("could not parse login page: %v", err)
	}
	token := d.Find(`[name="__RequestVerificationToken"]`).First().AttrOr("value", "")
	if token == "" {
		return fmt.Errorf("could not get verification token")
	}
	values := url.Values{
		"Email":                      {email},
		"Password":                   {password},
		"RememberMe":                 {"true"},
		"__RequestVerificationToken": {token},
	}
	resp, err = plugins.DefaultClient.PostForm("https://account.reverso.net/Account/Login", values)
	if err != nil {
		return fmt.Errorf("error posting login form: %v", err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 && resp.StatusCode != 302 {
		return fmt.Errorf("error posting login form: status code %d", resp.StatusCode)
	}
	return nil
}

func logInOnce() {
	once.Do(func() {
		if err := logIn(); err != nil {
			log.Println("could not log in to reverso.net:", err)
		}
	})
}

func handleFirstPage(w http.ResponseWriter, r *http.Request, query, langPair string) {
	key := "r:" + langPair + ":" + query
	data, _ := plugins.Cache.Get(key)
	if data == nil {
		var err error
		logInOnce()
		url := reversoBaseURL + "/" + url.PathEscape(langPair) + "/" + url.PathEscape(query)
		req, _ := http.NewRequest("GET", url, nil)
		req.Header.Add("User-Agent", reversoUserAgent)
		resp, err := plugins.DefaultClient.Do(req)
		if err != nil {
			internalError(w, err)
			return
		}
		defer resp.Body.Close()

		if resp.StatusCode != 200 {
			_, _ = io.Copy(ioutil.Discard, resp.Body)
			internalError(w, plugins.HTTPStatus{StatusCode: resp.StatusCode, URL: url})
		}

		data, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			internalError(w, err)
			return
		}
		_ = plugins.Cache.Put(key, data)
	}

	d, err := goquery.NewDocumentFromReader(bytes.NewReader(data))
	if err != nil {
		internalError(w, err)
		return
	}

	moreExamples := d.Find("#load-more-examples").Length() > 0

	if d.Find(".username").Text() == "" {
		log.Println("[reverso] not logged in?")
	}

	examplesContent := d.Find("#examples-content")
	if examplesContent.Length() == 0 {
		http.NotFound(w, r)
		return
	}
	examplesContent.Find(".options, .icon, .inner-rca, .popup, .wide-container, #add-example, .other-result").Remove()

	policy := bluemonday.NewPolicy()
	policy.AllowElements("div", "span", "em")
	policy.AllowAttrs("class").Globally()

	html, _ := examplesContent.Html()
	w.Header().Add("Content-Type", "encoding/json")
	_ = json.NewEncoder(w).Encode(struct {
		HTML         string `json:"html"`
		MoreExamples bool   `json:"more_examples"`
	}{
		policy.Sanitize(html),
		moreExamples,
	})
}

type example struct {
	Source      string `json:"s_text"`
	Translation string `json:"t_text"`
}

type examples struct {
	List     []example `json:"list"`
	NumPages int       `json:"npages"`
}

func handleNext(w http.ResponseWriter, r *http.Request, query, sl, tl string) {
	page, _ := strconv.Atoi(r.FormValue("page"))
	key := fmt.Sprintf("r:%d:%s-%s:%s", page, sl, tl, query)
	data, _ := plugins.Cache.Get(key)
	if data == nil {
		logInOnce()
		rurl := "https://context.reverso.net/bst-query-service"
		values := map[string]interface{}{
			"source_text": query,
			"target_text": "",
			"source_lang": sl,
			"target_lang": tl,
			"npage":       page,
			"mode":        0,
		}
		var buf bytes.Buffer
		_ = json.NewEncoder(&buf).Encode(values)
		req, _ := http.NewRequest("POST", rurl, &buf)
		req.Header.Add("User-Agent", reversoUserAgent)
		req.Header.Add("Content-Type", "application/json; charset=UTF-8")
		resp, err := plugins.DefaultClient.Do(req)
		if err != nil {
			internalError(w, err)
			return
		}
		defer resp.Body.Close()

		if resp.StatusCode != 200 {
			_, _ = io.Copy(ioutil.Discard, resp.Body)
			internalError(w, plugins.HTTPStatus{StatusCode: resp.StatusCode, URL: rurl})
		}

		data, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			internalError(w, err)
			return
		}

		_ = plugins.Cache.Put(key, data)
	}

	var examples examples
	if err := json.NewDecoder(bytes.NewReader(data)).Decode(&examples); err != nil {
		internalError(w, err)
		return
	}

	policy := bluemonday.NewPolicy()
	policy.AllowElements("em")

	var buf strings.Builder
	for _, ex := range examples.List {
		buf.WriteString(`<div class="example">`)
		buf.WriteString(`<div class="src">`)
		buf.WriteString(policy.Sanitize(ex.Source))
		buf.WriteString(`</div>`)
		buf.WriteString(`<div class="trg">`)
		buf.WriteString(policy.Sanitize(ex.Translation))
		buf.WriteString(`</div>`)
		buf.WriteString(`</div>`)
	}

	w.Header().Add("Content-Type", "encoding/json")
	_ = json.NewEncoder(w).Encode(struct {
		HTML         string `json:"html"`
		MoreExamples bool   `json:"more_examples"`
	}{
		buf.String(),
		page < examples.NumPages,
	})
}

func handler(w http.ResponseWriter, r *http.Request) {
	query := strings.TrimSpace(r.FormValue("query"))
	if query == "" {
		http.NotFound(w, r)
		return
	}

	sourceLang := os.Getenv("TL_REVERSO_SOURCE_LANGUAGE")
	if sourceLang == "" {
		sourceLang = "en"
	}
	targetLang := os.Getenv("TL_REVERSO_TARGET_LANGUAGE")
	if targetLang == "" {
		targetLang = "ru"
	}
	langPair := os.Getenv("TL_REVERSO_LANGUAGE_PAIR")
	if langPair == "" {
		langPair = "english-russian"
	}
	if page := r.FormValue("page"); page == "" {
		handleFirstPage(w, r, query, langPair)
	} else {
		handleNext(w, r, query, sourceLang, targetLang)
	}
}

func init() { plugins.Register("reverso", handler) }
