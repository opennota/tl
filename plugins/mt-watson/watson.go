// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package watson

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"

	"gitlab.com/opennota/tl/plugins"
)

func internalError(w http.ResponseWriter, err error) {
	log.Println("ERR", err)
	http.Error(w, "internal server error", http.StatusInternalServerError)
}

func handler(w http.ResponseWriter, r *http.Request) {
	sl := os.Getenv("TL_MT_SOURCE_LANGUAGE")
	tl := os.Getenv("TL_MT_TARGET_LANGUAGE")
	if sl == "" || sl == "auto" {
		sl = "en"
	} else {
		sl = strings.TrimPrefix(sl, "auto,")
	}
	if tl == "" {
		w.WriteHeader(http.StatusTeapot)
		return
	}

	q := strings.TrimSpace(r.FormValue("q"))
	if q == "" {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	var transl string
	errMsg := ""

	key := fmt.Sprintf("mt:w:%s:%s:%s", sl, tl, q)
	data, _ := plugins.Cache.Get(key)

	if len(data) > 0 {
		transl = string(data)
	} else {
		watsonURL := os.Getenv("TL_MT_WATSON_URL")
		apiKey := os.Getenv("TL_MT_WATSON_API_KEY")
		if watsonURL == "" || apiKey == "" {
			w.WriteHeader(http.StatusTeapot)
			return
		}
		u, err := url.Parse(watsonURL)
		if err != nil {
			internalError(w, err)
			return
		}
		u.User = url.UserPassword("apikey", apiKey)

		var buf bytes.Buffer
		_ = json.NewEncoder(&buf).Encode(map[string]interface{}{
			"text":     []string{q},
			"model_id": sl + "-" + tl,
		})
		req, _ := http.NewRequest("POST", u.String(), &buf)
		req.Header.Add("Content-Type", "application/json")
		resp, err := plugins.DefaultClient.Do(req)
		if err != nil {
			errMsg = err.Error()
		} else {
			defer resp.Body.Close()
			if resp.StatusCode != 200 {
				_, _ = io.Copy(ioutil.Discard, resp.Body)
				internalError(w, plugins.HTTPStatus{StatusCode: resp.StatusCode, URL: u.String()})
				return
			}
			var res struct {
				Translations []struct {
					Translation string
				}
			}
			if err := json.NewDecoder(resp.Body).Decode(&res); err != nil {
				internalError(w, err)
				return
			}
			ts := make([]string, 0, len(res.Translations))
			for _, t := range res.Translations {
				ts = append(ts, strings.TrimSpace(t.Translation))
			}
			transl = strings.Join(ts, " ")
			_ = plugins.Cache.Put(key, []byte(transl))
		}
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(struct {
		Translation string `json:"translation"`
		Error       string `json:"error,omitempty"`
	}{
		transl,
		errMsg,
	}); err != nil {
		log.Println("ERR", err)
	}
}

func init() { plugins.RegisterMTPlugin("watson", handler) }
