// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package kartaslov

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
	"sync"

	"github.com/PuerkitoBio/goquery"
	"github.com/microcosm-cc/bluemonday"
	"gitlab.com/opennota/morph"
	"gitlab.com/opennota/tl/plugins"
)

const synonymsBaseURL = "https://kartaslov.ru/синонимы-к-слову/"

var (
	once     sync.Once
	useMorph bool
)

func initMorph() {
	err := morph.Init()
	useMorph = err == nil || err == morph.ErrAlreadyInitialized
}

func internalError(w http.ResponseWriter, err error) {
	log.Println("ERR", err)
	http.Error(w, "internal server error", http.StatusInternalServerError)
}

func appendUniq(a []string, s string) []string {
	for _, x := range a {
		if x == s {
			return a
		}
	}
	return append(a, s)
}

func lookUp(word string) ([]byte, error) {
	const keyPrefix = "a:k:"
	key := keyPrefix + word
	data, _ := plugins.Cache.Get(key)
	if data == nil && strings.Contains(word, "ё") {
		fallbackKey := keyPrefix + plugins.YoReplacer.Replace(word)
		data, _ = plugins.Cache.Get(fallbackKey)
	}

	if data == nil {
		url := synonymsBaseURL + url.QueryEscape(word)
		resp, err := plugins.DefaultClient.Get(url)
		if err != nil {
			return nil, err
		}
		defer resp.Body.Close()

		if resp.StatusCode != 200 {
			_, _ = io.Copy(ioutil.Discard, resp.Body)
			return nil, plugins.HTTPStatus{StatusCode: resp.StatusCode, URL: url}
		}

		data, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, err
		}

		_ = plugins.Cache.Put(key, data)
	}

	return data, nil
}

func handler(w http.ResponseWriter, r *http.Request) {
	once.Do(initMorph)

	query := strings.ToLower(strings.TrimSpace(r.FormValue("query")))
	query = plugins.StressRemover.Replace(query)
	if query == "" {
		http.NotFound(w, r)
		return
	}
	wordsToLookUp := []string{query}
	if useMorph && r.FormValue("exact") == "" && !strings.Contains(query, " ") {
		_, norms, _ := morph.XParse(query)
		for _, n := range norms {
			wordsToLookUp = appendUniq(wordsToLookUp, n)
		}
	}

	var data []byte
	var err error
	var seeAlso []string
	var i int
	var word string
	for i, word = range wordsToLookUp {
		data, err = lookUp(word)
		if err == nil && len(data) > 0 {
			seeAlso = wordsToLookUp[i+1:]
			break
		}
	}
	if err != nil {
		internalError(w, err)
		return
	}
	if len(data) == 0 {
		http.NotFound(w, r)
		return
	}

	d, err := goquery.NewDocumentFromReader(bytes.NewReader(data))
	if err != nil {
		internalError(w, err)
		return
	}

	policy := bluemonday.NewPolicy()
	policy.AllowElements("ul", "li", "a")
	policy.AllowAttrs("class").OnElements("a")

	var entries []string
	d.Find(".v2-syn-list").Each(func(_ int, sel *goquery.Selection) {
		html, _ := goquery.OuterHtml(sel)
		entries = append(entries, policy.Sanitize(html))
	})

	w.Header().Add("Content-Type", "encoding/json")
	_ = json.NewEncoder(w).Encode(struct {
		Word    string   `json:"word"`
		HTML    string   `json:"html"`
		SeeAlso []string `json:"see_also"`
	}{
		word,
		strings.Join(entries, ""),
		seeAlso,
	})
}

func init() { plugins.Register("kartaslov", handler) }
