// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package epithets

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"sort"
	"strings"
	"sync"

	"github.com/PuerkitoBio/goquery"
	"github.com/microcosm-cc/bluemonday"
	"gitlab.com/opennota/morph"
	"gitlab.com/opennota/tl/plugins"
)

const epithetsBaseURL = "https://gufo.me/dict/epithets/"

var (
	once     sync.Once
	useMorph bool
)

func initMorph() {
	err := morph.Init()
	useMorph = err == nil || err == morph.ErrAlreadyInitialized
}

func internalError(w http.ResponseWriter, err error) {
	log.Println("ERR", err)
	http.Error(w, "internal server error", http.StatusInternalServerError)
}

func handler(w http.ResponseWriter, r *http.Request) {
	once.Do(initMorph)

	query := strings.ToLower(strings.TrimSpace(r.FormValue("query")))
	query = plugins.YoReplacer.Replace(query)
	query = plugins.StressRemover.Replace(query)
	if query == "" {
		http.NotFound(w, r)
		return
	}
	if useMorph && r.FormValue("exact") == "" && !strings.Contains(query, " ") {
		_, norms, _ := morph.XParse(query)
		if len(norms) > 0 {
			norms = append(norms, query)
			for i, w := range norms {
				norms[i] = plugins.YoReplacer.Replace(w)
			}
			sort.Strings(norms)
			words := norms[:0]
			for i, w := range norms {
				if i == 0 || words[len(words)-1] != w {
					words = append(words, w)
				}
			}

			if len(words) == 1 {
				query = words[0]
			} else {
				w.Header().Add("Content-Type", "encoding/json")
				pre, post := `<a morph="1">`, `</a>`
				_ = json.NewEncoder(w).Encode(struct {
					HTML string `json:"html"`
				}{
					`<div>Select one of: ` + pre + strings.Join(words, post+", "+pre) + post + "</div>",
				})
				return
			}
		}
	}

	key := "e:" + query
	data, _ := plugins.Cache.Get(key)
	if data == nil {
		url := epithetsBaseURL + url.PathEscape(query)
		resp, err := plugins.DefaultClient.Get(url)
		if err != nil {
			internalError(w, err)
			return
		}
		defer resp.Body.Close()

		if resp.StatusCode != 200 {
			_, _ = io.Copy(ioutil.Discard, resp.Body)
			if resp.StatusCode == 404 {
				http.NotFound(w, r)
			} else {
				internalError(w, plugins.HTTPStatus{StatusCode: resp.StatusCode, URL: url})
			}
			return
		}

		data, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			internalError(w, err)
			return
		}

		_ = plugins.Cache.Put(key, data)
	}

	d, err := goquery.NewDocumentFromReader(bytes.NewReader(data))
	if err != nil {
		internalError(w, err)
		return
	}

	policy := bluemonday.NewPolicy()
	policy.AllowElements("article", "h1", "div", "em", "span", "p", "small")
	policy.AllowAttrs("style", "class").OnElements("span")

	sel := d.Find("#dictionary-acticle > article") // the typo is intentional
	sel.Find("small").Remove()
	sel.Find("[style]").Each(func(_ int, sel *goquery.Selection) {
		sel.RemoveAttr("style")
		sel.SetAttr("class", "epithets-styled")
	})
	html, _ := goquery.OuterHtml(sel)

	w.Header().Add("Content-Type", "encoding/json")
	_ = json.NewEncoder(w).Encode(struct {
		HTML string `json:"html"`
	}{
		policy.Sanitize(html),
	})
}

func init() { plugins.Register("epithets", handler) }
