// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

package deepl

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	"gitlab.com/opennota/mt"
	"gitlab.com/opennota/tl/plugins"
)

func handler(w http.ResponseWriter, r *http.Request) {
	sl := os.Getenv("TL_MT_SOURCE_LANGUAGE")
	tl := os.Getenv("TL_MT_TARGET_LANGUAGE")
	if sl == "" || strings.HasPrefix(sl, "auto,") {
		sl = "auto"
	}
	if tl == "" {
		w.WriteHeader(http.StatusTeapot)
		return
	}

	q := strings.TrimSpace(r.FormValue("q"))
	if q == "" {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	var transl string
	errMsg := ""

	key := fmt.Sprintf("mt:d:%s:%s:%s", sl, tl, q)
	data, _ := plugins.Cache.Get(key)

	if len(data) > 0 {
		transl = string(data)
	} else {
		var err error
		if transl, err = mt.DeepL(r.Context(), sl, tl, q); err != nil {
			errMsg = err.Error()
		} else {
			_ = plugins.Cache.Put(key, []byte(transl))
		}
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(struct {
		Translation string `json:"translation"`
		Error       string `json:"error,omitempty"`
	}{
		transl,
		errMsg,
	}); err != nil {
		log.Println("ERR", err)
	}
}

func init() { plugins.RegisterMTPlugin("deepl", handler) }
