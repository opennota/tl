// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

package deepl

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/chromedp/chromedp"
	"github.com/chromedp/chromedp/kb"
	"gitlab.com/opennota/tl/plugins"
)

const deeplURL = "https://www.deepl.com/translator"

const instrumentTargetTextareaJS = `(function(sel, cls) {
  var input = document.querySelector(sel);
  var defaultDescriptor = Object.getOwnPropertyDescriptor(input.constructor.prototype, 'value');
  Object.defineProperty(input, 'value', {
    get: function get() {
      return defaultDescriptor.get.call(input);
    },
    set: function set(newValue) {
      if (newValue) input.classList.add(cls);
      defaultDescriptor.set.call(input, newValue);
    }
  });
  return true;
})(%q, %q);
`

const removeClassJS = `(function(sel, cls) {
  document.querySelector(sel).classList.remove(cls);
  return true;
})(%q, %q);
`

const (
	sourceTextareaSel = ".lmt__source_textarea"
	targetTextareaSel = ".lmt__target_textarea"
	readyClass        = "--ready--"
)

var (
	once sync.Once

	m        sync.Mutex
	deeplCtx context.Context
)

func translateText(text string, translation *string) chromedp.Tasks {
	var tmp bool
	return chromedp.Tasks{
		chromedp.Evaluate(fmt.Sprintf(removeClassJS, targetTextareaSel, readyClass), &tmp),
		chromedp.SetValue(sourceTextareaSel, text, chromedp.ByQuery),
		chromedp.SendKeys(sourceTextareaSel, kb.Enter+kb.Backspace, chromedp.ByQuery),
		chromedp.WaitVisible("."+readyClass, chromedp.ByQuery),
		chromedp.Value(targetTextareaSel, translation, chromedp.ByQuery),
	}
}

func initEngine(sl, tl string) (context.Context, error) {
	c, _ := chromedp.NewExecAllocator(
		context.Background(),
		chromedp.Headless,
		chromedp.NoDefaultBrowserCheck,
		chromedp.NoFirstRun,
		chromedp.WindowSize(1280, 720), // doesn't work with default 800x600
	)

	ctx, _ := chromedp.NewContext(
		c,
		chromedp.WithLogf(log.Printf),
	)

	if err := chromedp.Run(ctx); err != nil {
		return nil, fmt.Errorf("could not start the browser: %v", err)
	}

	toCtx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()
	var tmp bool
	if err := chromedp.Run(toCtx,
		chromedp.Navigate(deeplURL),
		chromedp.Click(".lmt__language_select--source .lmt__language_select__opener", chromedp.ByQuery),
		chromedp.Click(fmt.Sprintf(`[dl-test="translator-lang-option-%s"]`, sl), chromedp.ByQuery),
		chromedp.Click(".lmt__language_select--target .lmt__language_select__opener", chromedp.ByQuery),
		chromedp.Click(fmt.Sprintf(`[dl-test="translator-lang-option-%s-%s"]`, tl, strings.ToUpper(tl)), chromedp.ByQuery),
		chromedp.Evaluate(fmt.Sprintf(instrumentTargetTextareaJS, targetTextareaSel, readyClass), &tmp),
	); err != nil {
		return nil, fmt.Errorf("could not initialize the engine: %v", err)
	}

	return ctx, nil
}

func handler(w http.ResponseWriter, r *http.Request) {
	sl := os.Getenv("TL_MT_SOURCE_LANGUAGE")
	tl := os.Getenv("TL_MT_TARGET_LANGUAGE")
	if sl == "" || strings.HasPrefix(sl, "auto,") {
		sl = "auto"
	}
	if tl == "" {
		w.WriteHeader(http.StatusTeapot)
		return
	}

	q := strings.TrimSpace(r.FormValue("q"))
	if q == "" {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	once.Do(func() {
		var err error
		deeplCtx, err = initEngine(sl, tl)
		if err != nil {
			log.Println("[deepl]", err)
		}
	})

	if deeplCtx == nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	var transl string
	errMsg := ""

	key := fmt.Sprintf("mt:d:%s:%s:%s", sl, tl, q)
	data, _ := plugins.Cache.Get(key)

	if len(data) > 0 {
		transl = string(data)
	} else {
		m.Lock()
		defer m.Unlock()

		ctx, cancel := context.WithTimeout(deeplCtx, 30*time.Second)
		defer cancel()
		ctx, cancel = context.WithCancel(ctx)
		defer cancel()
		go func() {
			select {
			case <-r.Context().Done():
				cancel()
			case <-ctx.Done():
			}
		}()
		if err := chromedp.Run(ctx, translateText(q, &transl)); err != nil {
			errMsg = err.Error()
		} else {
			_ = plugins.Cache.Put(key, []byte(transl))
		}
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(struct {
		Translation string `json:"translation"`
		Error       string `json:"error,omitempty"`
	}{
		transl,
		errMsg,
	}); err != nil {
		log.Println("ERR", err)
	}
}

func init() { plugins.RegisterMTPlugin("deepl", handler) }
