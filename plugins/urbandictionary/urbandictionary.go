// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package urbandictionary

import (
	"bytes"
	"encoding/json"
	"html"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/microcosm-cc/bluemonday"
	"gitlab.com/opennota/tl/plugins"
)

const baseURL = "https://www.urbandictionary.com/define.php?term="

func internalError(w http.ResponseWriter, err error) {
	log.Println("ERR", err)
	http.Error(w, "internal server error", http.StatusInternalServerError)
}

func handler(w http.ResponseWriter, r *http.Request) {
	query := strings.ToLower(strings.TrimSpace(r.FormValue("query")))
	if query == "" {
		http.NotFound(w, r)
		return
	}
	key := "u:" + query
	data, _ := plugins.Cache.Get(key)
	if data == nil {
		url := baseURL + url.QueryEscape(query)
		resp, err := plugins.DefaultClient.Get(url)
		if err != nil {
			internalError(w, err)
			return
		}
		defer resp.Body.Close()

		if resp.StatusCode != 200 && resp.StatusCode != 404 {
			_, _ = io.Copy(ioutil.Discard, resp.Body)
			internalError(w, plugins.HTTPStatus{StatusCode: resp.StatusCode, URL: url})
			return
		}

		data, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			internalError(w, err)
			return
		}

		_ = plugins.Cache.Put(key, data)
	}

	d, err := goquery.NewDocumentFromReader(bytes.NewReader(data))
	if err != nil {
		internalError(w, err)
		return
	}

	var result []string
	similar := d.Find(".try-these a[href^='/define.php?term=']")
	if similar.Length() > 0 {
		similar.Each(func(_ int, sel *goquery.Selection) {
			result = append(result, `<a class="similar">`+html.EscapeString(sel.Text())+"</a>")
		})
	} else {
		policy := bluemonday.NewPolicy()
		policy.AllowElements("div", "a", "b", "br")
		policy.AllowAttrs("class").OnElements("div")
		policy.AllowNoAttrs().OnElements("a")

		defs := d.Find(".def-panel")
		defs.Find(".def-header > .category").Remove()
		defs.Find(".def-header > a").Each(func(_ int, sel *goquery.Selection) {
			sel.Get(0).Data = "b"
		})
		defs.Each(func(_ int, d *goquery.Selection) {
			if strings.Contains(d.Find(".ribbon").Text(), "Word of the Day") {
				return
			}
			d.Children().Each(func(_ int, div *goquery.Selection) {
				if cls, _ := div.Attr("class"); cls == "def-header" || cls == "meaning" || cls == "example" {
					return
				}
				div.Remove()
			})
			html, _ := goquery.OuterHtml(d)
			result = append(result, policy.Sanitize(html))
		})
	}

	if len(result) == 0 {
		http.NotFound(w, r)
		return
	}

	w.Header().Add("Content-Type", "encoding/json")
	_ = json.NewEncoder(w).Encode(struct {
		HTML string `json:"html"`
	}{
		strings.Join(result, ""),
	})
}

func init() { plugins.Register("urbandictionary", handler) }
