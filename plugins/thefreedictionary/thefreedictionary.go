// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package thefreedictionary

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/microcosm-cc/bluemonday"
	"gitlab.com/opennota/tl/plugins"
)

const tfdBaseURL = "https://idioms.thefreedictionary.com/"

func internalError(w http.ResponseWriter, err error) {
	log.Println("ERR", err)
	http.Error(w, "internal server error", http.StatusInternalServerError)
}

func handler(w http.ResponseWriter, r *http.Request) {
	query := strings.ToLower(strings.TrimSpace(r.FormValue("query")))
	if query == "" {
		http.NotFound(w, r)
		return
	}
	key := "t:" + query
	data, _ := plugins.Cache.Get(key)
	if data == nil {
		url := tfdBaseURL + url.PathEscape(query)
		resp, err := plugins.DefaultClient.Get(url)
		if err != nil {
			internalError(w, err)
			return
		}
		defer resp.Body.Close()

		if resp.StatusCode != 200 {
			_, _ = io.Copy(ioutil.Discard, resp.Body)
			if resp.StatusCode == 404 {
				http.NotFound(w, r)
			} else {
				internalError(w, plugins.HTTPStatus{StatusCode: resp.StatusCode, URL: url})
			}
			return
		}

		data, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			internalError(w, err)
			return
		}

		_ = plugins.Cache.Put(key, data)
	}

	d, err := goquery.NewDocumentFromReader(bytes.NewReader(data))
	if err != nil {
		internalError(w, err)
		return
	}

	policy := bluemonday.NewPolicy()
	policy.AllowElements("div", "span", "h2", "section", "i", "b", "strong", "ul", "li")
	policy.AllowAttrs("class").OnElements("div", "span")

	sel := d.Find("#MainTxt")
	sel.Find("#Thesaurus, #Translations, .idiPgCont, .videoWrap").Remove()
	sel.Find("#VideoSubscrFrm").Parent().Remove()
	sel.Find(".idiKw > li > a").Each(func(_ int, sel *goquery.Selection) {
		sel.RemoveAttr("href")
		sel.Get(0).Data = "span"
		sel.SetAttr("class", "link")
	})
	html, _ := goquery.OuterHtml(sel)

	w.Header().Add("Content-Type", "encoding/json")
	_ = json.NewEncoder(w).Encode(struct {
		HTML string `json:"html"`
	}{
		policy.Sanitize(html),
	})
}

func init() { plugins.Register("thefreedictionary", handler) }
