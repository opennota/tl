// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package mymemory

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"

	"gitlab.com/opennota/tl/plugins"
)

var baseURL = "https://api.mymemory.translated.net/"

func internalError(w http.ResponseWriter, err error) {
	log.Println("ERR", err)
	http.Error(w, "internal server error", http.StatusInternalServerError)
}

func handler(w http.ResponseWriter, r *http.Request) {
	sl := os.Getenv("TL_MT_SOURCE_LANGUAGE")
	tl := os.Getenv("TL_MT_TARGET_LANGUAGE")
	if sl == "" || sl == "auto" {
		sl = "en"
	} else {
		sl = strings.TrimPrefix(sl, "auto,")
	}
	if tl == "" {
		w.WriteHeader(http.StatusTeapot)
		return
	}

	q := strings.TrimSpace(r.FormValue("q"))
	if q == "" {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}
	if len(q) > 500 {
		http.Error(w, http.StatusText(http.StatusRequestEntityTooLarge), http.StatusRequestEntityTooLarge)
		return
	}

	var jsonData []byte

	key := fmt.Sprintf("mt:m:%s:%s:%s", sl, tl, q)
	data, _ := plugins.Cache.Get(key)

	if len(data) > 0 {
		jsonData = data
	} else {
		email := os.Getenv("TL_MT_MYMEMORY_EMAIL")
		values := url.Values{
			"q":        {q},
			"langpair": {sl + "|" + tl},
		}
		if email != "" {
			values["de"] = []string{email}
		}
		rurl := baseURL + "/get?" + values.Encode()
		resp, err := plugins.DefaultClient.Get(rurl)
		if err != nil {
			internalError(w, err)
			return
		}
		defer resp.Body.Close()
		if resp.StatusCode != 200 {
			_, _ = io.Copy(ioutil.Discard, resp.Body)
			internalError(w, plugins.HTTPStatus{StatusCode: resp.StatusCode, URL: rurl})
			return
		}
		jsonData, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			internalError(w, err)
			return
		}
		_ = plugins.Cache.Put(key, jsonData)
	}

	var res struct {
		Matches []struct {
			Translation string
		}
		ResponseData struct {
			TranslatedText string
		}
	}
	if err := json.NewDecoder(bytes.NewReader(jsonData)).Decode(&res); err != nil {
		_ = plugins.Cache.Put(key, nil)
		internalError(w, err)
		return
	}

	alt := make([]string, 0, len(res.Matches))
	for _, m := range res.Matches {
		if m.Translation == res.ResponseData.TranslatedText {
			continue
		}
		alt = append(alt, m.Translation)
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(struct {
		Translation string   `json:"translation"`
		Alt         []string `json:"alt_translations"`
	}{
		res.ResponseData.TranslatedText,
		alt,
	}); err != nil {
		log.Println("ERR", err)
	}
}

func init() { plugins.RegisterMTPlugin("mymemory", handler) }
