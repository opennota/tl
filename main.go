// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// A web app for translators.
package main

import (
	"flag"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"gitlab.com/opennota/tl/plugins"
)

func main() {
	addr := flag.String("http", "", "HTTP service address (default :$PORT or :3000)")
	dataSource := flag.String("db", "tl.db", "Path to the translation database")
	withTM := flag.Bool("with-tm", false, "Enable translation memory")
	withST := flag.Bool("with-sentence-transformers", false, "Use UKPLab/sentence-transformers")
	tmServer := flag.String("tm-server", "", "External TM server")
	heroku := flag.Bool("heroku", false, "Running on Heroku?")
	flag.Parse()
	log.SetFlags(0)

	if *addr == "" {
		port := os.Getenv("PORT")
		if port == "" {
			port = "3000"
		}
		*addr = "127.0.0.1:" + port
	}

	fsys := NewFS(files)
	mustParseTemplates(fsys)

	if !*heroku {
		if err := initializeMorph(); err != nil {
			log.Println("failed to load morphology:", err)
		}
	}

	db, err := OpenDatabase(*dataSource, 0o600, nil)
	if err != nil {
		log.Fatal(err)
	}

	app := App{
		db:                       db,
		tmServer:                 *tmServer,
		withSentenceTransformers: *withST,
	}

	if *withTM {
		log.Print("Indexing translation memory...")
		trm, d, err := setupTMIndex(db)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("TM has been indexed (indexing took %v)\n", d)
		app.tm = trm
	}

	r := mux.NewRouter()

	withGzip := handlers.CompressHandler

	r.HandleFunc("/", app.Index).Methods("GET")
	r.HandleFunc("/add", app.AddBook).Methods("GET", "POST")
	r.HandleFunc("/add/{_:csv|json|notabenoid}", app.AddBook).Methods("POST")
	r.HandleFunc("/aligner", app.Aligner).Methods("GET", "POST")
	r.HandleFunc("/fresheye", app.FreshEye).Methods("GET", "POST")
	r.HandleFunc("/analysis{_:|/new}", app.Analysis).Methods("GET", "POST")
	r.HandleFunc("/backup", app.Backup).Methods("GET")
	r.HandleFunc("/books", app.JSONListOfBooks).
		Methods("GET")
	r.HandleFunc("/fuzzy", app.FuzzyMatches).
		Methods("GET")
	r.Handle("/book/{book_id:[0-9]+}", withGzip(http.HandlerFunc(app.Book))).
		Methods("GET")
	r.HandleFunc("/book/{book_id:[0-9]+}", app.Book).
		Methods("POST", "DELETE")
	r.HandleFunc("/book/{book_id:[0-9]+}/neighbors", app.NeighborBooks).
		Methods("GET")
	r.Handle("/book/{book_id:[0-9]+}/read", withGzip(http.HandlerFunc(app.ReadBook))).
		Methods("GET")
	r.HandleFunc("/book/{book_id:[0-9]+}/glossary", app.Glossary).
		Methods("GET", "POST")
	r.HandleFunc("/book/{book_id:[0-9]+}/glossary/download", app.DownloadGlossary).
		Methods("GET")
	r.HandleFunc("/book/{book_id:[0-9]+}/glossary/merge", app.MergeGlossary).
		Methods("POST")
	r.HandleFunc("/book/{book_id:[0-9]+}/export", app.ExportBook).
		Methods("GET")
	r.HandleFunc("/book/{book_id:[0-9]+}/stats", app.BookStats).
		Methods("GET")
	r.HandleFunc("/book/{book_id:[0-9]+}/heatmap", app.BookHeatmap).
		Methods("GET")
	r.HandleFunc("/book/{book_id:[0-9]+}/{fragment_id:[0-9]+}", app.Fragment).
		Methods("GET")
	r.HandleFunc("/book/{book_id:[0-9]+}/fragments", app.AddFragment).
		Methods("POST")
	r.HandleFunc("/book/{book_id:[0-9]+}/{fragment_id:[0-9]+}", app.UpdateFragment).
		Methods("POST")
	r.HandleFunc("/book/{book_id:[0-9]+}/{fragment_id:[0-9]+}", app.RemoveFragment).
		Methods("DELETE")
	r.HandleFunc("/book/{book_id:[0-9]+}/{fragment_id:[0-9]+}/star", app.StarFragment).
		Methods("POST", "DELETE")
	r.HandleFunc("/book/{book_id:[0-9]+}/{fragment_id:[0-9]+}/comment", app.CommentFragment).
		Methods("POST")
	r.HandleFunc("/book/{book_id:[0-9]+}/{fragment_id:[0-9]+}/translate", app.Translate).
		Methods("POST")
	r.HandleFunc("/book/{book_id:[0-9]+}/{fragment_id:[0-9]+}/{version_id:[0-9]+}", app.RemoveVersion).
		Methods("DELETE")
	r.HandleFunc("/book/{book_id:[0-9]+}/{fragment_id:[0-9]+}/{version_id:[0-9]+}/mark", app.MarkVersion).
		Methods("POST", "DELETE")
	r.HandleFunc("/book/{book_id:[0-9]+}/notabenoid/export", app.ExportToNotabenoid).
		Methods("POST")
	r.HandleFunc("/book/{book_id:[0-9]+}/notabenoid/update", app.UpdateFromNotabenoid).
		Methods("POST")
	r.HandleFunc("/book/{book_id:[0-9]+}/notabenoid/compare", app.CompareToNotabenoid).
		Methods("POST")
	r.HandleFunc("/book/{book_id:[0-9]+}/snr", app.SearchAndReplace).
		Methods("POST")

	if flag.Lookup("test.timeout") == nil { // we aren't within a test
		for _, p := range plugins.ListMTPlugins() {
			r.HandleFunc("/mt/"+p.Name, p.HandlerFunc).Methods("GET")
		}
	} else {
		r.HandleFunc("/mt/{_:[^/]+}", func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Type", "application/json")
			_, _ = w.Write([]byte(`{"error":""}`))
		}).Methods("GET")
	}

	fileServer := http.FileServer(http.FS(fsys))
	defaultLastModified := time.Unix(0, 0).Format("Mon, 02 Jan 2006 15:04:05 GMT")
	r.HandleFunc("/{_:(?:css|js|fonts)/.+}", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Last-Modified", defaultLastModified)
		w.Header().Add("Cache-Control", "max-age: 31536000, immutable")
		fileServer.ServeHTTP(w, r)
	}).Methods("GET")

	if *withTM {
		plugins.Register("concordance", app.ConcordancePlugin)
	}

	for _, p := range plugins.List() {
		r.HandleFunc("/plugins/"+p.Name, p.HandlerFunc).Methods("GET")
	}

	log.Println("listening on", *addr)
	log.Fatal(http.ListenAndServe(*addr, r))
}
