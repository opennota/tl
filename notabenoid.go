// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/gorilla/mux"
	"gitlab.com/opennota/tl/diff"
)

type NotabenoidClient struct {
	chapterURL *url.URL
	*http.Client
}

func newNotabenoidClient(chapterURL *url.URL) *NotabenoidClient {
	jar, _ := cookiejar.New(nil)
	return &NotabenoidClient{
		chapterURL,
		&http.Client{
			Jar: jar,
			Transport: &http.Transport{
				Proxy: http.ProxyFromEnvironment,
				DialContext: (&net.Dialer{
					Timeout:   30 * time.Second,
					KeepAlive: 30 * time.Second,
					DualStack: true,
				}).DialContext,
				IdleConnTimeout:       90 * time.Second,
				TLSHandshakeTimeout:   10 * time.Second,
				ExpectContinueTimeout: 1 * time.Second,
			},
		},
	}
}

func badGateway(w http.ResponseWriter, err error) {
	logError(err)
	http.Error(w, err.Error(), http.StatusBadGateway)
}

func (c *NotabenoidClient) logIn(username, password string) error {
	values := url.Values{
		"login[login]": {username},
		"login[pass]":  {password},
	}
	loginURL := c.chapterURL.Scheme + "://" + c.chapterURL.Host + "/"
	resp, err := c.PostForm(loginURL, values)
	if err != nil {
		return fmt.Errorf("logging in: POST failed: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return fmt.Errorf(
			"logging in: remote server returned %d %s",
			resp.StatusCode,
			http.StatusText(resp.StatusCode),
		)
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("logging in: read failed: %v", err)
	}

	if !bytes.Contains(data, []byte(`<a href="/register/logout">`)) {
		return fmt.Errorf("logging in: invalid username/password?")
	}

	return nil
}

func (c *NotabenoidClient) postOriginal(ord int, text string) (string, error) {
	values := url.Values{
		"ajax":       {"1"},
		"Orig[ord]":  {fmt.Sprint(ord)},
		"Orig[body]": {text},
	}
	postURL := c.chapterURL.String() + "/0/edit"
	resp, err := c.PostForm(postURL, values)
	if err != nil {
		return "", fmt.Errorf("adding original: POST failed: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return "", fmt.Errorf(
			"adding original: remote server returned %d %s",
			resp.StatusCode,
			http.StatusText(resp.StatusCode),
		)
	}

	id := ""
	if err := json.NewDecoder(resp.Body).Decode(&struct {
		ID *string
	}{
		&id,
	}); err != nil {
		return "", fmt.Errorf("adding original: JSON decode failed: %v", err)
	}

	return id, nil
}

func (c *NotabenoidClient) postTranslation(oid, tid, text string) error {
	values := url.Values{
		"ajax":              {"1"},
		"Translation[body]": {text},
	}
	postURL := c.chapterURL.String() + "/" + oid + "/translate"
	if tid != "" {
		postURL += "?tr_id=" + tid
	}
	resp, err := c.PostForm(postURL, values)
	if err != nil {
		return fmt.Errorf("adding translation: POST failed: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return fmt.Errorf(
			"adding translation: remote server returned %d %s",
			resp.StatusCode,
			http.StatusText(resp.StatusCode),
		)
	}

	return nil
}

func (c *NotabenoidClient) removeTranslation(oid, tid string) error {
	values := url.Values{
		"tr_id": {tid},
	}
	postURL := c.chapterURL.String() + "/" + oid + "/tr_rm"
	resp, err := c.PostForm(postURL, values)
	if err != nil {
		return fmt.Errorf("removing translation: POST failed: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return fmt.Errorf(
			"removing translation: remote server returned %d %s",
			resp.StatusCode,
			http.StatusText(resp.StatusCode),
		)
	}

	return nil
}

func (c *NotabenoidClient) postComment(oid, text string) error {
	values := url.Values{
		"ajax":          {"1"},
		"Comment[body]": {"#exported#\n" + text},
		"Comment[pid]":  {"0"},
	}
	postURL := c.chapterURL.String() + "/" + oid + "/c0/reply"
	resp, err := c.PostForm(postURL, values)
	if err != nil {
		return fmt.Errorf("adding comment: POST failed: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return fmt.Errorf(
			"adding comment: remote server returned %d %s",
			resp.StatusCode,
			http.StatusText(resp.StatusCode),
		)
	}

	return nil
}

func (c *NotabenoidClient) removeExportedComment(oid, text string) (bool, error) {
	postURL := c.chapterURL.String() + "/" + oid + "/comments?ajax=1"
	resp, err := c.PostForm(postURL, nil)
	if err != nil {
		return false, fmt.Errorf("loading comments: POST failed: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return false, fmt.Errorf(
			"loading comments: remote server returned %d %s",
			resp.StatusCode,
			http.StatusText(resp.StatusCode),
		)
	}

	d, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		return false, fmt.Errorf("loading comments: cannot parse response: %v", err)
	}

	cid := ""
	d.Find(".thread > .comment[id^=cmt_]").Each(func(i int, sel *goquery.Selection) {
		oldText := sel.Find(".text").Text()
		if strings.HasPrefix(oldText, "#exported#") {
			oldText = strings.TrimPrefix(oldText, "#exported#\n")
			if oldText != text {
				cid = strings.TrimPrefix(sel.AttrOr("id", ""), "cmt_")
				return
			}
		}
	})

	if cid != "" {
		postURL := c.chapterURL.String() + "/" + oid + "/c" + cid + "/remove"
		values := url.Values{
			"ajax": {"1"},
			"id":   {cid},
		}
		resp, err := c.PostForm(postURL, values)
		if err != nil {
			return false, fmt.Errorf("removing comment: POST failed: %v", err)
		}
		defer resp.Body.Close()
		if resp.StatusCode != 200 {
			return false, fmt.Errorf(
				"removing comment: remote server returned %d %s",
				resp.StatusCode,
				http.StatusText(resp.StatusCode),
			)
		}
		return true, nil
	}

	return false, nil
}

func (a *App) ExportToNotabenoid(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	bid, err := u64(vars["book_id"])
	if err != nil {
		http.NotFound(w, r)
		return
	}

	chapterURL := r.FormValue("chapterUrl")
	u, err := url.Parse(chapterURL)
	if err != nil || u.Host == "" || u.RawQuery != "" {
		http.Error(w, "invalid URL", http.StatusBadRequest)
		return
	}

	book, err := a.db.BookWithTranslations(bid, 0, -1, fNone)
	if err != nil {
		if err == ErrNotFound {
			http.NotFound(w, r)
		} else {
			internalError(w, err)
		}
		return
	}

	c := newNotabenoidClient(u)
	if username := r.FormValue("username"); username != "" {
		password := r.FormValue("password")
		if err := c.logIn(username, password); err != nil {
			badGateway(w, err)
			return
		}
	}

	d, err := c.parsePage(1)
	if err != nil {
		badGateway(w, err)
		return
	}

	noDelete := r.FormValue("noDelete") != ""
	noNew := r.FormValue("noNew") != ""
	exportComments := r.FormValue("exportComments") != ""

	if d.Find("#alert-empty").Length() > 0 {
		for i, f := range book.Fragments {
			oid, err := c.postOriginal(i+1, f.Text)
			if err != nil {
				badGateway(w, err)
				return
			}
			for _, v := range f.Versions {
				if err := c.postTranslation(oid, "", v.Text); err != nil {
					badGateway(w, err)
					return
				}
			}
			if exportComments && f.Comment != "" {
				if err := c.postComment(oid, f.Comment); err != nil {
					badGateway(w, err)
					return
				}
			}
		}
	} else {
		var original []string
		var origIDs []string
		var translation [][]string
		var transIDs [][]string
		var commented []bool
		for page := 1; ; page++ {
			if page != 1 {
				d, err = c.parsePage(page)
				if err != nil {
					badGateway(w, err)
					return
				}
			}

			d.Find("#Tr > tbody > tr").Each(func(i int, row *goquery.Selection) {
				orig := strings.TrimSpace(row.Find(".o .text").Text())
				oid := strings.TrimPrefix(row.AttrOr("id", ""), "o")
				var trans []string
				var tids []string
				row.Find(".t > div > .text").Each(func(_ int, p *goquery.Selection) {
					text := strings.TrimSpace(p.Text())
					trans = append(trans, text)
					tid := strings.TrimPrefix(p.Parent().AttrOr("id", ""), "t")
					tids = append(tids, tid)
				})
				original = append(original, orig)
				origIDs = append(origIDs, oid)
				translation = append(translation, trans)
				transIDs = append(transIDs, tids)
				if exportComments {
					hasComment := row.Find(".c .icon-comment").Length() > 0
					commented = append(commented, hasComment)
				}
			})

			if d.Find(".chic-pages .right + a").Length() == 0 {
				break
			}
		}

		if len(book.Fragments) != len(original) {
			msg := fmt.Sprintf(
				"Cannot upload because of conflicts: there are %d local fragments, but %d remote fragments",
				len(book.Fragments),
				len(original),
			)
			http.Error(w, msg, http.StatusConflict)
			return
		}

		for i, f := range book.Fragments {
			if f.Text != original[i] {
				msg := fmt.Sprintf("Cannot upload because of conflicts: local and remote fragments #%d differ", i+1)
				http.Error(w, msg, http.StatusConflict)
				return
			}
		}
		for i, f := range book.Fragments {
			for j, v := range f.Versions {
				tid := ""
				if j < len(translation[i]) {
					if translation[i][j] == v.Text {
						continue
					}
					tid = transIDs[i][j]
				}
				if tid == "" && noNew {
					continue
				}
				if err := c.postTranslation(origIDs[i], tid, v.Text); err != nil {
					badGateway(w, err)
					return
				}
			}
			if noDelete {
				continue
			}
			for k := len(f.Versions); k < len(translation[i]); k++ {
				if err := c.removeTranslation(origIDs[i], transIDs[i][k]); err != nil {
					badGateway(w, err)
					return
				}
			}
			if exportComments && f.Comment != "" {
				post := true
				if commented[i] {
					if post, err = c.removeExportedComment(origIDs[i], f.Comment); err != nil {
						badGateway(w, err)
						return
					}
				}
				if post {
					if err := c.postComment(origIDs[i], f.Comment); err != nil {
						badGateway(w, err)
						return
					}
				}
			}
		}
	}

	w.Header().Add("Content-Type", "application/json")
	_ = json.NewEncoder(w).Encode(struct {
		URL string `json:"url"`
	}{
		chapterURL,
	})
}

func (c *NotabenoidClient) parsePage(page int) (*goquery.Document, error) {
	url := c.chapterURL.String()
	if page != 1 {
		url += "?Orig_page=" + fmt.Sprint(page)
	}
	resp, err := c.Get(url)
	if err != nil {
		return nil, fmt.Errorf("parsing page: GET failed: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return nil, fmt.Errorf(
			"parsing page: remote server returned %d %s",
			resp.StatusCode,
			http.StatusText(resp.StatusCode),
		)
	}

	d, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("parsing page: parse failed: %v", err)
	}

	return d, nil
}

func (c *NotabenoidClient) downloadChapter() ([]string, [][]ImportedVersion, error) {
	var original []string
	var translation [][]ImportedVersion
	for page := 1; ; page++ {
		d, err := c.parsePage(page)
		if err != nil {
			return nil, nil, err
		}

		d.Find("#Tr > tbody > tr").Each(func(i int, row *goquery.Selection) {
			orig := strings.TrimSpace(row.Find(".o .text").Text())
			var trans []ImportedVersion
			row.Find(".t > div > .text").Each(func(_ int, par *goquery.Selection) {
				text := strings.TrimSpace(par.Text())
				username := par.Parent().Find(".info > .user").Text()
				trans = append(trans, ImportedVersion{
					Text:   text,
					Author: username,
				})
			})
			original = append(original, orig)
			translation = append(translation, trans)
		})

		if d.Find(".chic-pages .right + a").Length() == 0 {
			break
		}
	}
	return original, translation, nil
}

func (a *App) UpdateFromNotabenoid(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	bid, err := u64(vars["book_id"])
	if err != nil {
		http.NotFound(w, r)
		return
	}

	chapterURL := r.FormValue("chapterUrl")
	u, err := url.Parse(chapterURL)
	if err != nil || u.Host == "" || u.RawQuery != "" {
		http.Error(w, "invalid URL", http.StatusBadRequest)
		return
	}

	if _, err := a.db.BookByID(bid); err != nil {
		if err == ErrNotFound {
			http.NotFound(w, r)
		} else {
			internalError(w, err)
		}
		return
	}

	c := newNotabenoidClient(u)
	if username := r.FormValue("username"); username != "" {
		password := r.FormValue("password")
		if err := c.logIn(username, password); err != nil {
			badGateway(w, err)
			return
		}
	}

	orig, trans, err := c.downloadChapter()
	if err != nil {
		badGateway(w, err)
		return
	}

	noDelete := r.FormValue("noDelete") != ""
	noNew := r.FormValue("noNew") != ""

	if err := a.db.SafelyUpdate(bid, orig, trans, noDelete, noNew); err != nil {
		if err == ErrConflict {
			http.Error(w, "Cannot update because of conflicts", http.StatusConflict)
		} else {
			internalError(w, err)
		}
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

func (a *App) ImportFromNotabenoid(w http.ResponseWriter, r *http.Request) {
	const errorURL = "/add?type=notabenoid"
	sess, _ := store.Get(r, "tl_sess")

	title := strings.TrimSpace(r.FormValue("title"))
	if title == "" {
		sess.AddFlash("Title must not be empty!")
		_ = sess.Save(r, w)
		http.Redirect(w, r, errorURL, http.StatusSeeOther)
		return
	}
	sess.Values["title"] = title

	chapterURL := r.FormValue("chapter-url")
	u, err := url.Parse(chapterURL)
	if err != nil || u.Host == "" || u.RawQuery != "" {
		sess.AddFlash("Invalid URL")
		_ = sess.Save(r, w)
		http.Redirect(w, r, errorURL, http.StatusSeeOther)
		return
	}

	c := newNotabenoidClient(u)
	if username := r.FormValue("nb-user"); username != "" {
		password := r.FormValue("nb-pass")
		if err := c.logIn(username, password); err != nil {
			sess.AddFlash("Bad gateway (invalid username/password?)")
			_ = sess.Save(r, w)
			http.Redirect(w, r, errorURL, http.StatusSeeOther)
			return
		}
	}

	orig, trans, err := c.downloadChapter()
	if err != nil {
		badGateway(w, err)
		return
	}

	bid, err := a.db.AddTranslatedBookWithMultipleVersions(title, orig, trans)
	if err != nil {
		logError(err)
		sess.AddFlash("Internal server error")
		_ = sess.Save(r, w)
		http.Redirect(w, r, errorURL, http.StatusSeeOther)
		return
	}
	http.Redirect(w, r, "/book/"+fmt.Sprint(bid), http.StatusSeeOther)
}

func (a *App) CompareToNotabenoid(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	bid, err := u64(vars["book_id"])
	if err != nil {
		http.NotFound(w, r)
		return
	}

	chapterURL := r.FormValue("chapterUrl")
	u, err := url.Parse(chapterURL)
	if err != nil || u.Host == "" || u.RawQuery != "" {
		http.Error(w, "invalid URL", http.StatusBadRequest)
		return
	}

	book, err := a.db.BookWithTranslations(bid, 0, -1, fNone)
	if err != nil {
		if err == ErrNotFound {
			http.NotFound(w, r)
		} else {
			internalError(w, err)
		}
		return
	}

	c := newNotabenoidClient(u)
	if username := r.FormValue("username"); username != "" {
		password := r.FormValue("password")
		if err := c.logIn(username, password); err != nil {
			badGateway(w, err)
			return
		}
	}

	original, translation, err := c.downloadChapter()
	if err != nil {
		badGateway(w, err)
		return
	}

	if len(original) != len(book.FragmentsIDs) {
		msg := fmt.Sprintf(
			"Cannot compare because of conflicts: there are %d local fragments, but %d remote fragments",
			len(book.Fragments),
			len(original),
		)
		http.Error(w, msg, http.StatusConflict)
		return
	}
	for i, f := range book.Fragments {
		if f.Text != original[i] {
			msg := fmt.Sprintf("Cannot compare because of conflicts: local and remote fragments #%d differ", i+1)
			http.Error(w, msg, http.StatusConflict)
			return
		}
	}

	reverse := r.FormValue("reverse") != ""
	h1, h2 := "notabenoid", "local"
	if reverse {
		h1, h2 = h2, h1
	}

	var buf bytes.Buffer
	fmt.Fprintf(&buf, `<!DOCTYPE html><html><head><meta charset="utf-8"><style>
.diff { border-collapse: collapse; }
.diff-row {
  border-top: 1px dashed #777;
  border-bottom: 1px dashed #777;
}
.diff__added0 { background-color: #d6ffe2; }
.diff__added1,
.diff__added0 .diff__added1 {
  background-color: #acf2bd;
}
.diff__removed0 { background-color: #ffdee2; }
.diff__removed1,
.diff__removed0 .diff__removed1 {
  background-color: #fdb8c0;
}
</style></head><body><table class="diff"><thead>
<tr>
<th>#</th>
<th>%s</th>
<th>%s</th>
</tr>
</thead><tbody>`, h1, h2)

	lasti := -1
	for i, f := range book.Fragments {
		for j, v := range f.Versions {
			if j < len(translation[i]) {
				if strings.TrimSpace(translation[i][j].Text) == strings.TrimSpace(v.Text) {
					continue
				}

				var left, right string
				if reverse {
					left, right = diff.TwoSideHTML(v.Text, translation[i][j].Text)
				} else {
					left, right = diff.TwoSideHTML(translation[i][j].Text, v.Text)
				}

				fmt.Fprintf(&buf, `<tr class="diff-row"><td>`)
				if i != lasti {
					fmt.Fprintf(&buf, "#%d", i+1)
				}
				lasti = i
				fmt.Fprintf(&buf, `</td><td>%s</td><td>%s</td></tr>`, left, right)
			} else {
				fmt.Fprintf(&buf, `<tr class="diff-row"><td>`)
				if i != lasti {
					fmt.Fprintf(&buf, "#%d", i+1)
				}
				lasti = i
				if reverse {
					fmt.Fprintf(&buf, `</td><td class="diff__removed0">%s</td><td></td></tr>`, v.Text)
				} else {
					fmt.Fprintf(&buf, `</td><td></td><td class="diff__added0">%s</td></tr>`, v.Text)
				}
			}
		}
		for k := len(f.Versions); k < len(translation[i]); k++ {
			fmt.Fprintf(&buf, `<tr class="diff-row"><td>`)
			if i != lasti {
				fmt.Fprintf(&buf, "#%d", i+1)
			}
			lasti = i
			if reverse {
				fmt.Fprintf(&buf, `</td><td></td><td class="diff__added0">%s</td></tr>`, translation[i][k].Text)
			} else {
				fmt.Fprintf(&buf, `</td><td class="diff__removed0">%s</td><td></td></tr>`, translation[i][k].Text)
			}
		}
	}
	fmt.Fprintf(&buf, `</tbody></table>`)

	if lasti != -1 {
		_, _ = buf.WriteTo(w)
	} else {
		w.WriteHeader(http.StatusNoContent)
	}
}
