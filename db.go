// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"errors"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"
	"unicode"
	"unicode/utf8"

	"github.com/boltdb/bolt"
	"gitlab.com/opennota/substring"
)

var (
	ErrNotFound      = errors.New("not found")
	ErrInvalidOffset = errors.New("invalid offset")
	ErrMarkNotFound  = errors.New("marked text not found")
	ErrConflict      = errors.New("conflict")

	rWord   = regexp.MustCompile(`\S+`)
	rLetter = regexp.MustCompile(`\pL`)
)

type DB struct {
	*bolt.DB
}

type Book struct {
	ID                  uint64    `json:"id"`
	Title               string    `json:"title"`
	Created             time.Time `json:"created"`
	FragmentsTotal      int       `json:"fragments_total"`
	FragmentsTranslated int       `json:"fragments_translated"`
	FragmentsIDs        []uint64  `json:"fragments_ids"`
	LastActivity        time.Time `json:"last_activity"`
	LastVisitedPage     int       `json:"last_visited_page"`
	Glossary            Glossary  `json:"glossary,omitempty"`

	Fragments []Fragment `json:"-"`
}

type Fragment struct {
	ID          uint64    `json:"id"`
	Created     time.Time `json:"created"`
	Updated     time.Time `json:"updated"`
	Text        string    `json:"text"`
	Comment     string    `json:"comment"`
	Starred     bool      `json:"starred"`
	VersionsIDs []uint64  `json:"versions_ids"`

	Versions []TranslationVersion `json:"-"`
	SeqNum   int                  `json:"-"`
}

type TranslationVersion struct {
	ID      uint64    `json:"id"`
	Created time.Time `json:"created"`
	Updated time.Time `json:"updated"`
	Text    string    `json:"text"`
	Marks   []string  `json:"marks"`
	Author  string    `json:"author,omitempty"`
}

type ImportedVersion struct {
	Text   string
	Author string
}

type Glossary map[string]TermDef

type TermDef struct {
	Def     string `json:"def"`
	Comment string `json:"comment,omitempty"`
}

type Term struct {
	Term string `json:"term"`
	TermDef
}

type Stats struct {
	NumFragments    int `json:"num_fragments"`
	NumTranslated   int `json:"num_translated"`
	OrigWords       int `json:"orig_words"`
	OrigChars       int `json:"orig_chars"`
	TransWords      int `json:"trans_words"`
	TransChars      int `json:"trans_chars"`
	TransWordsToday int `json:"trans_words_today"`
	TransCharsToday int `json:"trans_chars_today"`
	TransWordsYday  int `json:"trans_words_yday"`
	TransCharsYday  int `json:"trans_chars_yday"`
}

func encode(v uint64) []byte {
	b := make([]byte, 8)
	binary.LittleEndian.PutUint64(b, v)
	return b
}

func marshal(b *bolt.Bucket, key uint64, val interface{}) error {
	data, err := json.Marshal(val)
	if err != nil {
		return err
	}
	return b.Put(encode(key), data)
}

func unmarshal(b *bolt.Bucket, key uint64, val interface{}) (bool, error) {
	data := b.Get(encode(key))
	if data == nil {
		return false, nil
	}
	if err := json.Unmarshal(data, val); err != nil {
		return true, err
	}
	return true, nil
}

func OpenDatabase(path string, mode os.FileMode, options *bolt.Options) (DB, error) {
	db, err := bolt.Open(path, mode, options)
	if err != nil {
		return DB{}, err
	}
	if err := db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte("index"))
		if err != nil {
			return err
		}
		_, err = tx.CreateBucketIfNotExists([]byte("fragments"))
		if err != nil {
			return err
		}
		_, err = tx.CreateBucketIfNotExists([]byte("versions"))
		if err != nil {
			return err
		}
		return nil
	}); err != nil {
		return DB{}, err
	}
	return DB{db}, nil
}

func (g Glossary) AlphabeticalTerms() []Term {
	if g == nil {
		return nil
	}
	terms := make([]Term, 0, len(g))
	for k, v := range g {
		terms = append(terms, Term{
			Term:    k,
			TermDef: v,
		})
	}
	sort.Slice(terms, func(i, j int) bool {
		return terms[i].Term < terms[j].Term
	})
	return terms
}

func firstRune(s string) rune {
	for _, r := range s {
		return r
	}
	panic("unreachable")
}

func (g Glossary) TermsRegexp() *regexp.Regexp {
	if g == nil || len(g) == 0 {
		return nil
	}
	terms := make([]string, 0, len(g))
	for k := range g {
		terms = append(terms, k)
	}
	sort.Slice(terms, func(i, j int) bool {
		if ll := len(terms[i]) - len(terms[j]); ll != 0 {
			return ll > 0
		}
		return terms[i] < terms[j]
	})
	for i, term := range terms {
		terms[i] = regexp.QuoteMeta(term)
		if !unicode.IsUpper(firstRune(term)) {
			terms[i] = "(?i:" + terms[i] + ")"
		}
	}
	return regexp.MustCompile(strings.Join(terms, "|"))
}

func (db *DB) Books() ([]Book, error) {
	var books []Book
	err := db.View(func(tx *bolt.Tx) error {
		books = books[:0]
		b := tx.Bucket([]byte("index"))
		c := b.Cursor()
		for k, v := c.First(); k != nil; k, v = c.Next() {
			var book Book
			if err := json.Unmarshal(v, &book); err != nil {
				return err
			}
			books = append(books, book)
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return books, nil
}

func (db *DB) BooksByActivity() ([]Book, error) {
	books, err := db.Books()
	if err != nil {
		return nil, err
	}
	sort.Slice(books, func(i, j int) bool {
		if a, b := books[i].LastActivity, books[j].LastActivity; !a.Equal(b) {
			return b.Before(a)
		}
		return books[i].ID > books[j].ID
	})
	return books, nil
}

var rDigitSequence = regexp.MustCompile(`[0-9]+`)

func lessConsideringDigits(s1, s2 string) bool {
	if s1 == s2 {
		return false
	}
	if !rDigitSequence.MatchString(s1) || !rDigitSequence.MatchString(s2) {
		return s1 < s2
	}
	if rDigitSequence.ReplaceAllString(s1, "#") != rDigitSequence.ReplaceAllString(s2, "#") {
		return s1 < s2
	}
	numbers1 := rDigitSequence.FindAllString(s1, -1)
	numbers2 := rDigitSequence.FindAllString(s2, -1)
	if len(numbers1) != len(numbers2) {
		return s1 < s2
	}

	for i := range numbers1 {
		n1, _ := strconv.Atoi(numbers1[i])
		n2, _ := strconv.Atoi(numbers2[i])
		if c := n1 - n2; c < 0 {
			return true
		} else if c > 0 {
			return false
		}
	}

	return false
}

func (db *DB) BooksByTitle() ([]Book, error) {
	books, err := db.Books()
	if err != nil {
		return nil, err
	}
	sort.Slice(books, func(i, j int) bool {
		return lessConsideringDigits(books[i].Title, books[j].Title)
	})
	return books, nil
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

type filterKind int

const (
	fNone filterKind = iota
	fUntranslated
	fCommented
	fStarred
	fMarked
	fWithTwoOrMoreVersions
	fOriginalContains
	fTranslationContains
	fTranslatedWhen
)

func wordCount(s string) int {
	return len(rWord.FindAllString(s, -1))
}

type matcher interface {
	Match(string) bool
}

type regexpMatcher struct{ r *regexp.Regexp }

func (m regexpMatcher) Match(s string) bool {
	return m.r.MatchString(s)
}

func digit(b byte) bool { return b >= '0' && b <= '9' }

func dayEquals(d1, d2 time.Time) bool {
	return d1.Day() == d2.Day() &&
		d1.Month() == d2.Month() &&
		d1.Year() == d2.Year()
}

func (db *DB) BookWithTranslations(bid uint64, from, size int, filter filterKind, filterArg ...string) (Book, error) {
	var book Book
	if err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("index"))
		if found, err := unmarshal(b, bid, &book); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}

		if from >= len(book.FragmentsIDs) && from > 0 {
			return ErrInvalidOffset
		}

		if size == -1 {
			size = len(book.FragmentsIDs)
		}

		var m matcher
		if filter == fOriginalContains || filter == fTranslationContains {
			what := filterArg[0]
			if rRegexpWithModifiers.MatchString(what) {
				i := strings.LastIndexByte(what, '/')
				x := what[1:i]
				if i != len(what)-1 {
					x = "(?" + what[i+1:] + ")" + x
				}
				r, err := regexp.Compile(x)
				if err == nil {
					m = regexpMatcher{r}
				}
			}
			if m == nil {
				m = substring.NewMatcher(what)
			}
		}

		var todayOrYesterday time.Time
		if filter == fTranslatedWhen {
			todayOrYesterday = time.Now()
			if filterArg[0] == "yesterday" {
				todayOrYesterday = todayOrYesterday.Add(-24 * time.Hour)
			}
		}

		fb := tx.Bucket([]byte("fragments")).Bucket(encode(bid))
		vb := tx.Bucket([]byte("versions")).Bucket(encode(bid))
		origFragmentIDs := book.FragmentsIDs

		if filter != fNone {
			filtered := make([]uint64, 0, len(book.FragmentsIDs))

			switch filter {
			case fNone:
				// no-op
			case fUntranslated:
				for _, fid := range book.FragmentsIDs {
					data := fb.Get(encode(fid))
					needle := []byte(`"versions_ids":[`)
					if i := bytes.Index(data, needle); i != -1 {
						if i += len(needle); i < len(data) && digit(data[i]) {
							continue
						}
					}
					filtered = append(filtered, fid)
				}
			case fStarred:
				ifStarred := filterArg[0] == "s"
				for _, fid := range book.FragmentsIDs {
					data := fb.Get(encode(fid))
					if bytes.Contains(data, []byte(`"starred":true`)) != ifStarred {
						continue
					}
					filtered = append(filtered, fid)
				}
			case fCommented:
				needle := []byte(`"comment":""`)
				for _, fid := range book.FragmentsIDs {
					data := fb.Get(encode(fid))
					if bytes.Contains(data, needle) {
						continue
					}
					filtered = append(filtered, fid)
				}
			case fWithTwoOrMoreVersions:
				for _, fid := range book.FragmentsIDs {
					data := fb.Get(encode(fid))
					if data == nil {
						continue
					}
					var tmp struct {
						IDs []uint64 `json:"versions_ids"`
					}
					if err := json.Unmarshal(data, &tmp); err != nil {
						return err
					}
					if len(tmp.IDs) < 2 {
						continue
					}
					filtered = append(filtered, fid)
				}
			case fOriginalContains:
				for _, fid := range book.FragmentsIDs {
					data := fb.Get(encode(fid))
					if data == nil {
						continue
					}
					var tmp struct{ Text string }
					if err := json.Unmarshal(data, &tmp); err != nil {
						return err
					}
					if !m.Match(tmp.Text) {
						continue
					}
					filtered = append(filtered, fid)
				}
			case fTranslationContains:
				for _, fid := range book.FragmentsIDs {
					var f Fragment
					if _, err := unmarshal(fb, fid, &f); err != nil {
						return err
					}
					for _, vid := range f.VersionsIDs {
						var v TranslationVersion
						if found, err := unmarshal(vb, vid, &v); err != nil {
							return err
						} else if !found {
							continue
						}
						if m.Match(v.Text) {
							filtered = append(filtered, fid)
							break
						}
					}
				}
			case fMarked:
				for _, fid := range book.FragmentsIDs {
					var f Fragment
					if _, err := unmarshal(fb, fid, &f); err != nil {
						return err
					}
					for _, vid := range f.VersionsIDs {
						var v TranslationVersion
						if found, err := unmarshal(vb, vid, &v); err != nil {
							return err
						} else if !found {
							continue
						}
						if len(v.Marks) > 0 {
							filtered = append(filtered, fid)
							break
						}
					}
				}
			case fTranslatedWhen:
				needle := []byte(`"created":"` + todayOrYesterday.Format("2006-01-02"))
				for _, fid := range book.FragmentsIDs {
					var f Fragment
					if _, err := unmarshal(fb, fid, &f); err != nil {
						return err
					}
					for _, vid := range f.VersionsIDs {
						data := vb.Get(encode(vid))
						if bytes.Contains(data, needle) {
							filtered = append(filtered, fid)
							break
						}
					}
				}
			}

			book.FragmentsIDs = filtered
		}

		if from >= len(book.FragmentsIDs) {
			return nil
		}

		to := min(len(book.FragmentsIDs), from+size)
		for _, fid := range book.FragmentsIDs[from:to] {
			var f Fragment
			if _, err := unmarshal(fb, fid, &f); err != nil {
				return err
			}

			if filter != fUntranslated {
				for _, vid := range f.VersionsIDs {
					var v TranslationVersion
					if found, err := unmarshal(vb, vid, &v); err != nil {
						return err
					} else if !found {
						continue
					}
					if filter == fTranslationContains && !m.Match(v.Text) ||
						filter == fMarked && len(v.Marks) == 0 ||
						filter == fTranslatedWhen && !dayEquals(v.Created, todayOrYesterday) {
						continue
					}
					f.Versions = append(f.Versions, v)
				}
			}

			if (filter == fTranslationContains || filter == fMarked || filter == fTranslatedWhen) && len(f.Versions) == 0 {
				continue
			}

			f.SeqNum = 1 + idx(origFragmentIDs, f.ID)

			book.Fragments = append(book.Fragments, f)
		}

		return nil
	}); err != nil {
		if err == ErrInvalidOffset {
			return book, err
		}
		return Book{}, err
	}
	return book, nil
}

func (db *DB) BookByID(bid uint64) (Book, error) {
	var book Book
	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("index"))
		if found, err := unmarshal(b, bid, &book); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}
		return nil
	})
	if err != nil {
		return Book{}, err
	}
	return book, nil
}

func (db *DB) NeighborBooks(bid uint64) (*Book, *Book, error) {
	var prev, next *Book
	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("index"))
		c := b.Cursor()
		key := encode(bid)

		k, _ := c.Seek(key)
		if k == nil {
			return ErrNotFound
		}
		k, v := c.Prev()
		if k != nil {
			if err := json.Unmarshal(v, &prev); err != nil {
				return err
			}
		}

		c.Seek(key)
		k, v = c.Next()
		if k != nil {
			if err := json.Unmarshal(v, &next); err != nil {
				return err
			}
		}

		return nil
	})
	if err != nil {
		return nil, nil, err
	}
	return prev, next, nil
}

func (db *DB) AddBook(title string, fragments []string, autotranslate bool) (uint64, error) {
	now := time.Now()
	var bid uint64
	err := db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("index"))
		bid, _ = b.NextSequence()

		fb, err := tx.Bucket([]byte("fragments")).CreateBucket(encode(bid))
		if err != nil {
			return err
		}
		vb, err := tx.Bucket([]byte("versions")).CreateBucket(encode(bid))
		if err != nil {
			return err
		}

		ids := make([]uint64, len(fragments))
		fragmentsTranslated := 0
		for i := range fragments {
			versionsIDs := []uint64{}
			if autotranslate && !rLetter.MatchString(fragments[i]) {
				vid, _ := vb.NextSequence()
				if err := marshal(vb, vid, TranslationVersion{
					ID:      vid,
					Created: now,
					Updated: now,
					Text:    fragments[i],
				}); err != nil {
					return err
				}
				fragmentsTranslated++
				versionsIDs = append(versionsIDs, vid)
			}

			fid, _ := fb.NextSequence()
			ids[i] = fid
			if err := marshal(fb, fid, Fragment{
				ID:          fid,
				Created:     now,
				Updated:     now,
				Text:        fragments[i],
				VersionsIDs: versionsIDs,
			}); err != nil {
				return err
			}
		}

		return marshal(b, bid, Book{
			ID:                  bid,
			Title:               title,
			Created:             now,
			FragmentsTotal:      len(fragments),
			FragmentsTranslated: fragmentsTranslated,
			FragmentsIDs:        ids,
		})
	})
	if err != nil {
		return 0, err
	}
	return bid, nil
}

func (db *DB) AddTranslatedBook(title string, fragments [][]string) (uint64, error) {
	now := time.Now()
	var bid uint64
	err := db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("index"))
		bid, _ = b.NextSequence()

		fb, err := tx.Bucket([]byte("fragments")).CreateBucket(encode(bid))
		if err != nil {
			return err
		}
		vb, err := tx.Bucket([]byte("versions")).CreateBucket(encode(bid))
		if err != nil {
			return err
		}

		fragmentsTranslated := 0
		ids := make([]uint64, len(fragments))
		for i := range fragments {
			versionsIDs := []uint64{}
			translationText := strings.TrimSpace(fragments[i][1])
			if translationText != "" {
				vid, _ := vb.NextSequence()
				vers := TranslationVersion{
					ID:      vid,
					Created: now,
					Updated: now,
					Text:    translationText,
				}
				if err := marshal(vb, vid, vers); err != nil {
					return err
				}
				versionsIDs = []uint64{vid}
				fragmentsTranslated++
			}
			fid, _ := fb.NextSequence()
			ids[i] = fid
			if err := marshal(fb, fid, Fragment{
				ID:          fid,
				Created:     now,
				Updated:     now,
				Text:        strings.TrimSpace(fragments[i][0]),
				VersionsIDs: versionsIDs,
			}); err != nil {
				return err
			}
		}

		return marshal(b, bid, Book{
			ID:                  bid,
			Title:               title,
			Created:             now,
			FragmentsTotal:      len(fragments),
			FragmentsTranslated: fragmentsTranslated,
			FragmentsIDs:        ids,
		})
	})
	if err != nil {
		return 0, err
	}
	return bid, nil
}

func (db *DB) AddTranslatedBookWithMultipleVersions(title string, orig []string, trans [][]ImportedVersion) (uint64, error) {
	now := time.Now()
	var bid uint64
	err := db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("index"))
		bid, _ = b.NextSequence()

		fb, err := tx.Bucket([]byte("fragments")).CreateBucket(encode(bid))
		if err != nil {
			return err
		}
		vb, err := tx.Bucket([]byte("versions")).CreateBucket(encode(bid))
		if err != nil {
			return err
		}

		fragmentsTranslated := 0
		ids := make([]uint64, len(orig))
		for i, origText := range orig {
			versionsIDs := []uint64{}
			translated := false
			for _, v := range trans[i] {
				translationText := strings.TrimSpace(v.Text)
				if translationText == "" {
					continue
				}
				vid, _ := vb.NextSequence()
				vers := TranslationVersion{
					ID:      vid,
					Created: now,
					Updated: now,
					Text:    translationText,
					Author:  v.Author,
				}
				if err := marshal(vb, vid, vers); err != nil {
					return err
				}
				versionsIDs = append(versionsIDs, vid)
				translated = true
			}
			if translated {
				fragmentsTranslated++
			}

			fid, _ := fb.NextSequence()
			ids[i] = fid
			if err := marshal(fb, fid, Fragment{
				ID:          fid,
				Created:     now,
				Updated:     now,
				Text:        strings.TrimSpace(origText),
				VersionsIDs: versionsIDs,
			}); err != nil {
				return err
			}
		}

		return marshal(b, bid, Book{
			ID:                  bid,
			Title:               title,
			Created:             now,
			FragmentsTotal:      len(orig),
			FragmentsTranslated: fragmentsTranslated,
			FragmentsIDs:        ids,
		})
	})
	if err != nil {
		return 0, err
	}
	return bid, nil
}

func (db *DB) UpdateBookTitle(bid uint64, title string) error {
	return db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("index"))
		var book Book
		if found, err := unmarshal(b, bid, &book); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}
		book.Title = title
		return marshal(b, bid, book)
	})
}

func (db *DB) RemoveBook(bid uint64) error {
	return db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("index"))
		key := encode(bid)
		if b.Get(key) == nil {
			return ErrNotFound
		}
		return b.Delete(key)
	})
}

func idx(a []uint64, v uint64) int {
	for i, w := range a {
		if w == v {
			return i
		}
	}
	return -1
}

func has(a []uint64, v uint64) bool {
	return idx(a, v) != -1
}

func (db *DB) AddFragment(bid, fidAfter, fidBefore uint64, text string) (Fragment, error) {
	now := time.Now()
	var f Fragment
	if err := db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("index"))
		var book Book
		if found, err := unmarshal(b, bid, &book); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}

		fb := tx.Bucket([]byte("fragments")).Bucket(encode(bid))
		fid, _ := fb.NextSequence()
		f = Fragment{
			ID:          fid,
			Created:     now,
			Updated:     now,
			Text:        text,
			VersionsIDs: []uint64{},
		}
		if fidAfter == 0 && fidBefore == 0 {
			book.FragmentsIDs = append([]uint64{fid}, book.FragmentsIDs...)
			f.SeqNum = 1
		} else {
			fidAB := fidAfter
			if fidAB == 0 {
				fidAB = fidBefore
			}
			findex := idx(book.FragmentsIDs, fidAB)
			if findex == -1 {
				return ErrNotFound
			}
			if fidAB == fidAfter {
				findex++
			}
			fids := make([]uint64, 0, len(book.FragmentsIDs)+1)
			fids = append(fids, book.FragmentsIDs[:findex]...)
			fids = append(fids, fid)
			fids = append(fids, book.FragmentsIDs[findex:]...)
			book.FragmentsIDs = fids
			f.SeqNum = findex + 1
		}
		book.FragmentsTotal++

		if err := marshal(fb, fid, f); err != nil {
			return err
		}

		book.LastActivity = now

		return marshal(b, bid, book)
	}); err != nil {
		return Fragment{}, err
	}
	return f, nil
}

func (db *DB) UpdateFragment(bid, fid uint64, text string) (string, error) {
	now := time.Now()
	var oldText string
	if err := db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("index"))
		var book Book
		if found, err := unmarshal(b, bid, &book); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}
		if !has(book.FragmentsIDs, fid) {
			return ErrNotFound
		}

		fb := tx.Bucket([]byte("fragments")).Bucket(encode(bid))
		var f Fragment
		if found, err := unmarshal(fb, fid, &f); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}
		oldText = f.Text
		f.Text = text
		if err := marshal(fb, fid, f); err != nil {
			return err
		}

		book.LastActivity = now

		return marshal(b, bid, book)
	}); err != nil {
		return "", err
	}
	return oldText, nil
}

func (db *DB) RemoveFragment(bid, fid uint64) (string, int, error) {
	now := time.Now()
	var text string
	var fragmentsTranslated int
	if err := db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("index"))
		var book Book
		if found, err := unmarshal(b, bid, &book); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}
		findex := idx(book.FragmentsIDs, fid)
		if findex == -1 {
			return ErrNotFound
		}
		book.FragmentsIDs = append(book.FragmentsIDs[:findex], book.FragmentsIDs[findex+1:]...)

		fb := tx.Bucket([]byte("fragments")).Bucket(encode(bid))
		var f Fragment
		if found, err := unmarshal(fb, fid, &f); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}
		if err := fb.Delete(encode(fid)); err != nil {
			return err
		}
		text = f.Text

		book.LastActivity = now
		book.FragmentsTotal--
		if len(f.VersionsIDs) > 0 {
			book.FragmentsTranslated--
		}
		fragmentsTranslated = book.FragmentsTranslated

		return marshal(b, bid, book)
	}); err != nil {
		return "", 0, err
	}
	return text, fragmentsTranslated, nil
}

func (db *DB) StarFragment(bid, fid uint64) error {
	return db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("index"))
		var book Book
		if found, err := unmarshal(b, bid, &book); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}
		if !has(book.FragmentsIDs, fid) {
			return ErrNotFound
		}

		fb := tx.Bucket([]byte("fragments")).Bucket(encode(bid))
		var f Fragment
		if found, err := unmarshal(fb, fid, &f); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		} else if f.Starred {
			return nil
		}

		f.Starred = true

		return marshal(fb, fid, f)
	})
}

func (db *DB) UnstarFragment(bid, fid uint64) error {
	return db.Update(func(tx *bolt.Tx) error {
		fb := tx.Bucket([]byte("fragments")).Bucket(encode(bid))
		var f Fragment
		if found, err := unmarshal(fb, fid, &f); err != nil {
			return err
		} else if !found {
			return nil
		} else if !f.Starred {
			return nil
		}

		f.Starred = false

		return marshal(fb, fid, f)
	})
}

func (db *DB) CommentFragment(bid, fid uint64, text string) error {
	return db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("index"))
		var book Book
		if found, err := unmarshal(b, bid, &book); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}
		if !has(book.FragmentsIDs, fid) {
			return ErrNotFound
		}

		fb := tx.Bucket([]byte("fragments")).Bucket(encode(bid))
		var f Fragment
		if found, err := unmarshal(fb, fid, &f); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}

		f.Comment = text

		return marshal(fb, fid, f)
	})
}

func (db *DB) Translate(bid, fid, vidOrZero uint64, text string) (TranslationVersion, int, error) {
	var vers TranslationVersion
	now := time.Now()
	var fragmentsTranslated int
	err := db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("index"))
		var book Book
		if found, err := unmarshal(b, bid, &book); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}
		if !has(book.FragmentsIDs, fid) {
			return ErrNotFound
		}

		fb := tx.Bucket([]byte("fragments")).Bucket(encode(bid))
		var f Fragment
		if found, err := unmarshal(fb, fid, &f); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}
		if vidOrZero != 0 && !has(f.VersionsIDs, vidOrZero) {
			return ErrNotFound
		}

		book.LastActivity = now
		if len(f.VersionsIDs) == 0 {
			book.FragmentsTranslated++
		}
		fragmentsTranslated = book.FragmentsTranslated

		if err := marshal(b, bid, book); err != nil {
			return err
		}

		vb := tx.Bucket([]byte("versions")).Bucket(encode(bid))
		if vid := vidOrZero; vid == 0 {
			vid, _ = vb.NextSequence()
			f.VersionsIDs = append(f.VersionsIDs, vid)
			if err := marshal(fb, fid, f); err != nil {
				return err
			}
			vers.ID = vid
			vers.Created = now
		} else {
			if found, err := unmarshal(vb, vid, &vers); err != nil {
				return err
			} else if !found {
				return ErrNotFound
			}
		}

		vers.Updated = now
		vers.Text = text
		marks := vers.Marks[:0]
		for _, m := range vers.Marks {
			if !strings.Contains(vers.Text, m) {
				continue
			}
			marks = append(marks, m)
		}
		vers.Marks = marks

		return marshal(vb, vers.ID, vers)
	})
	if err != nil {
		return TranslationVersion{}, 0, err
	}
	return vers, fragmentsTranslated, nil
}

func (db *DB) RemoveVersion(bid, fid, vid uint64) (int, error) {
	now := time.Now()
	var fragmentsTranslated int
	if err := db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("index"))
		var book Book
		if found, err := unmarshal(b, bid, &book); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}
		if !has(book.FragmentsIDs, fid) {
			return ErrNotFound
		}

		fb := tx.Bucket([]byte("fragments")).Bucket(encode(bid))
		var f Fragment
		if found, err := unmarshal(fb, fid, &f); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}
		vindex := idx(f.VersionsIDs, vid)
		if vindex == -1 {
			return ErrNotFound
		}
		f.VersionsIDs = append(f.VersionsIDs[:vindex], f.VersionsIDs[vindex+1:]...)

		if err := marshal(fb, f.ID, f); err != nil {
			return err
		}

		book.LastActivity = now
		if len(f.VersionsIDs) == 0 {
			book.FragmentsTranslated--
		}
		fragmentsTranslated = book.FragmentsTranslated

		return marshal(b, bid, book)
	}); err != nil {
		return 0, err
	}
	return fragmentsTranslated, nil
}

func hasStr(a []string, s string) bool {
	for _, v := range a {
		if v == s {
			return true
		}
	}
	return false
}

func (db *DB) MarkVersion(bid, fid, vid uint64, mark string) (TranslationVersion, error) {
	now := time.Now()
	var v TranslationVersion
	if err := db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("index"))
		var book Book
		if found, err := unmarshal(b, bid, &book); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}

		fb := tx.Bucket([]byte("fragments")).Bucket(encode(bid))
		var f Fragment
		if found, err := unmarshal(fb, fid, &f); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}
		vindex := idx(f.VersionsIDs, vid)
		if vindex == -1 {
			return ErrNotFound
		}

		vb := tx.Bucket([]byte("versions")).Bucket(encode(bid))
		if found, err := unmarshal(vb, vid, &v); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}

		if hasStr(v.Marks, mark) {
			return nil
		}
		if !strings.Contains(v.Text, mark) {
			return ErrMarkNotFound
		}
		v.Marks = append(v.Marks, mark)

		if err := marshal(vb, v.ID, v); err != nil {
			return err
		}

		book.LastActivity = now

		return marshal(b, bid, book)
	}); err != nil {
		return TranslationVersion{}, err
	}
	return v, nil
}

func (db *DB) UnmarkVersion(bid, fid, vid uint64, mark string) (TranslationVersion, error) {
	now := time.Now()
	var v TranslationVersion
	if err := db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("index"))
		var book Book
		if found, err := unmarshal(b, bid, &book); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}

		fb := tx.Bucket([]byte("fragments")).Bucket(encode(bid))
		var f Fragment
		if found, err := unmarshal(fb, fid, &f); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}
		vindex := idx(f.VersionsIDs, vid)
		if vindex == -1 {
			return ErrNotFound
		}

		vb := tx.Bucket([]byte("versions")).Bucket(encode(bid))
		if found, err := unmarshal(vb, vid, &v); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}

		ml := strings.ToLower(mark)
		marks := v.Marks[:0]
		for _, m := range v.Marks {
			if strings.ToLower(m) == ml {
				continue
			}
			marks = append(marks, m)
		}
		v.Marks = marks

		if err := marshal(vb, v.ID, v); err != nil {
			return err
		}

		book.LastActivity = now

		return marshal(b, bid, book)
	}); err != nil {
		return TranslationVersion{}, err
	}
	return v, nil
}

func (db *DB) UpdateLastVisitedPage(bid uint64, page int) error {
	return db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("index"))
		var book Book
		if found, err := unmarshal(b, bid, &book); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}
		if book.LastVisitedPage == page {
			return nil
		}
		book.LastVisitedPage = page
		return marshal(b, bid, &book)
	})
}

func (db *DB) ExportBookToJSON(bid uint64) ([]byte, error) {
	var data []byte
	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("index"))
		var book Book
		if found, err := unmarshal(b, bid, &book); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}
		fb := tx.Bucket([]byte("fragments")).Bucket(encode(bid))
		vb := tx.Bucket([]byte("versions")).Bucket(encode(bid))
		fragments := make([]Fragment, 0, book.FragmentsTotal)
		versions := make([]TranslationVersion, 0, book.FragmentsTranslated)
		for _, fid := range book.FragmentsIDs {
			var f Fragment
			if _, err := unmarshal(fb, fid, &f); err != nil {
				return err
			}
			fragments = append(fragments, f)
			for _, vid := range f.VersionsIDs {
				var v TranslationVersion
				if _, err := unmarshal(vb, vid, &v); err != nil {
					return err
				}
				versions = append(versions, v)
			}
		}

		var err error
		data, err = json.Marshal(struct {
			Book      `json:"book"`
			Fragments []Fragment           `json:"fragments"`
			Versions  []TranslationVersion `json:"versions"`
		}{
			book,
			fragments,
			versions,
		})
		return err
	})
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (db *DB) ImportBookFromJSON(data []byte) (uint64, error) {
	var book Book
	var fragments []Fragment
	var versions []TranslationVersion
	if err := json.Unmarshal(data, &struct {
		Book      *Book
		Fragments *[]Fragment
		Versions  *[]TranslationVersion
	}{
		&book,
		&fragments,
		&versions,
	}); err != nil {
		return 0, err
	}
	if err := db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("index"))
		bid, _ := b.NextSequence()
		book.ID = bid
		fb, err := tx.Bucket([]byte("fragments")).CreateBucket(encode(bid))
		if err != nil {
			return err
		}
		vb, err := tx.Bucket([]byte("versions")).CreateBucket(encode(bid))
		if err != nil {
			return err
		}

		vmap := make(map[uint64]uint64)
		for _, v := range versions {
			vid, _ := vb.NextSequence()
			vmap[v.ID] = vid
			v.ID = vid
			if err := marshal(vb, vid, v); err != nil {
				return err
			}
		}
		fmap := make(map[uint64]uint64)
		for i, f := range fragments {
			fid, _ := fb.NextSequence()
			fmap[f.ID] = fid
			f.ID = fid
			book.FragmentsIDs[i] = fid
			for j, vid := range f.VersionsIDs {
				f.VersionsIDs[j] = vmap[vid]
			}
			if err := marshal(fb, fid, f); err != nil {
				return err
			}
		}

		return marshal(b, bid, book)
	}); err != nil {
		return 0, err
	}
	return book.ID, nil
}

func (db *DB) BookStats(bid uint64) (Stats, error) {
	var stats Stats
	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("index"))
		var book Book
		if found, err := unmarshal(b, bid, &book); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}
		fb := tx.Bucket([]byte("fragments")).Bucket(encode(bid))
		vb := tx.Bucket([]byte("versions")).Bucket(encode(bid))
		translated := 0
		origWords, origChars := 0, 0
		transWords, transChars := 0, 0
		transWordsToday, transCharsToday := 0, 0
		transWordsYday, transCharsYday := 0, 0
		today := time.Now()
		yesterday := today.Add(-24 * time.Hour)
		for _, fid := range book.FragmentsIDs {
			var f Fragment
			if _, err := unmarshal(fb, fid, &f); err != nil {
				return err
			}
			origWords += wordCount(f.Text)
			origChars += utf8.RuneCountInString(f.Text)
			if len(f.VersionsIDs) > 0 {
				translated++
			}
			for i, vid := range f.VersionsIDs {
				var v TranslationVersion
				if _, err := unmarshal(vb, vid, &v); err != nil {
					return err
				}
				wc, rc := wordCount(v.Text), utf8.RuneCountInString(v.Text)
				if i == 0 { // count only the first translation
					transWords += wc
					transChars += rc
				}
				if dayEquals(v.Created, today) {
					transWordsToday += wc
					transCharsToday += rc
				}
				if dayEquals(v.Created, yesterday) {
					transWordsYday += wc
					transCharsYday += rc
				}
			}
		}

		stats = Stats{
			NumFragments:    len(book.FragmentsIDs),
			NumTranslated:   translated,
			OrigWords:       origWords,
			OrigChars:       origChars,
			TransWords:      transWords,
			TransChars:      transChars,
			TransWordsToday: transWordsToday,
			TransCharsToday: transCharsToday,
			TransWordsYday:  transWordsYday,
			TransCharsYday:  transCharsYday,
		}
		return nil
	})
	if err != nil {
		return Stats{}, err
	}
	return stats, nil
}

type DayProgress struct {
	Level           int
	TranslatedWords int
	TranslatedChars int
	EditingLevel    int
	Edited          int
	Date            time.Time
}

func (db *DB) BookHeatmap(bid uint64, year int) (Book, int, []DayProgress, error) {
	var heatmap []DayProgress
	var book Book
	if err := db.View(func(tx *bolt.Tx) error {
		heatmap = heatmap[:0]
		b := tx.Bucket([]byte("index"))
		if found, err := unmarshal(b, bid, &book); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}
		fb := tx.Bucket([]byte("fragments")).Bucket(encode(bid))
		vb := tx.Bucket([]byte("versions")).Bucket(encode(bid))
		type dateKey struct {
			year  int
			month time.Month
			day   int
		}
		m := make(map[dateKey]DayProgress)
		for _, fid := range book.FragmentsIDs {
			var f Fragment
			if _, err := unmarshal(fb, fid, &f); err != nil {
				return err
			}
			for _, vid := range f.VersionsIDs {
				var v TranslationVersion
				if _, err := unmarshal(vb, vid, &v); err != nil {
					return err
				}
				if v.Created.Year() == year {
					wc, rc := wordCount(v.Text), utf8.RuneCountInString(v.Text)
					k := dateKey{year, v.Created.Month(), v.Created.Day()}
					progress := m[k]
					progress.TranslatedWords += wc
					progress.TranslatedChars += rc
					m[k] = progress
				}
				if v.Updated.Year() == year && !dayEquals(v.Created, v.Updated) {
					k := dateKey{year, v.Updated.Month(), v.Updated.Day()}
					progress := m[k]
					progress.Edited++
					m[k] = progress
				}
			}
		}

		d := time.Date(year, 1, 1, 0, 0, 0, 0, time.UTC)
		minChars, maxChars := 0, 0
		minEdited, maxEdited := 0, 0
		for d.Year() == year {
			k := dateKey{d.Year(), d.Month(), d.Day()}
			progress := m[k]
			progress.Date = d
			m[k] = progress
			heatmap = append(heatmap, m[k])

			chars := m[k].TranslatedChars
			if chars > 0 && (minChars == 0 || chars < minChars) {
				minChars = chars
			}
			if chars > maxChars {
				maxChars = chars
			}

			edited := m[k].Edited
			if edited > 0 && (minEdited == 0 || edited < minEdited) {
				minEdited = edited
			}
			if edited > maxEdited {
				maxEdited = edited
			}

			d = d.AddDate(0, 0, 1)
		}
		div := (maxChars-minChars)/4 + 1
		divE := (maxEdited-minEdited)/4 + 1
		for i, d := range heatmap {
			if d.TranslatedChars > 0 {
				d.Level = (d.TranslatedChars-minChars)/div + 1
				heatmap[i] = d
			}
			if d.Edited > 0 {
				d.EditingLevel = (d.Edited-minEdited)/divE + 1
				heatmap[i] = d
			}
		}
		return nil
	}); err != nil {
		return Book{}, 0, nil, err
	}

	weekday := int(heatmap[0].Date.Weekday()+7) % 8

	return book, weekday, heatmap, nil
}

func (db *DB) SafelyUpdate(bid uint64, original []string, translation [][]ImportedVersion, noDelete, noNew bool) error {
	now := time.Now()
	return db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("index"))
		var book Book
		if found, err := unmarshal(b, bid, &book); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}
		if len(original) != len(book.FragmentsIDs) {
			return ErrConflict
		}
		fb := tx.Bucket([]byte("fragments")).Bucket(encode(bid))
		vb := tx.Bucket([]byte("versions")).Bucket(encode(bid))
		var fragments []Fragment
		for i, fid := range book.FragmentsIDs {
			var f Fragment
			if _, err := unmarshal(fb, fid, &f); err != nil {
				return err
			}
			if f.Text != original[i] {
				return ErrConflict
			}
			fragments = append(fragments, f)
		}

		anyChanges := false
		for i, f := range fragments {
			fragmentUpdated := false
			for j, trans := range translation[i] {
				if j < len(f.VersionsIDs) {
					vid := f.VersionsIDs[j]
					var v TranslationVersion
					if _, err := unmarshal(vb, vid, &v); err != nil {
						return err
					}
					if v.Text == trans.Text {
						continue
					}
					v.Text = trans.Text
					v.Updated = now
					if err := marshal(vb, vid, v); err != nil {
						return err
					}
					anyChanges = true
				} else if noNew {
					break
				} else {
					vid, _ := vb.NextSequence()
					if err := marshal(vb, vid, TranslationVersion{
						ID:      vid,
						Created: now,
						Updated: now,
						Text:    trans.Text,
						Author:  trans.Author,
					}); err != nil {
						return err
					}
					anyChanges = true
					if len(f.VersionsIDs) == 0 {
						book.FragmentsTranslated++
					}
					f.VersionsIDs = append(f.VersionsIDs, vid)
					fragmentUpdated = true
				}
			}
			if len(f.VersionsIDs) > len(translation[i]) && !noDelete {
				f.VersionsIDs = f.VersionsIDs[:len(translation[i])]
				fragmentUpdated = true
				if len(f.VersionsIDs) == 0 {
					book.FragmentsTranslated--
				}
			}
			if !fragmentUpdated {
				continue
			}

			if err := marshal(fb, f.ID, f); err != nil {
				return err
			}
			anyChanges = true
		}

		if !anyChanges {
			return nil
		}

		book.LastActivity = now
		return marshal(b, bid, book)
	})
}

func (db *DB) SearchAndReplace(bid uint64, search, replace string) error {
	now := time.Now()
	return db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("index"))
		var book Book
		if found, err := unmarshal(b, bid, &book); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}
		fb := tx.Bucket([]byte("fragments")).Bucket(encode(bid))
		vb := tx.Bucket([]byte("versions")).Bucket(encode(bid))
		anyChanges := false
		for _, fid := range book.FragmentsIDs {
			var f Fragment
			if _, err := unmarshal(fb, fid, &f); err != nil {
				return err
			}
			for _, vid := range f.VersionsIDs {
				var v TranslationVersion
				found, err := unmarshal(vb, vid, &v)
				if err != nil {
					return err
				}
				if !found {
					continue
				}
				if !strings.Contains(v.Text, search) {
					continue
				}
				v.Text = strings.ReplaceAll(v.Text, search, replace)
				v.Updated = now
				if err := marshal(vb, vid, &v); err != nil {
					return err
				}
				anyChanges = true
			}
		}

		if !anyChanges {
			return nil
		}

		book.LastActivity = now
		return marshal(b, bid, book)
	})
}

func (db *DB) UpdateTerm(bid uint64, term, termOrig, def string) error {
	now := time.Now()
	return db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("index"))
		var book Book
		if found, err := unmarshal(b, bid, &book); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}
		if book.Glossary == nil {
			book.Glossary = map[string]TermDef{}
		}
		if termOrig != "" {
			if _, ok := book.Glossary[termOrig]; ok {
				delete(book.Glossary, termOrig)
			} else {
				delete(book.Glossary, strings.ToLower(termOrig))
			}
		}
		if term != "" && def != "" {
			book.Glossary[term] = TermDef{
				Def: def,
			}
		} else if len(book.Glossary) == 0 {
			book.Glossary = nil
		}

		book.LastActivity = now

		return marshal(b, bid, &book)
	})
}

func (db *DB) MergeGlossary(from, to uint64, replace bool) error {
	now := time.Now()
	return db.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("index"))
		var fbook, tbook Book
		if found, err := unmarshal(b, from, &fbook); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}
		if found, err := unmarshal(b, to, &tbook); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}
		if fbook.Glossary == nil || len(fbook.Glossary) == 0 {
			return nil
		}
		if tbook.Glossary == nil {
			tbook.Glossary = map[string]TermDef{}
		}
		anyChanges := false
		for term, td := range fbook.Glossary {
			d, ok := tbook.Glossary[term]
			if ok && (!replace || d == td) {
				continue
			}
			tbook.Glossary[term] = td
			anyChanges = true
		}

		if !anyChanges {
			return nil
		}

		tbook.LastActivity = now

		return marshal(b, to, &tbook)
	})
}

func (db *DB) FragmentsText(bid uint64, fragmentsIDs []uint64) (Book, []string, error) {
	var book Book
	text := make([]string, 0, len(fragmentsIDs))
	if err := db.View(func(tx *bolt.Tx) error {
		text = text[:0]
		b := tx.Bucket([]byte("index"))
		if found, err := unmarshal(b, bid, &book); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}

		fb := tx.Bucket([]byte("fragments")).Bucket(encode(bid))
		for _, fid := range fragmentsIDs {
			var f Fragment
			if _, err := unmarshal(fb, fid, &f); err != nil {
				return err
			}
			text = append(text, f.Text)
		}
		return nil
	}); err != nil {
		return Book{}, nil, err
	}
	return book, text, nil
}

func (db *DB) FetchVersions(bid, fid uint64) ([]TranslationVersion, error) {
	var versions []TranslationVersion
	if err := db.View(func(tx *bolt.Tx) error {
		versions = versions[:0]
		var f Fragment
		fb := tx.Bucket([]byte("fragments")).Bucket(encode(bid))
		if found, err := unmarshal(fb, fid, &f); err != nil {
			return err
		} else if !found {
			return ErrNotFound
		}
		vb := tx.Bucket([]byte("versions")).Bucket(encode(bid))
		for _, vid := range f.VersionsIDs {
			var v TranslationVersion
			if found, err := unmarshal(vb, vid, &v); err != nil {
				return err
			} else if !found {
				continue
			}
			versions = append(versions, v)
		}
		return nil
	}); err != nil {
		return nil, err
	}
	return versions, nil
}
