define(['jquery', 'errors'], ($, errors) => {
  function addListeners($page) {
    $page.on('click', 'a', (e) => {
      if (e.ctrlKey || e.shiftKey || e.altKey) return;
      load($(e.target).text(), null, $(e.target).attr('morph'));
    });
  }

  const defaultProperties = {
    title: 'Epithets',
    tooltip: 'gufo.me',
    pageClass: 'epithets',
    addListeners,
  };

  function load(text, callback, exact) {
    if (!/[а-яё]/i.test(text)) return;

    const data = { query: text.trim() };
    if (exact) data.exact = 1;
    $.ajax({
      url: '/plugins/epithets',
      method: 'GET',
      data,
    })
      .done((data) => {
        if (callback)
          callback(null, {
            html: data.html,
            ...defaultProperties,
          });
        else $(`.${defaultProperties.pageClass}`).html(data.html);
      })
      .fail((xhr, status, err) => {
        const msg = errors.makeMessage(xhr, status, err);
        if (callback)
          callback(msg, {
            ...defaultProperties,
          });
        else $(`.${defaultProperties.pageClass}`).html(msg);
      });

    return true;
  }

  return { load };
});
