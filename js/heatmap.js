define(['jquery', 'bootstrap'], ($) => {
  'use strict';

  $(document).ready(() => {
    $('.squares .day').each((index, el) => {
      const $el = $(el);
      const parts = $el.attr('title').split(' on ');
      $el.attr(
        'title',
        `<b>${parts[0]}</b> <span class='text-muted text-nowrap'>on ${parts[1]}</span>`
      );
    });
    $('.squares .day').tooltip({
      template:
        '<div class="tooltip progress-tooltip tooltip--black" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>',
      offset: '0,5',
      html: true,
    });
  });
});
// vim: ts=2 sts=2 sw=2 et
