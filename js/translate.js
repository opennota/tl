/* global book_id, fragments_total:true, fragments_translated:true, withTM, tmServer */
define([
  'jquery',
  'js.cookie',
  'bootbox',
  'markdown-it',
  'popper',
  'eventSaver',
  'mt-plugins',
  'bootstrap',
  'jquery.form',
  'jquery.autogrowtextarea',
], ($, Cookies, bootbox, markdownit, Popper, eventSaver, mtPlugins) => {
  'use strict';
  let cancelEdit;
  let cancelEditOrig;
  let $previous;
  let removeHighlight;
  let matchingPercentage = Number(Cookies.get('mp')) || 70;
  let showMatchesWithoutTranslation =
    Cookies.get('wt') === undefined ? true : Cookies.get('wt') == '1';

  function getfvids($el) {
    const $transl = $el.closest('.translation');
    const id = $transl.attr('id');
    if (!/^v[0-9]+$/.test(id)) return;
    const vid = +id.substr(1);
    const fid = +$transl.closest('tr').attr('id').substr(1);
    return [fid, vid];
  }

  function updateProgress(total, translated) {
    if (total != fragments_total || translated != fragments_translated) {
      fragments_total = total;
      fragments_translated = translated;
      const pct = total === 0 ? 0 : Math.floor((100 * translated) / total);
      $('.progress-bar').attr('style', `width:${pct}%`);
      $('.progress__percent').text(pct);
      $('.progress-fraction').attr('title', `${translated}/${total}`);
      $('.header-progress').attr(
        'data-original-title',
        `${pct}% (${fragments_translated} / ${fragments_total})`
      );
    }
    $('.orig-empty-alert').toggle(!total);
  }

  function errmsg(xhr, status, err) {
    if (xhr.readyState === 0) return 'Network error';
    return 'Error: ' + (xhr.responseText || err || 'unknown error');
  }

  function editTransl(e, pasteText) {
    if (removeHighlight) removeHighlight();
    $('#unmark-popper').css('visibility', 'hidden');
    $previous = null;
    if (cancelEdit) cancelEdit();
    const $target = $(e.target);
    const $row = $target.closest('tr');
    const fid = $row.attr('id').substr(1);
    displayGlossaryTerms(fid);
    const $div = $target.closest('div[id^=v]');
    const vid = $div.length ? $div.attr('id').substr(1) : 0;
    const $form = $($('#translate-form-tmpl').html());
    const origText = $row.find('.translator-column-orig .text').text();
    if (/\p{L}/u.test(origText)) {
      if (withTM) lookupFuzzyMatches(fid, origText);
      requestMachineTranslations(fid, origText);
    }
    const origLength = origText.replace(/\n/g, '').length;
    $form.find('.character-counter__orig').text(origLength);
    $form.attr('action', `/book/${book_id}/${fid}/translate`);
    $form.find('[name=version_id]').attr('value', vid);
    const $submit = $form.find(':submit');
    $submit.text(vid ? 'Save' : 'Add');
    const $textarea = $form.find('textarea');
    let text = pasteText;
    if (!text) text = $div.find('.text').text();
    $textarea.text(text);
    $textarea
      .on('keyup change blur click', () => {
        $form
          .find('.character-counter__transl')
          .text($textarea.val().replace(/\n/g, '').length);
      })
      .on('keydown', (e) => {
        if (
          e.which === 45 /* Insert */ &&
          (e.altKey || e.ctrlKey) &&
          !e.shiftKey
        )
          $textarea.val(origText);
        else if (
          e.ctrlKey &&
          !e.altKey &&
          !e.shiftKey &&
          e.which >= 48 &&
          e.which <= 57
        ) {
          e.preventDefault();
          if (e.which == 48 /* 0 */) $textarea.val(origText);
          else {
            const index = e.which - 49 + 1;
            const $transl = $(
              `.machine-translation:nth-child(${index}) .machine-translation__text`
            );
            if ($transl.length) $textarea.val($transl.text());
          }
        }
      })
      .keyup();
    let $next = null;
    $form.ajaxForm({
      dataType: 'json',
      beforeSerialize: () => {
        let text = $textarea.val();
        if (!/[а-яё]/i.test(text)) return;
        text = text.replace(/(^|\s)- /g, '$1— ');
        text = text.replace(/(\s)-$/, '$1—');
        text = text.replace(/(^|[-\s([])"(\S)/g, '$1«$2');
        text = text.replace(/(\S)"([-\s.,!?:;…)\]]|$)/g, '$1»$2');
        text = text.replace(/\.\.\./g, '…');
        $textarea.val(text);
      },
      beforeSubmit: () => {
        $submit.attr('disabled', true);
        $form.find('.alert-container').text('');
      },
      success: (data) => {
        cancelEdit = null;
        const $html = $($('#version-tmpl').html());
        $html.attr('id', `v${data.id}`);
        $html.find('.text').html(data.text);
        updateProgress(fragments_total, data.fragments_translated);
        $form.replaceWith($html);
        $previous = $html;
        if ($next) {
          $next.click();
        }
      },
      error: (xhr, status, err) => {
        $submit.attr('disabled', false);
        const $alert = $($('#alert-tmpl').html());
        $alert.append(errmsg(xhr, status, err));
        $form.find('.alert-container').html($alert);
      },
    });
    cancelEdit = () => {
      $('#machine-translations, #fuzzy-matches').text('');
      cancelEdit = null;
      $form.replaceWith($div);
      $previous = $div;
    };
    $textarea.on('keydown', (e) => {
      if (e.ctrlKey && e.which == 13) {
        e.stopPropagation();
        $next = null;
        if (!e.shiftKey) {
          const $nextRow = $row.next().next();
          const $nextDiv = $nextRow.find('div[id^=v]:visible').first();
          if ($nextDiv.length) {
            $next = $nextDiv.find('.action-edit-transl');
          } else {
            $next = $nextRow.find('.action-translate');
          }
        }
        if (!vid && $textarea.val() === '') {
          cancelEdit();
          if ($next) {
            $next.click();
          }
        } else if ($textarea.val() != text) {
          $form.find(':submit').click();
        } else if ($next && $next.length) {
          $next.click();
        } else {
          cancelEdit();
        }
      }
    });
    $form.on('click', '.cancel', cancelEdit);
    if (vid) {
      $div.replaceWith($form);
    } else {
      $row.find('.translator-column-transl').append($form);
    }
    $textarea.autoGrow().focus();
  }

  function closeCommentary(e) {
    const $target = $(e.target);
    const $commentaryRow = $target.closest('tr');
    $commentaryRow.removeClass('shown').find('td').html('');
    const $comment = $commentaryRow.prev().find('.button-comment');
    $comment.removeClass('fa-times-circle');
    if ($comment.data('comment')) {
      $comment.addClass('fa-comment');
    } else {
      $comment.addClass('fa-comment-o');
    }
  }

  function comment(e) {
    const $target = $(e.target);
    const $row = $target.closest('tr');
    const $commentaryRow = $row.next();
    $commentaryRow.toggleClass('shown');
    if (!$commentaryRow.hasClass('shown')) {
      $target.removeClass('fa-times-circle');
      if ($target.data('comment')) {
        $target.addClass('fa-comment');
      } else {
        $target.addClass('fa-comment-o');
      }
      return;
    }
    $target.removeClass('fa-comment fa-comment-o').addClass('fa-times-circle');
    const fid = $row.attr('id').substr(1);
    const $form = $($('#commentary-form-tmpl').html());
    $form.attr('action', `/book/${book_id}/${fid}/comment`);
    const $submit = $form.find(':submit');
    const $edit = $form.find('.commentary-form__button-edit');
    const $div = $form.find('.text');
    const render = (text) => {
      const md = markdownit({
        linkify: true,
        typographer: true,
        quotes: '«»„“',
      });
      $div.html(md.render(String(text)));
      $div.on('dblclick', editCommentary);
      $submit.hide();
      $edit.show();
    };
    const editCommentary = () => {
      $div.off('dblclick');
      $edit.hide();
      const $textarea = $(
        '<textarea class="full-width-textarea" name="text" spellcheck="false">'
      );
      $div.html('').append($textarea);
      $textarea.text($target.data('comment'));
      $submit.attr('disabled', false).show();
      $textarea.on('keydown', (e) => {
        if (e.ctrlKey && e.which == 13) {
          e.stopPropagation();
          $submit.click();
        }
      });
      // Delay autoGrow until the textarea's width property is initialized (Google Chrome).
      setTimeout(() => $textarea.autoGrow().focus(), 1);
    };
    $edit.on('click', editCommentary);
    $form.ajaxForm({
      dataType: 'json',
      beforeSubmit: () => {
        $submit.attr('disabled', true);
        $form.find('.alert-container').text('');
      },
      success: (data) => {
        $target.data('comment', data.text);
        render(data.text);
      },
      error: (xhr, status, err) => {
        $submit.attr('disabled', false);
        const $alert = $($('#alert-tmpl').html());
        $alert.append(errmsg(xhr, status, err));
        $form.find('.alert-container').html($alert);
      },
    });
    const text = $target.data('comment');
    if (text) {
      render(text);
    } else {
      editCommentary();
    }
    $commentaryRow.find('td').html('').append($form);
  }

  function handleError(xhr, status, err) {
    if (xhr.readyState === 0) bootbox.alert('Network error');
    else bootbox.alert('Error: ' + (err || 'unknown error'));
  }

  function removeTransl(e) {
    const $div = $(e.target).closest('div[id^=v]');
    const vid = $div.attr('id').substr(1);
    const fid = $div.closest('tr').attr('id').substr(1);
    const text = $div.find('p.text').html();
    const dlg = bootbox.confirm({
      message:
        '<b>Remove the following version?</b><br><br><blockquote class="bq--limited-height">' +
        text +
        '</blockquote>',
      buttons: {
        confirm: {
          label: 'Remove',
          className: 'btn-danger button-confirm-remove-transl',
        },
      },
      callback: (result) => {
        if (!result) return;
        $.ajax({
          method: 'DELETE',
          url: `/book/${book_id}/${fid}/${vid}`,
        })
          .done((data) => {
            dlg.modal('hide');
            $div.remove();
            updateProgress(fragments_total, data.fragments_translated);
          })
          .fail(handleError);
      },
    });
  }

  function star(e) {
    const $icon = $(e.target);
    const fid = $icon.closest('tr').attr('id').substr(1);
    $.ajax({
      method: 'POST',
      url: `/book/${book_id}/${fid}/star`,
    })
      .done(() => {
        $icon
          .removeClass('action-star fa-star-o')
          .addClass('action-unstar fa-star');
      })
      .fail(handleError);
  }

  function unstar(e) {
    const $icon = $(e.target);
    const fid = $icon.closest('tr').attr('id').substr(1);
    $.ajax({
      method: 'DELETE',
      url: `/book/${book_id}/${fid}/star`,
    })
      .done(() => {
        $icon
          .removeClass('action-unstar fa-star')
          .addClass('action-star fa-star-o');
      })
      .fail(handleError);
  }

  function editOrig(e) {
    if (removeHighlight) removeHighlight();
    if (cancelEditOrig) cancelEditOrig();
    const $row = $(e.target).closest('tr');
    const fid = $row.attr('id').substr(1);
    const $div = $row.find('.translator-column-orig > div');
    const $form = $($('#edit-orig-form-tmpl').html());
    $form.attr('action', `/book/${book_id}/${fid}`);
    const $submit = $form.find(':submit');
    const $textarea = $form.find('textarea');
    $textarea.text($div.find('.text').text());
    $form.ajaxForm({
      dataType: 'json',
      beforeSubmit: () => $submit.attr('disabled', true),
      success: (data) => {
        cancelEditOrig = null;
        const $html = $($('#orig-tmpl').html());
        $html.find('.text').html(data.text);
        $form.replaceWith($html);
      },
      error: (xhr, status, err) => {
        $submit.attr('disabled', false);
        const $alert = $($('#alert-tmpl').html());
        $alert.append(errmsg(xhr, status, err));
        $form.find('.alert-container').html($alert);
      },
    });
    cancelEditOrig = () => {
      cancelEditOrig = null;
      $form.replaceWith($div);
    };
    $textarea.on('keydown', (e) => {
      if (e.ctrlKey && e.which == 13) {
        e.stopPropagation();
        $submit.click();
      }
    });
    $form.on('click', '.cancel', cancelEditOrig);
    $div.replaceWith($form);
    $textarea.autoGrow().focus();
  }

  function removeOrig(e) {
    const $row = $(e.target).closest('tr');
    const fid = $row.attr('id').substr(1);
    const text = $row.find('.translator-column-orig .text').html();
    const dlg = bootbox.confirm({
      message:
        '<b>Remove the following fragment?</b><br><br><blockquote class="bq--limited-height">' +
        text +
        '</blockquote>',
      buttons: {
        confirm: {
          label: 'Remove',
          className: 'btn-danger button-confirm-remove-orig',
        },
      },
      callback: (result) => {
        if (!result) return;
        $.ajax({
          method: 'DELETE',
          url: `/book/${book_id}/${fid}`,
        })
          .done((data) => {
            dlg.modal('hide');
            $row.next().remove(); // remove commentary
            $row.remove();
            updateProgress(fragments_total - 1, data.fragments_translated);
            const num_filtered = +$('.button-filter sup').text();
            if (num_filtered) {
              $('.button-filter sup').text(num_filtered - 1);
            }
          })
          .fail(handleError);
      },
    });
  }

  function addOrig(e) {
    if (removeHighlight) removeHighlight();
    if (cancelEditOrig) cancelEditOrig();
    const $newRow = $($('#new-row-tmpl').html());
    const $textarea = $newRow.find('textarea');
    cancelEditOrig = () => {
      cancelEditOrig = null;
      $newRow.remove();
    };
    $newRow
      .on('click', '.cancel', cancelEditOrig)
      .on('click', '.action-orig-up', () =>
        $newRow.prev().prev().before($newRow)
      )
      .on('click', '.action-orig-down', () =>
        $newRow.next().next().after($newRow)
      );
    const $form = $newRow.find('form');
    $form.attr('action', `/book/${book_id}/fragments`);
    const $submit = $form.find(':submit');
    $form.ajaxForm({
      dataType: 'json',
      beforeSerialize: ($form) => {
        const prev_id = $newRow.prev().prev().attr('id');
        const after = prev_id ? prev_id.substr(1) : '';
        $form.find('input[name=after]').attr('value', after);
        const next_id = $newRow.next().attr('id');
        const before = next_id ? next_id.substr(1) : '';
        $form.find('input[name=before]').attr('value', before);
      },
      beforeSubmit: () => $submit.attr('disabled', true),
      success: (data) => {
        cancelEditOrig = null;
        $newRow
          .find('.translator-column-star .insert-icon-here')
          .html(
            '<i class="fa fa-star-o action-star icon--sheer icon--clickable p-1"></i>'
          );
        $newRow
          .find('.translator-column-middle .insert-icon-here')
          .html(
            '<i class="fa fa-arrow-right action-translate icon--sheer icon--clickable p-1"></i>'
          );
        $newRow
          .find('.translator-column-comment .insert-icon-here')
          .html(
            '<i class="fa fa-comment-o button-comment action-comment icon--sheer icon--clickable p-1"></i>'
          );
        $newRow
          .find('.insert-icon-here')
          .each((_, el) => $(el).removeClass('insert-icon-here'));
        let $html = $($('#orig-tmpl').html());
        $html.find('.text').html(data.text);
        $html = $html.add(
          `<a class="translator__fragment-permalink link--gray" href="/book/${book_id}/${data.id}">` +
            `#${data.seq_num}</a>`
        );
        $newRow.find('.translator-column-orig > form').replaceWith($html);
        $newRow.removeClass('editing').attr('id', `f${data.id}`);
        $newRow.after(
          '<tr class="commentary"><td class="commentary-cell" colspan="5"></td></tr>'
        );
        updateProgress(fragments_total + 1, fragments_translated);
      },
      error: (xhr, status, err) => {
        $submit.attr('disabled', false);
        const $alert = $($('#alert-tmpl').html());
        $alert.append(errmsg(xhr, status, err));
        $form.find('.alert-container').html($alert);
      },
    });
    if (e) {
      $(e.target).closest('tr').next().after($newRow);
    } else {
      $('.translator > tbody').append($newRow);
    }
    $textarea
      .on('keydown', (e) => {
        if (e.ctrlKey && e.which == 13) {
          e.stopPropagation();
          $submit.click();
        }
      })
      .autoGrow()
      .focus();
  }

  function toggleOrigToolbox(e) {
    if (removeHighlight) removeHighlight();
    const $target = $(e.target);
    const oldOffset = $target.offset();
    const oldScrollX = window.scrollX;
    const oldScrollY = window.scrollY;
    $('.translator').toggleClass('show-orig-toolbox');
    Cookies.set(
      'show-orig-toolbox',
      $('.translator').hasClass('show-orig-toolbox') ? 1 : 0
    );
    setTimeout(
      () =>
        window.scrollTo(
          oldScrollX,
          oldScrollY - oldOffset.top + $target.offset().top
        ),
      1
    );
  }

  const emptyRect = {
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    width: 0,
    height: 0,
    x: 0,
    y: 0,
  };
  Object.freeze(emptyRect);

  function backwardSelection() {
    const sel = document.getSelection();
    const pos = sel.anchorNode.compareDocumentPosition(sel.focusNode);
    return (
      (!pos && sel.anchorOffset > sel.focusOffset) ||
      pos == Node.DOCUMENT_POSITION_PRECEDING ||
      ((pos ==
        (Node.DOCUMENT_POSITION_PRECEDING | Node.DOCUMENT_POSITION_CONTAINS) ||
        pos ==
          (Node.DOCUMENT_POSITION_FOLLOWING |
            Node.DOCUMENT_POSITION_CONTAINED_BY)) &&
        sel.anchorNode === sel.getRangeAt(0).endContainer)
    );
  }

  class RangeRef {
    constructor(el) {
      this.el = el;
    }
    getBoundingClientRect() {
      const sel = document.getSelection();
      if (!sel || !sel.rangeCount) return emptyRect;
      const { x, width, left, right } = this.el.getBoundingClientRect();
      let { y, height, top, bottom } = sel
        .getRangeAt(0)
        .getBoundingClientRect();
      if (backwardSelection()) {
        bottom = top;
      } else {
        top = bottom;
      }
      return { top, right, bottom, left, width, height, x, y };
    }
    get clientWidth() {
      return this.el.clientWidth;
    }
    get clientHeight() {
      return this.el.clientHeight;
    }
  }

  let markPopper, unmarkPopper, termPopper;

  function selectionChanged() {
    const $node = $('#right-popper');
    const sel = document.getSelection();
    if (!sel || !sel.rangeCount) return $node.css('visibility', 'hidden');
    const length = sel.toString().trim().length;
    if (!length) return $node.css('visibility', 'hidden');
    const $anchor = $(sel.anchorNode);
    const $v = $anchor.closest('div[id^=v]');
    const sidebarVisible = $('#sidebar-left').is(':visible');
    if (!$v.length && !sidebarVisible) return $node.css('visibility', 'hidden');
    const $focus = $(sel.focusNode);
    const $v2 = $focus.closest('div[id^=v]');
    const oneVersion = $v.length && $v2.get(0) === $v.get(0);
    if (!oneVersion && ($v.length || $v2.length))
      return $node.css('visibility', 'hidden');
    $node.find('.button-mark').toggle(!!oneVersion);
    $node.find('.button-lookup-selection').toggle(sidebarVisible);
    const $tr = $anchor.closest('tr[id^=f]');
    if (!$tr.length) return $node.css('visibility', 'hidden');
    const tr = $tr.get(0);
    if (tr !== $focus.closest('tr[id^=f]').get(0))
      return $node.css('visibility', 'hidden');
    if (!markPopper) {
      markPopper = new Popper(new RangeRef(tr), $node.get(0), {
        placement: 'right',
        modifiers: { offset: { offset: '0,5' } },
      });
    } else if (markPopper.reference.el !== tr) {
      markPopper.reference.el = tr;
    }
    $node.find('.button-add-term').toggle(!$v.length);
    markPopper.options.placement = $v.length ? 'right' : 'left';
    markPopper.scheduleUpdate();
    $node.css('visibility', 'visible');
  }

  function toggleUnmarkPopper(e) {
    const $node = $('#unmark-popper');
    let oldEl;
    if (!unmarkPopper) {
      unmarkPopper = new Popper(e.target, $node.get(0), {
        placement: 'top',
        modifiers: { preventOverflow: { boundariesElement: window } },
      });
    } else {
      oldEl = unmarkPopper.reference;
      if (unmarkPopper.reference !== e.target) {
        unmarkPopper.reference = e.target;
        unmarkPopper.scheduleUpdate();
      }
    }
    if ($node.css('visibility') == 'hidden' || oldEl !== e.target) {
      $node.css('visibility', 'visible');
    } else {
      $node.css('visibility', 'hidden');
    }
  }

  function toggleTermPopper(e) {
    const $target = $(e.target);
    const def = $target.attr('title');
    const $input = $('#glossary-term-popper-input');
    $input.val(def);
    $input.data('term', $target.text());
    const $node = $('#glossary-term-popper');
    let oldEl;
    if (!termPopper) {
      termPopper = new Popper(e.target, $node.get(0), {
        placement: 'top',
        modifiers: { preventOverflow: { boundariesElement: window } },
      });
    } else {
      oldEl = termPopper.reference;
      if (termPopper.reference !== e.target) {
        termPopper.reference = e.target;
        termPopper.scheduleUpdate();
      }
    }
    if ($node.css('visibility') == 'hidden' || oldEl !== e.target) {
      $node.css('visibility', 'visible');
      setTimeout(() => $input.focus(), 100);
    } else {
      $node.css('visibility', 'hidden');
      $input.blur();
    }
  }

  function addMarkFromSelection() {
    const sel = document.getSelection();
    if (!sel || !sel.rangeCount) return;
    const mark = sel.toString().trim();
    if (!mark.length) return;
    const $p = $(sel.anchorNode).closest('p.text');
    if (!$p.length) return;
    const [fid, vid] = getfvids($p);
    $.ajax({
      method: 'POST',
      url: `/book/${book_id}/${fid}/${vid}/mark`,
      data: { mark },
    })
      .done((data) => {
        $p.html(data.text);
      })
      .fail(handleError);
  }

  function getFragmentsIDs() {
    return Array.from(document.querySelectorAll('.translator tr[id^=f]')).map(
      (el) => {
        return parseInt(el.getAttribute('id').substr(1));
      }
    );
  }

  function reloadFragments(ids, text) {
    for (let i = 0; i < ids.length; i++) {
      document.querySelector(`tr[id=f${ids[i]}] .text`).innerHTML = text[i];
    }
  }

  function addGlossaryTermOnServer() {
    const $inputTerm = $('#panel-glossary__input-term');
    const $inputDef = $('#panel-glossary__input-def');
    const term = $inputTerm.val().trim();
    const def = $inputDef.val().trim();
    if (term === '' || def === '') return;
    const $btn = $('#panel-glossary__btn-add-term');
    $btn.prop('disabled', 'true');
    const fragmentsToReload = getFragmentsIDs();
    $.ajax({
      method: 'POST',
      url: `/book/${book_id}/glossary`,
      data: {
        term,
        def,
        fragmentsToReload: JSON.stringify(fragmentsToReload),
      },
    })
      .done((text) => {
        $inputTerm.val('');
        $inputDef.val('');
        $btn.prop('disabled', '');
        document.activeElement.blur();
        reloadFragments(fragmentsToReload, text);
        let updated = false;
        $('#panel-glossary-content .glossary-term').each((_, el) => {
          const $el = $(el);
          if ($el.text() === term) {
            $el.next().text(def);
            updated = true;
          }
        });
        if (!updated) {
          let $dl = $('#panel-glossary-content > dl');
          if (!$dl.length) {
            $dl = $('<dl></dl>');
            $('#panel-glossary-content').html($dl);
          }
          $dl.append(makeGlossaryTerm(term, def));
        }
      })
      .fail((xhr, status, err) => {
        $btn.prop('disabled', '');
        handleError(xhr, status, err);
      });
  }

  function addGlossaryTerm(e) {
    e.preventDefault();
    e.stopPropagation();
    const sel = document.getSelection();
    const $term = $('#panel-glossary__input-term');
    let term = sel && sel.rangeCount ? sel.toString().trim() : '';
    $term.focus().get(0).scrollIntoView();
    if (!term.length) return;
    $('#panel-glossary__input-def').val('');
    $term.val(term);
  }

  function updateTermDef(e) {
    const $target = $(e.target);
    let term = $target.data('term');
    const termOrig = term;
    const def = $target.val().trim();
    if (def === '') term = '';
    const fragmentsToReload = getFragmentsIDs();
    $.ajax({
      method: 'POST',
      url: `/book/${book_id}/glossary`,
      data: {
        term,
        def,
        termOrig,
        fragmentsToReload: JSON.stringify(fragmentsToReload),
      },
    })
      .done((text) => {
        $('#glossary-term-popper').css('visibility', 'hidden');
        document.activeElement.blur();
        reloadFragments(fragmentsToReload, text);
      })
      .fail(handleError);
  }

  function removeMark() {
    const $el = $(unmarkPopper.reference);
    const mark = $el.text().trim();
    const $p = $el.closest('p.text');
    if (!$p.length) return;
    const [fid, vid] = getfvids($p);
    $.ajax({
      method: 'DELETE',
      url: `/book/${book_id}/${fid}/${vid}/mark`,
      data: { mark },
    })
      .done((data) => {
        $p.html(data.text);
        $('#unmark-popper').css('visibility', 'hidden');
      })
      .fail(handleError);
  }

  function showStats(e) {
    e.preventDefault();
    const dlg = bootbox.dialog({
      message:
        '<div style="text-align: center;"><i class="fa fa-spin fa-spinner"></i> Loading...</div>',
      onEscape: true,
      backdrop: true,
    });
    dlg.init(() => {
      $.ajax({
        method: 'GET',
        url: `/book/${book_id}/stats`,
      })
        .done((stats) => {
          const $html = $($('#stats-tmpl').html());

          $html
            .find('.stats__orig-words')
            .text(stats.orig_words.toLocaleString());
          $html
            .find('.stats__orig-chars')
            .text(stats.orig_chars.toLocaleString());

          $html
            .find('.stats__trans-words')
            .text(stats.trans_words.toLocaleString());
          const pctWords = (100 * stats.trans_words) / stats.orig_words;
          $html.find('.stats__trans-words-pct').text(`${pctWords.toFixed(2)}%`);

          $html
            .find('.stats__trans-chars')
            .text(stats.trans_chars.toLocaleString());
          const pctChars = (100 * stats.trans_chars) / stats.orig_chars;
          $html.find('.stats__trans-chars-pct').text(`${pctChars.toFixed(2)}%`);

          dlg.find('.bootbox-body').html($html);
        })
        .fail(handleError);
    });
  }

  function showSearchAndReplaceDialog(e) {
    e.preventDefault();
    const dlg = bootbox.dialog({
      title: 'Search and replace',
      message: $('#search-and-replace-tmpl').html(),
      onEscape: true,
      backdrop: true,
    });
    dlg.on('shown.bs.modal', () =>
      $('.search-and-replace-form__input-search').focus()
    );
  }

  function showExportNBForm(e) {
    e.preventDefault();
    const dlg = bootbox.dialog({
      title: 'Export to notabenoid',
      message: $('#export-nb-tmpl').html(),
      onEscape: true,
      backdrop: true,
    });
    dlg.on('shown.bs.modal', () => $('.export-to-nb-form__input-url').focus());
    dlg.init(() => {
      dlg.find('.export-to-nb-form__button-export').on('click', (e) => {
        const $alert = dlg.find('.alert');
        $alert.hide();
        const $btn = $(e.target);
        $btn.prop('disabled', 'true');
        $btn.append('<i class="fa fa-spin fa-spinner"></i>');
        const data = {
          chapterUrl: $('#chapter-url').val(),
          username: $('#nb-user').val(),
          password: $('#nb-pass').val(),
        };
        if ($('.export-to-nb-form__check-no-delete').prop('checked'))
          data.noDelete = 1;
        if ($('.export-to-nb-form__check-no-add-new').prop('checked'))
          data.noNew = 1;
        if ($('.export-to-nb-form__check-export-comments').prop('checked'))
          data.exportComments = 1;
        $.ajax({
          method: 'POST',
          url: `/book/${book_id}/notabenoid/export`,
          data,
        })
          .done((data) => {
            const $html = $('<div>DONE: <a></a></div>');
            const $a = $html.find('a');
            $a.attr('href', data.url).text(data.url);
            dlg.find('.bootbox-body').html($html);
          })
          .fail((xhr, status, err) => {
            $btn.prop('disabled', '').find('.fa-spin').remove();
            $alert.text(errmsg(xhr, status, err)).show();
          });
      });
    });
  }

  function showUpdateNBForm(e) {
    e.preventDefault();
    const dlg = bootbox.dialog({
      title: 'Update from notabenoid',
      message: $('#update-nb-tmpl').html(),
      onEscape: true,
      backdrop: true,
    });
    dlg.on('shown.bs.modal', () =>
      $('.update-from-nb-form__input-url').focus()
    );
    dlg.init(() => {
      dlg.find('.update-from-nb-form__button-update').on('click', (e) => {
        const $alert = dlg.find('.alert');
        $alert.hide();
        const $btn = $(e.target);
        $btn.prop('disabled', 'true');
        $btn.append('<i class="fa fa-spin fa-spinner"></i>');
        const data = {
          chapterUrl: $('.update-from-nb-form__input-url').val(),
          username: $('.update-from-nb-form__input-user').val(),
          password: $('.update-from-nb-form__input-password').val(),
        };
        if ($('.update-from-nb-form__check-no-delete').prop('checked'))
          data.noDelete = 1;
        if ($('.update-from-nb-form__check-no-add-new').prop('checked'))
          data.noNew = 1;
        $.ajax({
          method: 'POST',
          url: `/book/${book_id}/notabenoid/update`,
          data,
        })
          .done(() => location.reload())
          .fail((xhr, status, err) => {
            $btn.prop('disabled', '').find('.fa-spin').remove();
            $alert.text(errmsg(xhr, status, err)).show();
          });
      });
    });
  }

  function showCompareNBForm(e) {
    e.preventDefault();
    const dlg = bootbox.dialog({
      title: 'Compare to notabenoid',
      message: $('#compare-nb-tmpl').html(),
      onEscape: true,
      backdrop: true,
    });
    dlg.on('shown.bs.modal', () => $('.compare-to-nb-form__input-url').focus());
    dlg.init(() => {
      dlg.find('.compare-to-nb-form__button-compare').on('click', (e) => {
        const $alert = dlg.find('.alert');
        $alert.hide();
        const $btn = $(e.target);
        $btn.prop('disabled', 'true');
        $btn.append('<i class="fa fa-spin fa-spinner"></i>');
        const data = {
          chapterUrl: $('.compare-to-nb-form__input-url').val(),
          username: $('.compare-to-nb-form__input-user').val(),
          password: $('.compare-to-nb-form__input-password').val(),
        };
        if ($('input[name="dorder"]:checked').val() === 'newer')
          data.reverse = 1;
        $.ajax({
          method: 'POST',
          url: `/book/${book_id}/notabenoid/compare`,
          data,
        })
          .done((data, textStatus, xhr) => {
            const dlgbody = dlg.find('.bootbox-body');
            if (xhr.status == 204) {
              dlgbody.html('There are no differences.');
            } else {
              const blob = new Blob([data], { type: 'text/html' });
              const url = URL.createObjectURL(blob);
              dlgbody.html(
                `There are differences. <a class="compare-to-nb-form__link-differences" href="${url}">View.</a>`
              );
            }
          })
          .fail((xhr, status, err) => {
            $btn.prop('disabled', '').find('.fa-spin').remove();
            $alert.text(errmsg(xhr, status, err)).show();
          });
      });
    });
  }

  function requestMachineTranslations(fid, origText) {
    const $translations = $('#machine-translations');
    $translations.text('');
    if (!/\p{L}/u.test(origText)) return;
    for (let plugin of mtPlugins) {
      const p = plugin;
      $.ajax({
        method: 'GET',
        url: `/mt/${plugin}?q=` + encodeURIComponent(origText),
      }).done((data) => {
        if (data.error) return;
        if ($(`#f${fid} .translator-column-transl textarea`).length) {
          const number = $translations.children().length + 1;
          const $transl = $(
            `<div class="machine-translation mb-3"><div class="mt-heading"><span class="mt-number" title="Ctrl-${number} to insert">${number}</span><span class="mt-engine">${p}</span></div><div class="machine-translation__text"></div></div>`
          );
          $transl.find('.machine-translation__text').text(data.translation);
          $translations.append($transl);
        }
      });
    }
  }

  function lookupFuzzyMatches(fid, origText) {
    const $matches = $('#fuzzy-matches');
    $matches.html('<i class="fa fa-spinner fa-spin"></i>');
    if (!/\p{L}/u.test(origText)) return;
    $.ajax({
      method: 'GET',
      url: tmServer + '/fuzzy',
      data: {
        s: matchingPercentage,
        t: showMatchesWithoutTranslation ? 0 : 1,
        b: book_id,
        f: fid,
        q: origText,
      },
    })
      .done((matches) => {
        if (!$(`#f${fid} .translator-column-transl textarea`).length) return;
        $matches.text('');
        for (let m of matches) {
          if (m.fragment_id == fid && m.book_id === book_id) continue;
          const $match = $(
            `<div class="fuzzy-match mb-3"><p><span class="fuzzy-match__percent"></span> <span class="fuzzy-match__text"></span> <a class="fuzzy-match__fragment-permalink"></a></p><div class="fuzzy-match-translations"></div></div>`
          );
          const h = m.similarity == 100 ? 170 : 120;
          const s = Math.min(90, 150 - m.similarity);
          const color = s > 60 ? '#333' : '#fff';
          $match
            .find('.fuzzy-match__percent')
            .text(`${m.similarity}%`)
            .attr(
              'style',
              `background-color: hsl(${h}, 50%, ${s}%); color: ${color}`
            );
          const $text = $match.find('.fuzzy-match__text');
          if (m.html) $text.html(m.html);
          else $text.text(m.text);
          if (m.book_id && m.fragment_id)
            $match
              .find('a')
              .text(`#${m.fragment_id}`)
              .attr('href', `/book/${m.book_id}/${m.fragment_id}`);
          if (m.translations && m.translations.length) {
            const $ts = $match.find('.fuzzy-match-translations');
            for (let t of m.translations) {
              const $t = $('<p class="fuzzy-match__translation"></p>');
              $t.text(t);
              $ts.append($t);
            }
          } else if (m.translation) {
            const $ts = $match.find('.fuzzy-match-translations');
            const $t = $('<p class="fuzzy-match__translation"></p>');
            $t.text(m.translation);
            $ts.append($t);
          }
          $matches.append($match);
        }
      })
      .fail(() => {
        $matches.text('');
      });
  }

  function jumpUntranslated() {
    const $editing = $('.translator-column-transl textarea');
    if ($editing.length) {
      $editing.first().focus();
      return;
    }
    const rows = document.querySelectorAll('.translator-row');
    const el = Array.prototype.find.call(
      rows,
      (r) => !r.querySelector('.translator-column-transl > *')
    );
    if (el) {
      el.scrollIntoView();
      $(el).closest('tr').find('.action-translate').click();
    }
  }

  function makeGlossaryTerm(term, def) {
    const $dt = $('<dt class="glossary-term --clickable"></dt>');
    $dt.text(term);
    const $dd = $('<dd></dd>');
    $dd.text(def);
    return $('<div></div>').append($dt, $dd).html();
  }

  function displayGlossaryTerms(fid) {
    const $glossary = $('#panel-glossary-content');
    $glossary.text('');
    const $dl = $('<dl></dl>');
    const seen = {};
    $(`#f${fid} .translator-column-orig .text .glossary-term`).each((_, el) => {
      const $el = $(el);
      const text = $el.text();
      if (seen[text]) return;
      seen[text] = 1;
      $dl.append(makeGlossaryTerm($el.text(), $el.attr('title')));
    });
    $glossary.append($dl);
  }

  function applyFuzzySettings() {
    $('.action-fuzzy-settings').tooltip('hide');
    matchingPercentage = Number($('[name="matching-percentage"]').val()) || 70;
    showMatchesWithoutTranslation = $(
      '[name="show-matches-w/o-translation"]'
    ).prop('checked');
    Cookies.set('mp', matchingPercentage);
    Cookies.set('wt', showMatchesWithoutTranslation ? 1 : 0);
    const $ta = $('.translator-column-transl textarea');
    if ($ta.length) {
      const $row = $ta.closest('tr');
      const fid = +$row.attr('id').substr(1);
      lookupFuzzyMatches(
        fid,
        $row.find('.translator-column-orig .text').text()
      );
    }
  }

  function nextDictionaryTab() {
    const $pills = $('#panel-dictionary-pills > li');
    if ($pills.length < 2) return;
    const $active = $pills.find('.active').parent();
    const $next = $active.next();
    if ($next.length) $next.click();
    else $pills.first().click();
  }

  $(document).ready(() => {
    $.ajax({
      method: 'GET',
      url: `/book/${book_id}/neighbors?q=${encodeURIComponent(
        location.search.substr(1)
      )}`,
    })
      .done((data) => {
        if (data.prev) {
          const $prev = $('.prev-book');
          $prev.attr('href', data.prev.url);
          $prev.attr('title', data.prev.title);
        }
        if (data.next) {
          const $next = $('.next-book');
          $next.attr('href', data.next.url);
          $next.attr('title', data.next.title);
        }
      })
      .fail(() => {});
    $('.translator')
      .on('click', '.action-translate, .action-edit-transl', editTransl)
      .on('click', '.action-remove-transl', removeTransl)
      .on('click', '.action-comment', comment)
      .on('click', '.commentary-form__button-close', closeCommentary)
      .on('click', '.action-star', star)
      .on('click', '.action-unstar', unstar)
      .on('click', '.action-toggle-toolbox', toggleOrigToolbox)
      .on('click', '.action-remove-orig', removeOrig)
      .on('click', '.action-edit-orig', editOrig)
      .on('click', '.action-add-orig', addOrig)
      .on('click', '.marked', toggleUnmarkPopper)
      .on('click', '.glossary-term', toggleTermPopper)
      .on('pasteTextReady', '.action-translate', (e, text) => {
        editTransl(e, text);
      });
    $('#glossary-term-popper-input').on('keydown', (e) => {
      if (e.which == 13) updateTermDef(e);
    });
    $('.action-mark').on('click', addMarkFromSelection);
    $('.action-unmark').on('click', removeMark);
    $('.action-add-term').on('click', addGlossaryTerm);
    $('.action-show-stats').on('click', showStats);
    $('.action-search-and-replace').on('click', showSearchAndReplaceDialog);
    $('.action-compare-nb').on('click', showCompareNBForm);
    $('.action-export-nb').on('click', showExportNBForm);
    $('.action-update-nb').on('click', showUpdateNBForm);
    let pageTooltip;
    $('.pagination__jump-to-page')
      .tooltip({
        placement: 'bottom',
        trigger: 'click',
        html: true,
        sanitize: false,
        title: `<form action="/book/${book_id}"><input type="text" size="3" name="page"></input></form>`,
      })
      .on('show.bs.tooltip', () => {
        if (pageTooltip) pageTooltip.tooltip('hide');
      })
      .on('shown.bs.tooltip', (e) => {
        $('#' + $(e.target).attr('aria-describedby'))
          .find('input')
          .focus();
        pageTooltip = $(e.target);
      });
    $(document)
      .on('keydown', (e) => {
        if (e.ctrlKey && e.which == 13) {
          if ($previous) {
            $previous.find('.action-edit-transl').click();
          } else {
            $('.translation:first-child .action-edit-transl').first().click();
          }
        } else if (e.altKey && !e.ctrlKey && !e.shiftKey) {
          if (e.which == 77 /* Alt-M */) addMarkFromSelection();
          else if (e.which == 71 /* Alt-G */) addGlossaryTerm(e);
          else if (e.which == 88 /* Alt-X */)
            $('.clear-filter-link')
              .get(0) // jQuery doesn't click A elements without handlers.
              .click();
          else if (e.which == 67 /* Alt-C */) jumpUntranslated();
          else if (e.which == 78 /* Alt-N */) nextDictionaryTab();
        }
      })
      .on('selectionchange', selectionChanged);
    $('.action-add-first-fragment').on('click', (e) => {
      e.preventDefault();
      addOrig();
    });
    updateProgress(fragments_total, fragments_translated);
    $('.filter-dropdown')
      .on('click', 'label', (e) => {
        const radios = $(e.target)
          .closest('.dropdown-item')
          .find('[type="radio"]');
        if (radios.length === 1) radios.prop('checked', true);
      })
      .on('click', '.dropdown-menu', (e) => e.stopPropagation())
      .on('input focus', 'input[type="text"]', (e) => {
        const $li = $(e.target).closest('.dropdown-item');
        $li.find('[type="radio"]').prop('checked', true);
      });
    $('#orig_contains, #transl_contains').on('click', (e) =>
      $(e.target).next().focus()
    );
    $('#panel-glossary__btn-add-term').click(addGlossaryTermOnServer);
    $('#panel-glossary__input-term').keydown((e) => {
      if (e.which == 13) {
        e.preventDefault();
        e.stopPropagation();
        const text = $(e.target).val().trim();
        if (text !== '') $('#panel-glossary__input-def').focus();
      }
    });
    $('#panel-glossary__input-def').keydown((e) => {
      if (e.which == 13) {
        e.preventDefault();
        e.stopPropagation();
        addGlossaryTermOnServer();
      }
    });
    $('.action-jump-untranslated').click(jumpUntranslated);
    $('#machine-translations').on(
      'click',
      '.machine-translation__text',
      (e) => {
        $('.translator-column-transl textarea')
          .text($(e.target).text())
          .focus();
      }
    );
    $('#panel-glossary-content').on('click', '.glossary-term', (e) => {
      const $el = $(e.target);
      const term = $el.text();
      const def = $el.next().text();
      $('#panel-glossary__input-term').val(term);
      $('#panel-glossary__input-def').val(def).focus().select();
    });
    $('.action-fuzzy-settings')
      .tooltip({
        placement: 'bottom',
        trigger: 'click',
        html: true,
        sanitize: false,
        title: $('#fuzzy-settings-tmpl').html(),
      })
      .on('shown.bs.tooltip', (e) => {
        const $tooltip = $('#' + $(e.target).attr('aria-describedby'));
        $tooltip
          .find('[name="matching-percentage"]')
          .val(matchingPercentage)
          .on('keydown', (e) => {
            if (e.which == 13) applyFuzzySettings(e);
          });
        $tooltip
          .find('[name="show-matches-w/o-translation"]')
          .prop('checked', showMatchesWithoutTranslation);
        $tooltip
          .find('.action-apply-fuzzy-settings')
          .on('click', applyFuzzySettings);
      });
    if (location.hash) {
      const $hl = $(location.hash);
      $hl.addClass('translator-row--highlighted');
      removeHighlight = () => {
        $hl.removeClass('translator-row--highlighted');
        $(document).off('dblclick', removeHighlight);
      };
      $(document).on('dblclick', removeHighlight);
    }
    $('[data-toggle="tooltip"]').tooltip();
    eventSaver.replay();
    $('body').addClass('interactive');
  });
});
// vim: ts=2 sts=2 sw=2 et
