define(['jquery', 'errors'], ($, errors) => {
  function addListeners($page) {
    $page.on('click', '[data-id], .see-also', (e) => {
      if (e.ctrlKey || e.shiftKey || e.altKey) return;
      load($(e.target).text(), null, !!$(e.target).attr('data-exact'));
    });
  }

  const defaultProperties = {
    title: 'Academic',
    tooltip: 'dic.academic.ru (synonyms)',
    pageClass: 'academic-synonyms',
    addListeners,
  };

  function load(text, callback, exact) {
    if (!/[а-яё]/i.test(text)) return;

    const data = { query: text.trim() };
    if (exact) data.exact = 1;
    $.ajax({
      url: '/plugins/academic',
      method: 'GET',
      data,
    })
      .done((data) => {
        const $header = $('<div class="academic-header">');
        const $seeAlso = $('<div class="academic-see-also">');
        const $body = $('<div class="academic-body">');
        $header.text(data.word);
        $body.html(data.html);
        if (data.see_also.length) {
          $seeAlso.html('<i>See also</i>: ');
          data.see_also.forEach((v) => {
            const $el = $(`<a class="see-also" data-exact="1">`).text(v);
            $seeAlso.append($el);
          });
        }
        const $page = $('<div></div>');
        $page.append($header, $seeAlso, $body);
        if (callback)
          callback(null, {
            html: $page.html(),
            ...defaultProperties,
          });
        else $(`.${defaultProperties.pageClass}`).html($page.html());
      })
      .fail((xhr, status, err) => {
        const msg = errors.makeMessage(xhr, status, err);
        if (callback)
          callback(msg, {
            ...defaultProperties,
          });
        else $(`.${defaultProperties.pageClass}`).html(msg);
      });

    return true;
  }

  return { load };
});
