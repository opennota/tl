define(['jquery', 'errors'], ($, errors) => {
  const defaultProperties = {
    title: 'Multitran',
    tooltip: 'multitran.com',
    pageClass: 'multitran',
  };

  function load(text, callback) {
    if (/[а-яё]/i.test(text)) return;

    $.ajax({
      url: '/plugins/multitran',
      method: 'GET',
      data: { query: text.trim() },
    })
      .done((data) => {
        callback(null, {
          html: data.html,
          ...defaultProperties,
        });
      })
      .fail((xhr, status, err) => {
        callback(errors.makeMessage(xhr, status, err), {
          ...defaultProperties,
        });
      });

    return true;
  }

  return { load };
});
