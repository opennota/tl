/* global book_id */
define(['jquery', 'popper', 'bootstrap'], ($, Popper) => {
  'use strict';

  const insidePopper = (el) => {
    while (el) {
      if (el.id === 'link-popper') return true;
      el = el.parentNode;
    }
    return false;
  };

  $(document).ready(() => {
    let popper;
    const $node = $('#link-popper');
    const $link = $node.find('.fragment-permalink');
    let prevTarget;
    $('.container').on('click', '.read__fragment', (e) => {
      const $target = $(e.currentTarget);
      e.stopPropagation();
      if (popper) {
        popper.destroy();
        popper = null;
        if (e.currentTarget === prevTarget) {
          $node.css('visibility', 'hidden');
          $target.removeClass('read__fragment--active');
          return;
        }
        if (prevTarget) {
          $(prevTarget).removeClass('read__fragment--active');
        }
      }
      prevTarget = e.currentTarget;
      $node.css('visibility', 'visible');
      $target.addClass('read__fragment--active');
      const fid = $target.data('fid');
      const seq = $target.data('seq');
      $link.attr('href', `/book/${book_id}/${fid}`);
      $link.text(`#${seq || fid}`);
      popper = new Popper(e.currentTarget, $node.get(0), {
        placement: 'left',
        modifiers: { offset: { offset: '0,5' } },
      });
    });
    $(document).on('click', (e) => {
      if (insidePopper(e.target)) return;
      if (popper) {
        popper.destroy();
        popper = null;
        $node.css('visibility', 'hidden');
        $('.read__fragment--active').each((i, el) =>
          $(el).removeClass('read__fragment--active')
        );
      }
    });
    $('.marks-toggler').on('click', () => {
      $('.read').toggleClass('hide-marks');
    });
  });
});
// vim: ts=2 sts=2 sw=2 et
