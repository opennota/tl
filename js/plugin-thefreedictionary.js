define(['jquery', 'errors'], ($, errors) => {
  function addListeners($page) {
    $page.on('click', '.link', (e) => {
      if (e.ctrlKey || e.shiftKey || e.altKey) return;
      load($(e.target).text());
    });
  }

  const defaultProperties = {
    title: 'TFD',
    tooltip: 'idioms.thefreedictionary.com',
    pageClass: 'thefreedictionary',
    addListeners,
  };

  function load(text, callback) {
    if (!/[a-z]/i.test(text)) return;

    $.ajax({
      url: '/plugins/thefreedictionary',
      method: 'GET',
      data: { query: text.trim() },
    })
      .done((data) => {
        if (callback)
          callback(null, {
            html: data.html,
            ...defaultProperties,
          });
        else $(`.${defaultProperties.pageClass}`).html(data.html);
      })
      .fail((xhr, status, err) => {
        const msg = errors.makeMessage(xhr, status, err);
        if (callback)
          callback(msg, {
            ...defaultProperties,
          });
        else $(`.${defaultProperties.pageClass}`).html(msg);
      });

    return true;
  }

  return { load };
});
