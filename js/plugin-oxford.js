define(['jquery', 'errors'], ($, errors) => {
  function addListeners($page) {
    $page
      .on('click', 'a.similar', (e) => {
        if (e.ctrlKey || e.shiftKey || e.altKey) return;
        load($(e.target).text());
      })
      .on('click', '.moreInfo button', (e) => {
        $(e.target).parent().toggleClass('active');
      });
  }

  const defaultProperties = {
    title: 'Oxford',
    tooltip: 'lexico.com',
    pageClass: 'oxford-dictionaries',
    addListeners,
  };

  function load(text, callback) {
    if (!/[a-z]/i.test(text)) return;

    $.ajax({
      url: '/plugins/oxford',
      method: 'GET',
      data: { query: text.trim() },
    })
      .done((data) => {
        callback(null, {
          html: data.html,
          ...defaultProperties,
        });
      })
      .fail((xhr, status, err) => {
        callback(errors.makeMessage(xhr, status, err), {
          ...defaultProperties,
        });
      });

    return true;
  }

  return { load };
});
