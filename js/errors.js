define([], () => {
  function makeMessage(xhr, status, err) {
    if (xhr.status == 404) return 'Not found.';
    if (xhr.readyState === 0) return 'Network error';
    return 'Error: ' + (err || 'unknown error');
  }
  return { makeMessage };
});
