/* global tmServer */
define(['jquery'], ($) => {
  const plugins = [];

  const stickyHistory = {};
  const stickyCurrent = [];
  let concordancePlugin;

  let userPressedAltS = false;

  function switchTo($page, onlyOnUserInteraction, clicked) {
    if (onlyOnUserInteraction && !userPressedAltS) {
      const index = $('.sticky-pills > li > a.active').parent().index();
      $('.sticky__back-button').toggleClass(
        'active',
        !!stickyHistory[index].length
      );
      return;
    }
    if (!clicked && $('.sticky-page.active').length) return;
    const index = $page.index();
    $('.sticky-pills > li > a').each((idx, el) =>
      $(el).toggleClass('active', idx === index)
    );
    $('.sticky-page').each((idx, el) =>
      $(el).toggleClass('active', idx === index)
    );
    $('.sticky__back-button').toggleClass(
      'active',
      !!stickyHistory[index].length
    );
    userPressedAltS = false;
    $('.sticky-tab-pages').scrollTop(0);
  }

  function getSelection(el) {
    const sel = document.getSelection();
    if (!sel || !sel.rangeCount) return;
    let text = sel.toString();

    // https://bugzilla.mozilla.org/show_bug.cgi?id=85686
    if (text === '' && el.nodeName == 'TEXTAREA') {
      if (el.selectionStart < el.selectionEnd)
        return el.value.substring(el.selectionStart, el.selectionEnd);
      let start = el.selectionStart;
      let end = el.selectionEnd;
      const rWord = /[-a-zа-яё\u0301]/iu;
      text = el.value;
      while (start > 0 && rWord.test(text.charAt(start - 1))) start--;
      while (end < text.length && rWord.test(text.charAt(end))) end++;
      return text.substring(start, end).replace(/^-|-$/g, '');
    }

    return text;
  }

  function lookup() {
    const text = $('#panel-dictionary__input-query').val().trim();
    if (text) lookupText(text);
  }

  function dictionaryPluginCallback(err, data) {
    if (err) return;
    const $pill = $(
      `<li class="nav-item" title="${data.tooltip}"><a class="nav-link">${data.title}</a></li>`
    );
    const $page = $(`<div class="sticky-page ${data.pageClass}"></div>`);
    $page.html(data.html);
    if (data.addListeners) data.addListeners($page);
    $('#panel-dictionary-pills').append($pill);
    const $pages = $('#panel-dictionary-pages');
    $pages.append($page);
    if (!data.dontActivate && !$pages.find('.active').length) $pill.click();
    $('#panel-dictionary-content').addClass('show');
  }

  function concordancePluginCallback(err, data) {
    if (err) return;
    const $pages = $('#panel-dictionary-pages');
    let $page = $(`.${data.pageClass}`);
    let $pill;
    if ($page.length)
      $pill = $(`#panel-dictionary-pills li:nth-child(${$page.index() + 1})`);
    else {
      $page = $(`<div class="sticky-page ${data.pageClass}"></div>`);
      $pill = $(
        `<li class="nav-item" title="${data.tooltip}"><a class="nav-link">${data.title}</a></li>`
      );
      if (data.addListeners) data.addListeners($page);
      $('#panel-dictionary-pills').append($pill);
      $pages.append($page);
    }
    $page.html(data.html);
    $page
      .find('.concordance-results')
      .html('<i class="fa fa-spinner fa-spin"></i>');
    $pill.click();
    $('#panel-dictionary-content').addClass('show');
    $('#sidebar-left').focus();
  }

  function lookupText(text) {
    text = text.trim();
    if (!text) return;
    const $input = $('#panel-dictionary__input-query');
    $input.val(text);
    if (document.activeElement.nodeName !== 'TEXTAREA') $input.focus().select();
    userPressedAltS = true;
    $('#panel-dictionary-pills, #panel-dictionary-pages').text('');
    let n = 0;
    for (const plugin of plugins) {
      if (
        plugin.load(text, (err, data) => {
          if (!--n) {
            $('#panel-dictionary__lookup-spin').addClass('d-none');
            if (!$('#panel-dictionary-pages > .active').length)
              $('#panel-dictionary-pills > *:first-child').click();
          }
          dictionaryPluginCallback(err, data);
        })
      )
        n++;
    }
    if (n) $('#panel-dictionary__lookup-spin').removeClass('d-none');
  }

  function lookupSelection(e) {
    const text = getSelection(e.target);
    if (text) lookupText(text);
    else
      $('#panel-dictionary__input-query')
        .focus()
        .select()
        .get(0)
        .scrollIntoView();
  }

  function showConcordance(e) {
    const text = getSelection(e.target).trim();
    if (text) concordancePlugin.load(text, concordancePluginCallback, true);
  }

  $(document).on('keydown', (e) => {
    if (e.altKey && !(e.shiftKey || e.ctrlKey)) {
      if (!plugins.length) return;
      if (e.which == 83 /* Alt-S */) {
        e.preventDefault();
        e.stopPropagation();
        lookupSelection(e);
      } else if (e.which == 79 /* Alt-O */) {
        e.preventDefault();
        e.stopPropagation();
        if (concordancePlugin) showConcordance(e);
      }
    }
  });
  $('.action-lookup').on('click', lookup);
  $('#panel-dictionary__input-query')
    .on('keydown', (e) => {
      if (e.which == 13) {
        e.preventDefault();
        e.stopPropagation();
        lookup();
      }
    })
    .on('focus', (e) => $(e.target).select());
  $('.action-lookup-selection').on('click', lookupSelection);

  $('.sticky__back-button').on('click', () => {
    const index = $('.sticky-pills > li > a.active').parent().index();
    stickyCurrent[index] = null;
    const value = stickyHistory[index].pop();
    plugins[index].load(value);
  });
  $('.sticky-pills').on('click', 'li', (e) =>
    switchTo($(e.currentTarget), false, true)
  );

  return {
    registerPlugin: (plugin) => {
      stickyHistory[plugins.length] = [];
      plugins.push(plugin);
      if (plugin.name === 'concordance') {
        plugin.useServer(tmServer);
        concordancePlugin = plugin;
      }
    },
  };
});
// vim: ts=2 sts=2 sw=2 et
