define(['jquery', 'errors'], ($, errors) => {
  function addListeners($page) {
    $page.on('click', 'a', (e) => {
      if (e.ctrlKey || e.shiftKey || e.altKey) return;
      load($(e.target).text(), null, 1);
    });
  }

  const defaultProperties = {
    title: 'KS',
    tooltip: 'kartaslov.ru (synonyms)',
    pageClass: 'kartaslov-synonyms',
    addListeners,
  };

  function load(text, callback, exact) {
    if (!/[а-яё]/i.test(text)) return;

    const data = { query: text.trim() };
    if (exact) data.exact = 1;
    $.ajax({
      url: '/plugins/kartaslov',
      method: 'GET',
      data,
    })
      .done((data) => {
        const $header = $('<div class="kartaslov-header">');
        const $seeAlso = $('<div class="kartaslov-see-also">');
        const $body = $('<div class="kartaslov-body">');
        $header.text(data.word);
        if (data.see_also.length) {
          $seeAlso.html('<i>See also</i>: ');
          data.see_also.forEach((v) => {
            const $el = $(`<a class="see-also" data-exact="1">`).text(v);
            $seeAlso.append($el);
          });
        }
        $body.html(data.html);
        const $page = $('<div></div>');
        $page.append($header, $seeAlso, $body);
        if (callback)
          callback(null, {
            html: $page.html(),
            ...defaultProperties,
          });
        else $(`.${defaultProperties.pageClass}`).html($page.html());
      })
      .fail((xhr, status, err) => {
        const msg = errors.makeMessage(xhr, status, err);
        if (callback)
          callback(msg, {
            ...defaultProperties,
          });
        else $(`.${defaultProperties.pageClass}`).html(msg);
      });

    return true;
  }

  return { load };
});
