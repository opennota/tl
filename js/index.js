define(['jquery', 'js.cookie', 'bootbox', 'eventSaver', 'editable-title'], (
  $,
  Cookies,
  bootbox,
  eventSaver
) => {
  'use strict';

  const months = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ];

  const zpad2 = (n) => {
    if (n >= 10) {
      return n;
    }
    return '0' + n;
  };

  const zpad4 = (n) => {
    if (n >= 1000) {
      return n;
    }
    if (n >= 100) {
      return '0' + n;
    }
    if (n >= 10) {
      return '00' + n;
    }
    return '000' + n;
  };

  const format = (d) => {
    const day = d.getDate();
    const month = months[d.getMonth()];
    const year = d.getFullYear();
    const h = d.getHours();
    const m = d.getMinutes();
    const s = d.getSeconds();
    return `${zpad2(day)}-${month}-${zpad4(year)} ${zpad2(h)}:${zpad2(
      m
    )}:${zpad2(s)}`;
  };

  const pretty = (d) => {
    const seconds = Math.floor((new Date() - d) / 1e3);
    const days = Math.floor(seconds / (60 * 60 * 24));
    if (days < 0) {
      return 'somewhen in the future';
    }
    if (days === 0) {
      if (seconds < 60 * 60) {
        const minutes = Math.floor(seconds / 60);
        if (minutes === 0) {
          return 'just now';
        }
        if (minutes === 1) {
          return '1 minute ago';
        }
        return minutes + ' minutes ago';
      }
      const hours = Math.floor(seconds / (60 * 60));
      if (hours === 1) {
        return '1 hour ago';
      }
      return hours + ' hours ago';
    }
    if (days < 7) {
      if (days === 1) {
        return 'Yesterday';
      }
      return days + ' days ago';
    }
    if (days < 31) {
      const weeks = Math.floor(days / 7);
      if (weeks === 1) {
        return '1 week ago';
      }
      return weeks + ' weeks ago';
    }
    return format(d);
  };

  function errmsg(xhr, status, err) {
    if (xhr.readyState === 0) return 'Network error';
    return 'Error: ' + (xhr.responseText || err || 'unknown error');
  }

  function filterTitles(filter) {
    if (filter === undefined || filter === '') {
      $('table > tbody > tr').show();
      return;
    }
    const lowercaseFilter = filter.toLowerCase();
    $('table > tbody > tr').each((index, el) => {
      const $tr = $(el);
      const title = $tr.find('.book-title-link').text();
      $tr.toggle(title.toLowerCase().includes(lowercaseFilter));
    });
  }

  function showTitleFilter(e) {
    if ($('#filter').length) return;
    const $th = $('.title-header');
    const savedHtml = $th.html();
    const $input = $('<input id="filter" type="text"></input>');
    $th.text('Filter: ');
    $th.append($input);
    $input.on('click', (e) => e.stopPropagation());
    const hideFilterIfEmpty = () => {
      if ($input.val() === '') $th.html(savedHtml);
    };
    $input
      .on('blur', hideFilterIfEmpty)
      .on('input', () => {
        filterTitles($input.val());
      })
      .focus();
    e.stopPropagation();
  }

  function showRemoveDialog() {
    const dlg = bootbox.dialog({
      title: 'Remove book',
      message: $('#remove-dialog-tmpl').html(),
      onEscape: true,
      backdrop: true,
    });
    const $alert = dlg.find('.alert');
    const $select = dlg.find('select');
    const $btn = dlg.find('button[type="submit"]');
    const remove = (bookId) => {
      $.ajax({
        method: 'DELETE',
        url: `/book/${bookId}`,
      })
        .done(() => {
          location.reload(false);
        })
        .fail((xhr, status, err) => {
          $alert.text(errmsg(xhr, status, err)).show();
        });
    };
    dlg.init(() => {
      $.ajax({
        method: 'GET',
        url: '/books',
      })
        .done((books) => {
          $select.find('option').text('');
          for (let b of books) {
            const $opt = $('<option>');
            $opt.val(b.id);
            $opt.text(b.title);
            $select.append($opt);
          }
          $select.on('change', () => {
            $btn.prop('disabled', !$select.val());
          });
          $btn.on('click', () => {
            remove($select.val());
          });
        })
        .fail((xhr, status, err) => {
          $select.find('option').text('');
          $alert.text(errmsg(xhr, status, err)).show();
        });
    });
  }

  $(document).ready(() => {
    $(document).on('click', '.action-show-title-filter', showTitleFilter);
    $('.action-remove-book').on('click', showRemoveDialog);
    eventSaver.replay();
    setInterval(() => {
      $('time').each((_, el) => {
        const $el = $(el);
        const text = $el.text();
        const d = new Date($el.attr('datetime'));
        const newText = pretty(d);
        if (newText != text) {
          $el.text(newText);
        }
      });
    }, 60 * 1000);

    $('.button-sort-by').on('click', (e) => {
      Cookies.set('sort-by', $(e.target).attr('data-sort-by'));
      location.href = '/';
    });
  });
});
// vim: ts=2 sts=2 sw=2 et
