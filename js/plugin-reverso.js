define(['jquery', 'errors'], ($, errors) => {
  function addListeners($page) {
    $page.on('click', '.reverso-more-examples', (e) => {
      if (e.ctrlKey || e.shiftKey || e.altKey) return;
      loadNext(e);
    });
  }

  const defaultProperties = {
    title: 'Reverso',
    tooltip: 'context.reverso.net',
    pageClass: 'reverso-context',
    addListeners,
  };

  function loadNext(e) {
    const $el = $(e.target);
    const text = $el.attr('data-text');
    const page = 1 + Number($el.attr('data-page'));
    const $page = $('.reverso-context');

    $el.find('i').removeClass('d-none');

    $.ajax({
      url: '/plugins/reverso',
      method: 'GET',
      data: { query: text.trim(), page },
    })
      .done((data) => {
        $page.append(data.html);
        if (data.more_examples) {
          $el.find('i').addClass('d-none');
          $el.attr('data-page', page);
          $('.reverso-context').append($el);
        } else $el.remove();
      })
      .fail((xhr, status, err) => {
        $el.find('i').addClass('d-none');
        $page.append(errors.makeMessage(xhr, status, err));
      });
  }

  function load(text, callback) {
    if (!/[a-z]/i.test(text)) return;

    $.ajax({
      url: '/plugins/reverso',
      method: 'GET',
      data: { query: text.trim() },
    })
      .done((data) => {
        let additionalHtml = '';
        if (data.more_examples) {
          const $btn = $(
            `<a class="btn btn-light reverso-more-examples">
               More examples
               <i class="fa fa-spinner fa-spin d-none"></i>
             </a>`
          );
          $btn.attr('data-text', text);
          $btn.attr('data-page', 1);
          additionalHtml = $('<div>').append($btn).html();
        }
        callback(null, {
          html: data.html + additionalHtml,
          ...defaultProperties,
        });
      })
      .fail((xhr, status, err) => {
        callback(errors.makeMessage(xhr, status, err), {
          ...defaultProperties,
        });
      });

    return true;
  }

  return { load };
});
