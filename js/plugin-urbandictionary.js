define(['jquery', 'errors'], ($, errors) => {
  function addListeners($page) {
    $page.on('click', 'a', (e) => {
      if (e.ctrlKey || e.shiftKey || e.altKey) return;
      load($(e.target).text());
    });
  }

  const defaultProperties = {
    title: 'Urban',
    tooltip: 'urbandictionary.com',
    pageClass: 'urbandictionary',
    addListeners,
  };

  function load(text, callback) {
    if (!/[a-z]/i.test(text)) return;

    $.ajax({
      url: '/plugins/urbandictionary',
      method: 'GET',
      data: { query: text.trim() },
    })
      .done((data) => {
        callback(null, {
          html: data.html,
          ...defaultProperties,
        });
      })
      .fail((xhr, status, err) => {
        callback(errors.makeMessage(xhr, status, err), {
          ...defaultProperties,
        });
      });

    return true;
  }

  return { load };
});
