/* global pageNumber, rowsPerPage, colorize:true, nonce:true */
define(['jquery', 'bootbox', 'eventSaver', 'jquery.autogrowtextarea'], (
  $,
  bootbox,
  eventSaver
) => {
  'use strict';

  function flip(side) {
    return side === 'left' ? 'right' : 'left';
  }

  function appendWords($el, words) {
    $el.append(
      words.map((w) => $('<span>').text(w).add(document.createTextNode(' ')))
    );
  }

  function columnUp($row, sel) {
    while ($row.length) {
      $row.prev().find(sel).html($row.find(sel).html());
      $row = $row.next();
    }
  }

  function moveRest($start, $to) {
    let el = $start.get(0);
    while (el) {
      const next = el.nextSibling;
      $to.append(el);
      el = next;
    }
  }

  function checkErr(xhr, status, err) {
    if (xhr.status == 409 /* Conflict */) {
      bootbox.alert({
        message: 'Achtung! The aligner is out of sync and will be reloaded.',
        backdrop: true,
        callback: () => {
          location.href = '/aligner';
        },
      });
    } else {
      const contentType = xhr.getResponseHeader('content-type');
      if (
        contentType &&
        contentType.includes('text/plain') &&
        xhr.responseText
      ) {
        bootbox.alert('Error: ' + xhr.responseText);
      } else {
        let msg = 'Network error';
        if (xhr.readyState !== 0)
          msg = 'Error: ' + (xhr.responseText || err || 'unknown error');
        bootbox.alert(msg);
      }
    }
  }

  function split(e) {
    if (e.ctrlKey || e.shiftKey || e.altKey) return;
    e.stopPropagation();
    const $span = $(e.target);
    const $td = $span.parent();
    const $tr = $td.parent();
    const side = $td.attr('data-side');
    $.ajax({
      method: 'POST',
      url: '/aligner',
      data: {
        op: 'split',
        side,
        page: pageNumber,
        row: $tr.index(),
        word: $span.index(),
        nonce: nonce++,
      },
    })
      .done(() => {
        const $newRow = $($('#row-tmpl').text());
        const $nextRow = $tr.next();
        $tr.after($newRow);
        const $td = $newRow.find('.aligner-column-' + side);
        moveRest($span, $td);
        const sel = '.aligner-column-' + flip(side);
        columnUp($nextRow, sel);
        const $lastRow = $tr.parent().children().last();
        if ($lastRow.find('.aligner-column-' + side + ' span').length) {
          $lastRow.find(sel).html('');
        } else {
          $lastRow.remove();
        }
        const $rows = $tr.parent().children();
        if ($rows.length > rowsPerPage) {
          $rows.last().remove();
        }
        if (colorize) requestSimilarities();
      })
      .fail(checkErr);
  }

  function join(e) {
    e.preventDefault();
    e.stopPropagation();
    const $td = $(e.currentTarget);
    if ($td.find('textarea').length) return;
    const $tr = $td.parent();
    const side = $td.attr('data-side');
    $.ajax({
      method: 'POST',
      url: '/aligner',
      data: {
        op: 'join',
        side,
        page: pageNumber,
        row: $tr.index(),
        nonce: nonce++,
      },
    })
      .done((data) => {
        const sel = '.aligner-column-' + side;
        const $td1 = $tr.find(sel);
        const $nextRow = $tr.next();
        if (data[0]) {
          appendWords($td1, data[0]);
        }
        columnUp($nextRow.next(), sel);

        const $lastRow = $tr.parent().children().last();
        if (data[1]) {
          const $td = $lastRow.find(sel);
          $td.html('');
          appendWords($td, data[1]);
        } else {
          if ($lastRow.find('.aligner-column-' + flip(side) + ' span').length) {
            $lastRow.find(sel).html('');
          } else {
            $lastRow.remove();
          }
        }
        getSelection().removeAllRanges();
        if (colorize) requestSimilarities();
      })
      .fail(checkErr);
  }

  function rm(e) {
    const $tr = $(e.target).closest('tr');
    $.ajax({
      method: 'POST',
      url: '/aligner',
      data: {
        op: 'rm',
        page: pageNumber,
        row: $tr.index(),
        nonce: nonce++,
      },
    })
      .done((data) => {
        const $tbody = $tr.parent();
        $tr.remove();
        if (!data[0] && !data[1]) return;
        const $newRow = $($('#row-tmpl').text());
        if (data[0]) {
          appendWords($newRow.find('.aligner-column-left'), data[0]);
        }
        if (data[1]) {
          appendWords($newRow.find('.aligner-column-right'), data[1]);
        }
        $tbody.append($newRow);
        if (colorize) requestSimilarities();
      })
      .fail(checkErr);
  }

  function edit(e) {
    e.preventDefault();
    e.stopPropagation();
    const $td = $(e.currentTarget);
    if ($td.find('textarea').length) return;
    const $tr = $td.parent();
    const side = $td.attr('data-side');
    const origHtml = $td.html();
    const text = $td
      .children()
      .map((index, el) => $(el).text())
      .get()
      .join(' ');
    const $form = $($('#edit-form-tmpl').text());
    const $textarea = $form.find('textarea');
    $textarea.text(text);
    $td.html($form);
    $form
      .on('click', '.action-save-edit', (e) => {
        e.preventDefault();
        const text = $textarea
          .val()
          .replace(/\s{2,}/g, ' ')
          .replace(/^\s|\s$/g, '');
        $.ajax({
          method: 'POST',
          url: '/aligner',
          data: {
            op: 'edit',
            side,
            page: pageNumber,
            row: $tr.index(),
            text,
            nonce: nonce++,
          },
        })
          .done((words) => {
            $td.html('');
            appendWords($td, words);
            if (colorize) requestSimilarities();
          })
          .fail(checkErr);
      })
      .on('click', '.action-cancel-edit', () => {
        $td.html(origHtml);
      });
    $textarea
      .on('keydown', (e) => {
        if (e.ctrlKey && e.which == 13) {
          e.stopPropagation();
          $form.find('.action-save-edit').click();
        }
      })
      .autoGrow()
      .focus();
  }

  function gettext($row, leftOrRight) {
    return $row
      .find(`.aligner-column-${leftOrRight} span`)
      .map((_, x) => x.textContent)
      .toArray()
      .join(' ');
  }

  function requestSimilarities() {
    const spinnerHtml = '<i class="fa fa-spin fa-spinner"></i>';
    const rows = [];
    const a = [];
    const b = [];
    $('.aligner-column-similarity').each((_, el) => {
      const $row = $(el).closest('.aligner-row');
      const l = gettext($row, 'left');
      const r = gettext($row, 'right');
      if (l === '' || r === '') {
        el.textContent = '0%';
        el.dataset.left = l;
        el.dataset.right = r;
        el.setAttribute('style', 'background-color: red');
        return;
      }
      if (el.dataset.left === l && el.dataset.right === r) return;
      el.removeAttribute('style');
      el.innerHTML = spinnerHtml;
      rows.push($row);
      a.push(l);
      b.push(r);
    });
    if (!rows.length) return;
    $.ajax({
      method: 'POST',
      url: '/aligner',
      data: {
        op: 'getsimilarity',
        a: a,
        b: b,
      },
    })
      .done((data) => {
        for (let i = 0; i < data.length; i++) {
          const $row = rows[i];
          const $simCell = $row.find('.aligner-column-similarity');
          if (gettext($row, 'left') !== a[i] || gettext($row, 'right') !== b[i])
            continue;
          $simCell.text('');
          const sim = data[i];
          $simCell.text(`${(sim * 100).toFixed(0)}%`);
          const h = ((1 - sim) * 360).toFixed(2);
          if (sim < 0.5)
            $simCell.attr('style', `background-color: hsl(${h}, 100%, 50%)`);
          $simCell.get(0).dataset.left = a[i];
          $simCell.get(0).dataset.right = b[i];
          const minLen = Math.min(a[i].length, b[i].length);
          const maxLen = Math.max(a[i].length, b[i].length);
          const score = (maxLen - minLen) / (minLen + 29);
          if (score > 0.7) $simCell.attr('style', 'background-color: pink');
        }
      })
      .fail((xhr, status, err) => {
        $('.aligner-column-similarity .fa-spinner').remove();
        checkErr(xhr, status, err);
      });
  }

  $(document).ready(() => {
    $('.aligner-table')
      .on('click', 'span', split)
      .on('click', 'td', (e) => {
        if (e.shiftKey && !(e.ctrlKey || e.altKey)) {
          join(e);
        } else if (e.ctrlKey && !(e.shiftKey || e.altKey)) {
          edit(e);
        }
      })
      .on('click', '.aligner-row__icon-remove', rm);
    $('.action-swap').on('click', () => {
      $.ajax({
        method: 'POST',
        url: '/aligner',
        data: { op: 'swap', nonce: nonce++ },
      })
        .done(() => {
          location.href = '/aligner';
        })
        .fail(checkErr);
    });
    $('.action-clear').on('click', () => {
      bootbox.confirm({
        message: 'Are you sure?',
        callback: (result) => {
          if (!result) return;
          $.ajax({
            method: 'POST',
            url: '/aligner',
            data: { op: 'clear', nonce: nonce++ },
          })
            .done(() => {
              location.href = '/aligner';
            })
            .fail(checkErr);
        },
      });
    });
    if (colorize) requestSimilarities();
    $('.action-colorize').on('click', (e) => {
      const $check = $(e.target);
      colorize = $check.prop('checked');
      $.ajax({
        method: 'POST',
        url: '/aligner',
        data: { op: 'setcolorize', value: colorize, nonce: nonce++ },
      }).fail(checkErr);
      if (colorize) requestSimilarities();
      else
        $('.aligner-column-similarity')
          .text('')
          .removeAttr('style')
          .each((_, el) => (delete el.dataset.left, delete el.dataset.right));
    });
    $('.action-import').on('click', () => {
      const dlg = bootbox.prompt({
        title: 'Import with title:',
        callback: (title) => {
          if (!title || title.trim() == '') return;
          $.ajax({
            method: 'POST',
            url: '/aligner',
            data: {
              op: 'import',
              title: title,
              nonce: nonce++,
            },
          })
            .done((book_id) => {
              location.href = `/book/${book_id}`;
            })
            .fail(checkErr);
        },
      });
      dlg.init(() => {
        const $input = dlg.find('input');
        const $okButton = dlg.find('.btn-primary');
        $okButton.prop('disabled', true);
        $input.on('change keyup input paste', () => {
          const text = $input.val();
          $okButton.prop('disabled', !text || text.trim() === '');
        });
      });
    });
    $('[data-toggle="popover"]').popover();
    eventSaver.replay();
  });
});
// vim: ts=2 sts=2 sw=2 et
