/* global book_id */
define(['jquery', 'bootbox', 'eventSaver', 'bootstrap'], (
  $,
  bootbox,
  eventSaver
) => {
  'use strict';

  function handleError(xhr, status, err) {
    if (xhr.readyState === 0) bootbox.alert('Network error');
    else bootbox.alert('Error: ' + (err || 'unknown error'));
  }

  function errmsg(xhr, status, err) {
    if (xhr.readyState === 0) return 'Network error';
    return 'Error: ' + (xhr.responseText || err || 'unknown error');
  }

  function showMergeDialog() {
    const dlg = bootbox.dialog({
      title: 'Merge another glossary',
      message: $('#merge-dialog-tmpl').html(),
      onEscape: true,
      backdrop: true,
    });
    const $alert = dlg.find('.alert');
    const $select = dlg.find('select');
    const $btn = dlg.find('button[type="submit"]');
    const merge = (from, mergeStrategy) => {
      $.ajax({
        method: 'POST',
        url: `/book/${book_id}/glossary/merge`,
        data: {
          from,
          mergeStrategy,
        },
      })
        .done(() => {
          location.reload(true);
        })
        .fail((xhr, status, err) => {
          $alert.text(errmsg(xhr, status, err)).show();
        });
    };
    dlg.init(() => {
      $.ajax({
        method: 'GET',
        url: '/books',
      })
        .done((books) => {
          $select.find('option').text('');
          for (let b of books) {
            if (b.id === book_id) continue;
            const $opt = $('<option>');
            $opt.val(b.id);
            $opt.text(b.title);
            $select.append($opt);
          }
          $select.on('change', () => {
            $btn.prop('disabled', !$select.val());
          });
          $btn.on('click', () => {
            const mergeStrategy = dlg
              .find('[name="merge-strategy"]:checked')
              .val();
            merge($select.val(), mergeStrategy);
          });
        })
        .fail((xhr, status, err) => {
          $select.find('option').text('');
          $alert.text(errmsg(xhr, status, err)).show();
        });
    });
  }

  function clickOrEnter(e) {
    const $target = $(e.currentTarget);
    const $parentDiv = $target.parent();
    const $inputTerm = $parentDiv.find('.glossary__input-term');
    const $inputDef = $parentDiv.find('.glossary__input-def');
    const data = {
      term: $inputTerm.val().trim(),
      def: $inputDef.val().trim(),
    };
    const del = data.term === '' || data.def === '';
    const $termOrig = $parentDiv.find('.glossary__input-term-orig');
    if ($termOrig.length) {
      data.termOrig = $termOrig.val();
      if (del) {
        data.term = '';
        data.def = '';
      }
    } else if (del) {
      return;
    }
    $.ajax({
      method: 'POST',
      url: `/book/${book_id}/glossary`,
      data,
    })
      .done(() => {
        location.reload(true);
      })
      .fail(handleError);
  }

  $(document).ready(() => {
    $('.action-post-term').on('click', clickOrEnter);
    $('.glossary__input-term, .glossary__input-def').on('keydown', (e) => {
      if (e.which == 13) {
        e.preventDefault();
        clickOrEnter(e);
      }
    });
    $('.action-glossary-merge').on('click', showMergeDialog);
    eventSaver.replay();
  });
});
// vim: ts=2 sts=2 sw=2 et
