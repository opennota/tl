define(['jquery', 'errors'], ($, errors) => {
  let server = '';

  const defaultProperties = {
    title: 'Concordance',
    pageClass: 'concordance',
    addListeners,
    dontActivate: true,
  };

  function addListeners($page) {
    $page
      .on('click', '.concordance__btn-search', (e) => {
        if (e.ctrlKey || e.shiftKey || e.altKey) return;
        actuallyLoad($page.find('.what').text());
      })
      .on('click', '.concordance__btn-more', (e) => {
        const $moreBtn = $(e.target);
        const q = $moreBtn.attr('data-query');
        const from = $moreBtn.attr('data-next');
        const $page = $(`.${defaultProperties.pageClass}`);
        const $innerPage = $page.find('.concordance-results');
        $moreBtn.attr('disabled', true);
        $.ajax({
          url: server + '/plugins/concordance',
          method: 'GET',
          data: { q, from },
        })
          .done((html) => {
            const $html = $(html);
            $innerPage.find('tbody').append($html.find('tbody tr'));
            const $newBtn = $html.find('.concordance__btn-more');
            if ($newBtn.length) $moreBtn.replaceWith($newBtn);
            else $moreBtn.remove();
          })
          .fail((xhr, status, err) => {
            $moreBtn.attr('disabled', false);
            $innerPage.text(errors.makeMessage(xhr, status, err));
          });
      });
  }

  function load(text, callback, reallyLoad) {
    if (/[а-яё]/i.test(text)) return;

    const $page = $(
      '<div><div class="concordance-hdr"><button class="btn btn-light concordance__btn-search">Search</button> for "<span class="what"></span>"</div><div class="concordance-results"></div></div>'
    );
    $page.find('.what').text(text);
    const props = { ...defaultProperties };
    if (reallyLoad) props.dontActivate = false;
    callback(null, {
      html: $page.html(),
      ...props,
    });

    if (reallyLoad) actuallyLoad(text);

    return true;
  }

  function actuallyLoad(text) {
    const $page = $(`.${defaultProperties.pageClass}`);
    const $searchBtn = $page.find('.concordance__btn-search');
    $searchBtn.attr('disabled', true);

    const $innerPage = $page.find('.concordance-results');

    $.ajax({
      url: server + '/plugins/concordance',
      method: 'GET',
      data: { q: text.trim() },
    })
      .done((html) => {
        $searchBtn.attr('disabled', false);
        $innerPage.html(html);
      })
      .fail((xhr, status, err) => {
        $searchBtn.attr('disabled', false);
        $innerPage.text(errors.makeMessage(xhr, status, err));
      });
  }

  function useServer(srv) {
    server = srv;
  }

  return { load, name: 'concordance', useServer };
});
