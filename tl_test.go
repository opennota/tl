// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

//go:build e2e

package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"github.com/chromedp/cdproto/cdp"
	"github.com/chromedp/cdproto/emulation"
	"github.com/chromedp/cdproto/input"
	"github.com/chromedp/cdproto/log"
	"github.com/chromedp/cdproto/page"
	"github.com/chromedp/cdproto/runtime"
	"github.com/chromedp/chromedp"
	"github.com/chromedp/chromedp/kb"
)

const (
	testDB  = "testdata/testdb"
	rootURL = "http://localhost:3000"
)

const getTranslationRowsAsCSVJS = `(function() {
	const rows = document.querySelectorAll('.translator-row');
	let csv = '';
	for (let i = 0; i < rows.length; i++) {
		const row = rows[i];
		const orig = row.querySelector('.translator-column-orig .text');
		const ts = row.querySelectorAll('.translator-column-transl .translation .text');
		let text = '';
		for (let j = 0; j < ts.length; j++) {
			if (j) text += '\t';
			text += ts[j].textContent.trim();
		}
		csv += (orig ? orig.textContent.trim() : '') + ',' + text + '\n';
	}
	return csv;
})();`

const getBSModalVisible = `
(function() {
	const m = document.querySelector('.modal');
	if (!m) return false;
	const k = Object.keys(m).find(k => k.startsWith('jQuery') && m[k]['bs.modal']);
	const d = m[k]['bs.modal'];
	return d._isShown && !d._isTransitioning;
})();
`

func bsModalVisible(s *chromedp.Selector) {
	chromedp.WaitFunc(func(ctx context.Context, cur *cdp.Frame, _ runtime.ExecutionContextID, ids ...cdp.NodeID) ([]*cdp.Node, error) {
		if len(ids) == 0 {
			return nil, nil
		}
		var visible bool
		if err := chromedp.Evaluate(getBSModalVisible, &visible).Do(ctx); err != nil {
			return nil, err
		} else if !visible {
			return nil, nil
		}
		return []*cdp.Node{}, nil
	})(s)
}

func waitBSModalVisible() chromedp.QueryAction {
	return chromedp.Query(".modal", bsModalVisible, chromedp.AtLeast(0))
}

func httpGetAsString(url string) (string, error) {
	resp, err := http.DefaultClient.Get(url)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	return string(data), nil
}

func selectTextRangeAction(sel string, start, end int) chromedp.EvaluateAction {
	var res bool
	return chromedp.Evaluate(fmt.Sprintf(`
(function (selector, start, end) {
	const el = document.querySelector(selector);
	const startNode = el.firstChild;
	const endNode = startNode;
	startNode.nodeValue = startNode.nodeValue.trim();

	const range = document.createRange();
	range.setStart(startNode, start);
	range.setEnd(endNode, end + 1);

	const sel = window.getSelection();
	sel.removeAllRanges();
	sel.addRange(range);

	document.dispatchEvent(new Event('selectionchange'));

	return true;
})(%q, %d, %d);`, sel, start, end), &res)
}

func testTranslator(t *testing.T, ctx context.Context) {
	origCtx := ctx
	ctx, _ = context.WithTimeout(ctx, 120*time.Second)
	var location string
	var text string
	if err := chromedp.Run(ctx,
		chromedp.Navigate(rootURL),

		chromedp.Click(".button-add-book"),
		chromedp.WaitVisible(".form-add-book-plaintext"),

		chromedp.SetValue("#title", "test"),
		chromedp.SetValue("#content", "one\ntwo\n\n\nthree"),
		chromedp.Submit("#title"),
		chromedp.WaitVisible(".translator"),
		chromedp.Location(&location),
		chromedp.Evaluate(getTranslationRowsAsCSVJS, &text),
	); err != nil {
		t.Fatalf("could not add a book: %v", err)
	} else if want := "one,\ntwo,\nthree,\n"; text != want {
		t.Fatalf("could not add a book: want %q, got %q", want, text)
	}

	path := strings.TrimPrefix(location, rootURL)
	editBtnSel := fmt.Sprintf("//a[@href='%s']/parent::*/i[contains(@class,'fa-pencil')]", path)
	var title string
	if err := chromedp.Run(ctx,
		chromedp.Navigate(rootURL),
		chromedp.Click(editBtnSel),
		chromedp.WaitVisible(".editableform"),
		chromedp.Value(".editable-input > input", &title),
		chromedp.ActionFunc(func(context.Context) error {
			if title != "test" {
				return fmt.Errorf("editable title: want title to be \"test\", got %q", title)
			}
			return nil
		}),
		chromedp.Click(".editable-clear-x"),
		chromedp.Value(".editable-input > input", &title),
		chromedp.ActionFunc(func(context.Context) error {
			if title != "" {
				return fmt.Errorf("editable title: want title to be cleared, got %q", title)
			}
			return nil
		}),
		chromedp.SetValue(".editable-input > input", "new title"),
		chromedp.Click(".editable-submit"),
		chromedp.WaitNotPresent(".editableform"),
		chromedp.Text(fmt.Sprintf("a[href='%s']", path), &title),
		chromedp.ActionFunc(func(context.Context) error {
			if title != "new title" {
				return fmt.Errorf("editable title: want title to be \"new title\", got %q", title)
			}
			return nil
		}),
	); err != nil {
		saveScreenshot(t, origCtx)
		t.Fatalf("could not rename a book: %v", err)
	}

	const firstRowSel = ".translator-row:first-child"
	const translate1BtnSel = firstRowSel + " .action-translate"
	const textarea1Sel = firstRowSel + " .translator-column-transl textarea"
	const transl1Sel = firstRowSel + " .translator-column-transl .translation"
	const transl2Sel = firstRowSel + " .translator-column-transl .translation:nth-child(2)"
	const submitBtn1Sel = firstRowSel + " .translator-column-transl form button[type='submit']"
	var percent, fraction string
	if err := chromedp.Run(ctx,
		chromedp.Navigate(location),
		chromedp.Click(translate1BtnSel),
		chromedp.WaitVisible(textarea1Sel),
		chromedp.SetValue(textarea1Sel, "один"),
		chromedp.Click(submitBtn1Sel),
		chromedp.WaitNotPresent(textarea1Sel),
		chromedp.Evaluate(getTranslationRowsAsCSVJS, &text),
		chromedp.Text(".progress__percent", &percent),
		chromedp.AttributeValue(".progress-fraction", "title", &fraction, nil),
	); err != nil {
		t.Fatalf("could not translate: %v", err)
	} else if want := "one,один\ntwo,\nthree,\n"; text != want {
		t.Fatalf("could not translate: want %q, got %q", want, text)
	}
	if percent != "33" {
		t.Errorf("progress bar: want 33%%, got %s%%", percent)
	}
	if fraction != "1/3" {
		t.Errorf("progress bar: want 1/3 translated, got %s", fraction)
	}

	if err := chromedp.Run(ctx,
		chromedp.Click(translate1BtnSel),
		chromedp.WaitVisible(textarea1Sel),
		chromedp.SetValue(textarea1Sel, "раз"),
		chromedp.Click(submitBtn1Sel),
		chromedp.WaitNotPresent(textarea1Sel),
		chromedp.Evaluate(getTranslationRowsAsCSVJS, &text),
	); err != nil {
		t.Fatalf("could not translate: %v", err)
	} else if want := "one,один\tраз\ntwo,\nthree,\n"; text != want {
		t.Fatalf("could not translate: want %q, got %q", want, text)
	}

	const transl2RemoveBtnSel = firstRowSel + " .translator-column-transl .translation:nth-child(2) .action-remove-transl"
	if err := chromedp.Run(ctx,
		chromedp.Click(transl2RemoveBtnSel),
		waitBSModalVisible(),
		chromedp.Click(".button-confirm-remove-transl"),
		chromedp.WaitNotPresent(".modal-dialog"),
		chromedp.WaitNotPresent(".translation:nth-child(2)"),
		chromedp.Evaluate(getTranslationRowsAsCSVJS, &text),
	); err != nil {
		t.Fatalf("could not remove translation version: %v", err)
	} else if want := "one,один\ntwo,\nthree,\n"; text != want {
		t.Fatalf("could not remove translation version: want %q, got %q", want, text)
	}

	const transl1EditBtnSel = firstRowSel + " .translator-column-transl .translation:first-child .action-edit-transl"
	if err := chromedp.Run(ctx,
		chromedp.Click(transl1EditBtnSel),
		chromedp.WaitVisible(textarea1Sel),
		chromedp.SendKeys(textarea1Sel, kb.Control+kb.End+" и так далее"),
		chromedp.Click(submitBtn1Sel),
		chromedp.WaitNotPresent(textarea1Sel),
		chromedp.Evaluate(getTranslationRowsAsCSVJS, &text),
	); err != nil {
		t.Fatalf("could not edit translation version: %v", err)
	} else if want := "one,один и так далее\ntwo,\nthree,\n"; text != want {
		t.Fatalf("could not edit translation version: want %q, got %q", want, text)
	}

	if err := chromedp.Run(ctx,
		chromedp.Click(transl1EditBtnSel),
		chromedp.WaitVisible(textarea1Sel),
		chromedp.SendKeys(textarea1Sel, kb.Control+kb.End+" и тому подобное"),
		input.DispatchKeyEvent(input.KeyDown).WithModifiers(input.ModifierCtrl|input.ModifierShift).WithKey(kb.Enter).WithWindowsVirtualKeyCode(13),
		chromedp.WaitNotPresent(textarea1Sel),
		chromedp.Evaluate(getTranslationRowsAsCSVJS, &text),
	); err != nil {
		t.Fatalf("could not edit translation version: %v", err)
	} else if want := "one,один и так далее и тому подобное\ntwo,\nthree,\n"; text != want {
		t.Fatalf("could not edit translation version: want %q, got %q", want, text)
	}

	if got, err := httpGetAsString(location + "/export?f=csv"); err != nil {
		t.Fatalf("could not download CSV: %v", err)
	} else if want := "one,один и так далее и тому подобное\ntwo,\nthree,\n"; got != want {
		t.Fatalf("want CSV %q, got %q", want, got)
	}

	var filteredCount string
	if err := chromedp.Run(ctx,
		chromedp.Click(".filter-dropdown__button-toggle"),
		chromedp.Click(".filter-dropdown__show-untranslated"),
		chromedp.WaitReady(".translator"),
		chromedp.Evaluate(getTranslationRowsAsCSVJS, &text),
		chromedp.Text(".filter-dropdown__total-items", &filteredCount),
	); err != nil {
		t.Fatalf("untranslated filter didn't work: %v", err)
	} else if want := "two,\nthree,\n"; text != want {
		t.Fatalf("untranslated filter: want %q, got %q", want, text)
	}
	if filteredCount != "2" {
		t.Errorf("untranslated filter: want filtered count 2, got %s", filteredCount)
	}

	var found string
	var bgColor string
	if err := chromedp.Run(ctx,
		chromedp.Click(".filter-dropdown__button-toggle"),
		chromedp.Click("#orig_contains"),
		chromedp.SetValue(".filter-dropdown__orig-contains-input", "three"),
		chromedp.Click(".filter-dropdown__button-show"),
		chromedp.WaitVisible(".found"),
		chromedp.Text(".found", &found),
		chromedp.Evaluate(getTranslationRowsAsCSVJS, &text),
		chromedp.Text(".filter-dropdown__total-items", &filteredCount),
		chromedp.Evaluate("getComputedStyle(document.querySelector('.found')).backgroundColor", &bgColor),
	); err != nil {
		t.Fatalf("original contains filter didn't work: %v", err)
	} else if want := "three,\n"; text != want {
		t.Fatalf("original contains filter: want %q, got %q", want, text)
	} else if found != "three" {
		t.Fatalf("original contains filter: want mark \"three\", got %q", found)
	}
	if filteredCount != "1" {
		t.Errorf("original contains filter: want filtered count 1, got %s", filteredCount)
	}
	if want := "rgb(255, 255, 0)"; bgColor != want {
		t.Errorf("original contains filter: want mark to be colored %s, got %s", want, bgColor)
	}

	if err := chromedp.Run(ctx,
		chromedp.Click(".clear-filter-link"),
		chromedp.WaitNotPresent(".clear-filter-link"),

		chromedp.Click(".filter-dropdown__button-toggle"),
		chromedp.Click("#transl_contains"),
		chromedp.Sleep(1*time.Second),
		chromedp.SendKeys(".filter-dropdown__transl-contains-input", "и так далее"+kb.Enter),
		chromedp.Sleep(1*time.Second),
		chromedp.Text(".found", &found),
		chromedp.Evaluate(getTranslationRowsAsCSVJS, &text),
	); err != nil {
		t.Fatalf("translation contains filter didn't work: %v", err)
	} else if want := "one,один и так далее и тому подобное\n"; text != want {
		t.Fatalf("translation contains filter: want %q, got %q", want, text)
	} else if found != "и так далее" {
		t.Fatalf("translation contains filter: want mark \"и так далее\", got %q", found)
	}

	const toggleToolboxBtnSel = firstRowSel + " .action-toggle-toolbox"
	const editOrig1BtnSel = firstRowSel + " .button-edit-orig"
	const textareaOrig1Sel = firstRowSel + " .translator-column-orig textarea"
	const submitOrigBtn1Sel = firstRowSel + " .translator-column-orig form button[type='submit']"
	var taValue string
	if err := chromedp.Run(ctx,
		chromedp.Click(toggleToolboxBtnSel),
		chromedp.Click(editOrig1BtnSel),
		chromedp.WaitVisible(textareaOrig1Sel),
		chromedp.TextContent(textareaOrig1Sel, &taValue),
		chromedp.SetValue(textareaOrig1Sel, "one!11"),
		chromedp.Click(submitOrigBtn1Sel),
		chromedp.WaitNotPresent(textareaOrig1Sel),
		chromedp.Evaluate(getTranslationRowsAsCSVJS, &text),
	); err != nil {
		t.Fatalf("original editing didn't work: %v", err)
	} else if want := "one!11,один и так далее и тому подобное\n"; text != want {
		t.Fatalf("original editing: want %q, got %q", want, text)
	} else if taValue != "one" {
		t.Fatalf("original editing: want textarea to contain \"one\", got %q", taValue)
	}

	const secondRowSel = ".translator-row:nth-child(3)"
	const thirdRowSel = ".translator-row:nth-child(5)"
	const removeOrig2BtnSel = secondRowSel + " .button-remove-orig"
	if err := chromedp.Run(ctx,
		chromedp.Click(".clear-filter-link"),
		chromedp.WaitNotPresent(".clear-filter-link"),
		chromedp.Click(removeOrig2BtnSel),
		waitBSModalVisible(),
		chromedp.Click(".button-confirm-remove-orig"),
		chromedp.WaitNotPresent(thirdRowSel),
		chromedp.WaitNotPresent(".modal"),
		chromedp.Evaluate(getTranslationRowsAsCSVJS, &text),
	); err != nil {
		t.Fatalf("original removing didn't work: %v", err)
	} else if want := "one!11,один и так далее и тому подобное\nthree,\n"; text != want {
		t.Fatalf("original removing: want %q, got %q", want, text)
	}

	const addOrig1BtnSel = firstRowSel + " .button-add-orig"
	const textareaOrigSel = ".translator-column-orig textarea"
	const origDownBtn2Sel = secondRowSel + " .action-orig-down"
	if err := chromedp.Run(ctx,
		chromedp.Click(addOrig1BtnSel),
		chromedp.WaitVisible(textareaOrigSel),
		chromedp.SetValue(textareaOrigSel, "four4"),
		chromedp.Click(origDownBtn2Sel),
		chromedp.Focus(textareaOrigSel),
		input.DispatchKeyEvent(input.KeyDown).WithModifiers(input.ModifierCtrl).WithKey(kb.Enter).WithWindowsVirtualKeyCode(13),
		chromedp.WaitNotPresent(textareaOrigSel),
		chromedp.Evaluate(getTranslationRowsAsCSVJS, &text),
	); err != nil {
		t.Fatalf("original adding didn't work: %v", err)
	} else if want := "one!11,один и так далее и тому подобное\nthree,\nfour4,\n"; text != want {
		t.Fatalf("original adding: want %q, got %q", want, text)
	}

	const secondStarUnstarredSel = secondRowSel + " .fa-star-o"
	const secondStarStarredSel = secondRowSel + " .fa-star"
	if err := chromedp.Run(ctx,
		chromedp.Click(secondStarUnstarredSel),
		chromedp.WaitVisible(secondStarStarredSel),
		chromedp.Reload(),
		chromedp.Click(secondStarStarredSel),
		chromedp.WaitVisible(secondStarUnstarredSel),
		chromedp.Reload(),
		chromedp.WaitVisible(secondStarUnstarredSel),
	); err != nil {
		t.Fatalf("starring didn't work: %v", err)
	}

	const secondCommentBtnSel = secondRowSel + " .button-comment"
	var html string
	if err := chromedp.Run(ctx,
		chromedp.Click(secondCommentBtnSel+".fa-comment-o"),
		chromedp.WaitVisible(".commentary-form"),
		chromedp.SetValue(".commentary-form textarea", "*some* **markdown** ~~comment~~"),
		chromedp.Click(".commentary-form button[type='submit']"),
		chromedp.WaitNotPresent(".commentary-form textarea"),
		chromedp.WaitVisible(".commentary-form .text"),
		chromedp.InnerHTML(".commentary-form .text", &html),
		chromedp.Click(secondCommentBtnSel),
		chromedp.WaitNotVisible(".commentary-form textarea, .commentary-form .text"),
		chromedp.WaitVisible(secondCommentBtnSel+".fa-comment"),
	); err != nil {
		t.Fatalf("commenting didn't work: %v", err)
	} else if want := "<p><em>some</em> <strong>markdown</strong> <s>comment</s></p>\n"; html != want {
		t.Errorf("want comment to be %q, got %q", want, html)
	}

	if err := chromedp.Run(ctx,
		chromedp.Click(".filter-dropdown__button-toggle"),
		chromedp.Click(".filter-dropdown__show-commented"),
		chromedp.WaitVisible(".filter-dropdown__total-items"),
		chromedp.Evaluate(getTranslationRowsAsCSVJS, &text),
		chromedp.Text(".filter-dropdown__total-items", &filteredCount),
	); err != nil {
		t.Fatalf("commented filter didn't work: %v", err)
	} else if want := "three,\n"; text != want {
		t.Fatalf("commented filter: want %q, got %q", want, text)
	}
	if filteredCount != "1" {
		t.Errorf("commented filter: want filtered count 1, got %s", filteredCount)
	}

	if err := chromedp.Run(ctx,
		chromedp.Click(".clear-filter-link"),
		chromedp.WaitNotPresent(".clear-filter-link"),
		chromedp.Click(firstRowSel+" .fa-star-o"),
		chromedp.WaitVisible(firstRowSel+" .fa-star"),
		chromedp.Click(".filter-dropdown__button-toggle"),
		chromedp.Click(".filter-dropdown__show-starred"),
		chromedp.WaitVisible(".filter-dropdown__total-items"),
		chromedp.Text(".filter-dropdown__total-items", &filteredCount),
		chromedp.WaitVisible(firstRowSel),
		chromedp.Evaluate(getTranslationRowsAsCSVJS, &text),
	); err != nil {
		saveScreenshot(t, origCtx)
		t.Fatalf("starred filter didn't work: %v", err)
	} else if want := "one!11,один и так далее и тому подобное\n"; text != want {
		t.Fatalf("starred filter: want %q, got %q", want, text)
	}
	if filteredCount != "1" {
		t.Errorf("starred filter: want filtered count 1, got %s", filteredCount)
	}

	if err := chromedp.Run(ctx,
		chromedp.Click(firstRowSel+" .fa-star"),
		chromedp.WaitVisible(firstRowSel+" .fa-star-o"),
		chromedp.Click(".filter-dropdown__button-toggle"),
		chromedp.Click(".filter-dropdown__show-starred"),
		chromedp.WaitVisible(".alert-no-match"),
		chromedp.WaitVisible(".filter-dropdown__total-items"),
		chromedp.Text(".filter-dropdown__total-items", &filteredCount),
		chromedp.Evaluate(getTranslationRowsAsCSVJS, &text),
	); err != nil {
		saveScreenshot(t, origCtx)
		t.Fatalf("starred filter didn't work: %v", err)
	} else if want := ""; text != want {
		t.Fatalf("starred filter: want %q, got %q", want, text)
	}
	if filteredCount != "0" {
		t.Errorf("starred filter: want filtered count 0, got %s", filteredCount)
	}

	var origWords, origChars string
	var transWords, transChars string
	var transWordsPct, transCharsPct string
	if err := chromedp.Run(ctx,
		chromedp.Click(".button-tools"),
		chromedp.Click(".action-show-stats"),
		waitBSModalVisible(),
		chromedp.WaitVisible(".stats"),
		chromedp.Text(".stats__orig-words", &origWords),
		chromedp.Text(".stats__orig-chars", &origChars),
		chromedp.Text(".stats__trans-words", &transWords),
		chromedp.Text(".stats__trans-chars", &transChars),
		chromedp.Text(".stats__trans-words-pct", &transWordsPct),
		chromedp.Text(".stats__trans-chars-pct", &transCharsPct),
		chromedp.Click(".bootbox-close-button"),
		chromedp.WaitNotPresent(".modal"),
	); err != nil {
		t.Fatalf("statistics didn't work: %v", err)
	} else {
		const want = "3|7|233.33%\n16|32|200.00%"
		got := origWords + "|" + transWords + "|" + transWordsPct + "\n" +
			origChars + "|" + transChars + "|" + transCharsPct
		if got != want {
			t.Fatalf("statistics: want %q, got %q", want, got)
		}
	}

	if got, err := httpGetAsString(location + "/export?f=csv"); err != nil {
		t.Fatalf("could not download CSV: %v", err)
	} else if want := "one!11,один и так далее и тому подобное\nthree,\nfour4,\n"; got != want {
		t.Fatalf("want CSV %q, got %q", want, got)
	}

	var nfragments int
	var hlColor string
	if err := chromedp.Run(ctx,
		chromedp.Click(".translator__read-link"),
		chromedp.WaitVisible(".read"),
		chromedp.Text(".fragments", &text),
		chromedp.Evaluate("document.querySelectorAll('.read__fragment').length", &nfragments),
		chromedp.Click(".read__fragment:nth-child(3)"),
		chromedp.Click("#link-popper .fragment-permalink"),
		chromedp.WaitVisible(".translator"),
		chromedp.WaitVisible(".translator-row:nth-child(5).translator-row--highlighted"),
		chromedp.Evaluate("getComputedStyle(document.querySelector('.translator-row--highlighted')).backgroundColor", &hlColor),
	); err != nil {
		saveScreenshot(t, origCtx)
		t.Fatalf("read view didn't work: %v", err)
	} else if want := 3; nfragments != want {
		t.Fatalf("read view: want %d fragments, got %d", want, nfragments)
	} else if want := "один и так далее и тому подобное\n\nthree\n\nfour4"; text != want {
		t.Fatalf("read view: want text %q, got %q", want, text)
	} else if want := "rgb(255, 255, 178)"; hlColor != want {
		t.Fatalf("read view: want highlight color %q, got %q", want, hlColor)
	}

	var added, removed string
	var addedColor, removedColor string
	if err := chromedp.Run(ctx,
		chromedp.Click(".button-tools"),
		chromedp.Click(".action-search-and-replace"),
		waitBSModalVisible(),
		chromedp.WaitVisible(".search-and-replace-form"),
		chromedp.SetValue("#search", "один"),
		chromedp.SetValue("#replace-with", "два"),
		chromedp.Click(".button-preview-changes"),
		chromedp.WaitVisible(".snr-preview"),
		chromedp.WaitVisible(".diff"),
		chromedp.Text(".diff__added1", &added),
		chromedp.Text(".diff__removed1", &removed),
		chromedp.Evaluate("getComputedStyle(document.querySelector('.diff__added1')).backgroundColor", &addedColor),
		chromedp.Evaluate("getComputedStyle(document.querySelector('.diff__removed1')).backgroundColor", &removedColor),
		chromedp.Click(".button-replace"),
		chromedp.WaitVisible(firstRowSel),
		chromedp.Evaluate(getTranslationRowsAsCSVJS, &text),
	); err != nil {
		t.Fatalf("search-and-replace didn't work: %v", err)
	} else {
		if want := "два"; added != want {
			t.Errorf("added: want %q, got %q", want, added)
		}
		if want := "один"; removed != want {
			t.Errorf("removed: want %q, got %q", want, removed)
		}
		if want := "rgb(172, 242, 189)"; addedColor != want {
			t.Errorf("added color: want %q, got %q", want, addedColor)
		}
		if want := "rgb(253, 184, 192)"; removedColor != want {
			t.Errorf("removed color: want %q, got %q", want, removedColor)
		}
		if want := "one!11,два и так далее и тому подобное\nthree,\nfour4,\n"; text != want {
			t.Errorf("want replacement %q, got %q", want, text)
		}
	}

	var termColor string
	var term, def string
	var nterms int
	if err := chromedp.Run(ctx,
		chromedp.WaitVisible(".interactive"),
		chromedp.SendKeys("#panel-glossary__input-term", "one", chromedp.ByQuery),
		chromedp.SendKeys("#panel-glossary__input-def", "один"+kb.Enter, chromedp.ByQuery),
		chromedp.WaitVisible(".translator .glossary-term", chromedp.ByQuery),
		chromedp.Text(".translator .glossary-term", &text, chromedp.ByQuery),
		chromedp.Evaluate("getComputedStyle(document.querySelector('.translator .glossary-term')).color", &termColor),
		chromedp.Click(".glossary-link"),
		chromedp.WaitReady(".glossary"),
		chromedp.Evaluate("document.querySelectorAll('.glossary-row').length", &nterms),
		chromedp.Value(".glossary-row:first-child .glossary__input-term", &term),
		chromedp.Value(".glossary-row:first-child .glossary__input-def", &def),
	); err != nil {
		saveScreenshot(t, origCtx)
		t.Fatalf("adding to glossary didn't work: %v", err)
	}
	if want := "one"; text != want {
		t.Errorf("glossary term: want %q, got %q", want, text)
	}
	if want := "rgb(160, 82, 45)"; termColor != want {
		t.Errorf("glossary term color: want %q, got %q", want, termColor)
	}
	if nterms != 1 {
		t.Errorf("glossary: want 1 term, got %d", nterms)
	}
	if want := "one"; term != want {
		t.Errorf("glossary: want term to be %q, got %q", want, term)
	}
	if want := "один"; def != want {
		t.Errorf("glossary: want definition to be %q, got %q", want, def)
	}

	var markColor, markColor2 string
	var text2 string
	if err := chromedp.Run(ctx,
		chromedp.Navigate(location),
		chromedp.WaitVisible(".interactive"),
		selectTextRangeAction(firstRowSel+" .translator-column-transl .text", 0, 2),
		chromedp.Sleep(100*time.Millisecond), // wait for popper to be placed
		chromedp.Click(".button-mark"),
		chromedp.WaitVisible(".marked"),
		chromedp.Text(".marked", &text),
		chromedp.Evaluate("getComputedStyle(document.querySelector('.marked')).backgroundColor", &markColor),
		chromedp.Reload(),
		chromedp.WaitReady(".translator"),
		chromedp.Text(".marked", &text2),
		chromedp.Evaluate("getComputedStyle(document.querySelector('.marked')).backgroundColor", &markColor2),
	); err != nil {
		saveScreenshot(t, origCtx)
		t.Fatalf("marking didn't work: %v", err)
	}
	if want := "два"; text != want {
		t.Errorf("want marked text to be %q, got %q", want, text)
	}
	if want := "rgb(255, 165, 0)"; markColor != want {
		t.Errorf("want mark color %q, got %q", want, markColor)
	}
	if want := "два"; text2 != want {
		t.Errorf("want marked text after reload to be %q, got %q", want, text2)
	}
	if want := "rgb(255, 165, 0)"; markColor2 != want {
		t.Errorf("want mark color after reload to be %q, got %q", want, markColor2)
	}

	if err := chromedp.Run(ctx,
		page.SetDownloadBehavior(page.SetDownloadBehaviorBehaviorAllow).WithDownloadPath("testdata"),

		chromedp.Click(".button-export"),
		chromedp.Click(".dropdown-export__link-plaintext"),

		chromedp.Click(".button-export"),
		chromedp.Click(".dropdown-export__link-plaintext-orig"),

		chromedp.Click(".button-export"),
		chromedp.Click(".dropdown-export__link-csv"),

		chromedp.Click(".button-export"),
		chromedp.Click(".dropdown-export__link-jsonl"),

		chromedp.Click(".button-export"),
		chromedp.Click(".dropdown-export__link-json"),

		chromedp.Sleep(1*time.Second),
	); err != nil {
		saveScreenshot(t, origCtx)
		t.Fatalf("export didn't work: %v", err)
	}
	for _, fn := range []string{
		"book.txt", "book-orig.txt", "book.csv", "book.jsonl", "book.json",
	} {
		if !existsWithNonzeroSize("testdata/" + fn) {
			t.Errorf("could not download " + fn)
		}
	}

	abspath, _ := filepath.Abs("testdata/book.json")
	if err := chromedp.Run(ctx,
		chromedp.Navigate(rootURL),
		chromedp.Click(".button-add-book"),
		chromedp.Click(".link-json"),
		chromedp.WaitVisible(".form-add-book-json"),
		chromedp.SendKeys("#jsonfile", abspath),
		chromedp.Click(".form-add-book-json__button-submit"),
		chromedp.WaitReady(".translator"),
		chromedp.Text("h1", &title),
		chromedp.Evaluate(getTranslationRowsAsCSVJS, &text),
	); err != nil {
		saveScreenshot(t, origCtx)
		t.Fatalf("json import didn't work: %v", err)
	} else if want := "new title"; title != want {
		t.Fatalf("json import: want title %q, got %q", want, title)
	} else if want := "one!11,два и так далее и тому подобное\nthree,\nfour4,\n"; text != want {
		t.Fatalf("json import: want text %q, got %q", want, text)
	}

	abspath, _ = filepath.Abs("testdata/book.csv")
	if err := chromedp.Run(ctx,
		chromedp.Navigate(rootURL),
		chromedp.Click(".button-add-book"),
		chromedp.Click(".link-csv"),
		chromedp.WaitVisible(".form-add-book-csv"),
		chromedp.SetValue("#title", "brand new title"),
		chromedp.SendKeys("#csvfile", abspath),
		chromedp.Click(".form-add-book-csv__button-submit"),
		chromedp.WaitReady(".translator"),
		chromedp.Text("h1", &title),
		chromedp.Evaluate(getTranslationRowsAsCSVJS, &text),
	); err != nil {
		saveScreenshot(t, origCtx)
		t.Fatalf("csv import didn't work: %v", err)
	} else if want := "brand new title"; title != want {
		t.Fatalf("csv import: want title %q, got %q", want, title)
	} else if want := "one!11,два и так далее и тому подобное\nthree,\nfour4,\n"; text != want {
		t.Fatalf("csv import: want text %q, got %q", want, text)
	}
}

func existsWithNonzeroSize(fn string) bool {
	fi, err := os.Stat(fn)
	if err != nil {
		return false
	}
	return fi.Size() > 0
}

const getAlignerRowsAsCSVJS = `(function() {
	const rows = document.querySelectorAll('.aligner-row');
	let csv = '';
	for (let i = 0; i < rows.length; i++) {
		const row = rows[i];
		csv += row.querySelector('.aligner-column-left').textContent.trim() + ',' +
			row.querySelector('.aligner-column-right').textContent.trim() + '\n';
	}
	return csv;
})();`

func clickWithModifiers(sel interface{}, modifiers chromedp.MouseOption, opts ...chromedp.QueryOption) chromedp.QueryAction {
	return chromedp.QueryAfter(sel, func(ctx context.Context, _ runtime.ExecutionContextID, nodes ...*cdp.Node) error {
		if len(nodes) < 1 {
			return fmt.Errorf("selector %q did not return any nodes", sel)
		}

		return chromedp.MouseClickNode(nodes[0], modifiers).Do(ctx)
	}, append(opts, chromedp.NodeVisible)...)
}

func saveScreenshot(t *testing.T, ctx context.Context) {
	var pic []byte
	if err := chromedp.Run(ctx,
		chromedp.CaptureScreenshot(&pic),
	); err != nil {
		t.Errorf("could not save screenshot: %v", err)
	}
	if err := ioutil.WriteFile("screenshot.png", pic, 0o600); err != nil {
		t.Errorf("could not write file: %v", err)
	}
}

func testAligner(t *testing.T, ctx context.Context) {
	origCtx := ctx
	ctx, _ = context.WithTimeout(ctx, 20*time.Second)
	var text string
	if err := chromedp.Run(ctx,
		chromedp.Navigate(rootURL),

		chromedp.Click(".button-tools"),
		chromedp.Click(".button-aligner"),
		chromedp.WaitVisible("#left, #right, .button-align"),

		chromedp.SetValue("#left", "split1 split2"),
		chromedp.SetValue("#right", "edit\n\njoin1\n\njoin2\n\n"),
		chromedp.Click(".button-align"),
		chromedp.WaitVisible(".aligner-table"),
		chromedp.Evaluate(getAlignerRowsAsCSVJS, &text),
	); err != nil {
		saveScreenshot(t, origCtx)
		t.Fatalf("could not upload texts to aligner: %v", err)
	} else if want := "split1 split2,edit\n,join1\n,join2\n"; text != want {
		t.Fatalf("could not upload texts to aligner: want %q, got %q", want, text)
	}

	const split2SpanSel = ".aligner-row:first-child .aligner-column-left span:nth-child(2)"
	if err := chromedp.Run(ctx,
		chromedp.Click(split2SpanSel),
		chromedp.WaitReady(".aligner-row:nth-child(2) .aligner-column-left span"),
		chromedp.Evaluate(getAlignerRowsAsCSVJS, &text),
	); err != nil {
		t.Fatalf("split didn't work: %v", err)
	} else if want := "split1,edit\nsplit2,join1\n,join2\n"; text != want {
		t.Fatalf("split didn't work: want %q, got %q", want, text)
	}

	const join1SpanSel = ".aligner-row:nth-child(2) .aligner-column-right span"
	if err := chromedp.Run(ctx,
		clickWithModifiers(join1SpanSel, chromedp.ButtonModifiers(input.ModifierShift)),
		chromedp.WaitReady(".aligner-row:nth-child(2) .aligner-column-right span:nth-child(2)"),
		chromedp.Evaluate(getAlignerRowsAsCSVJS, &text),
	); err != nil {
		t.Fatalf("join didn't work: %v", err)
	} else if want := "split1,edit\nsplit2,join1 join2\n"; text != want {
		t.Fatalf("join didn't work: want %q, got %q", want, text)
	}

	const editSpanSel = ".aligner-row:first-child .aligner-column-right span"
	if err := chromedp.Run(ctx,
		clickWithModifiers(editSpanSel, chromedp.ButtonModifiers(input.ModifierCtrl)),
		chromedp.SetValue(".aligner-edit-textarea", "zzz"),
		chromedp.Click(".action-save-edit"),
		chromedp.WaitReady(".aligner-row:first-child .aligner-column-left span"),
		chromedp.Sleep(1*time.Second),
		chromedp.Evaluate(getAlignerRowsAsCSVJS, &text),
	); err != nil {
		t.Fatalf("edit didn't work: %v", err)
	} else if want := "split1,zzz\nsplit2,join1 join2\n"; text != want {
		t.Fatalf("edit didn't work: want %q, got %q", want, text)
	}

	if _, err := chromedp.RunResponse(ctx,
		chromedp.Click(".action-swap"),
	); err != nil {
		t.Fatalf("swap didn't work: %v", err)
	}

	if err := chromedp.Run(ctx,
		chromedp.WaitReady(".aligner-table, .aligner-row"),
		chromedp.Evaluate(getAlignerRowsAsCSVJS, &text),
	); err != nil {
		t.Fatalf("swap didn't work: %v", err)
	} else if want := "zzz,split1\njoin1 join2,split2\n"; text != want {
		t.Fatalf("swap didn't work: want %q, got %q", want, text)
	}

	if got, err := httpGetAsString(rootURL + "/aligner?download=csv"); err != nil {
		t.Fatalf("could not download CSV: %v", err)
	} else if want := "zzz,split1\njoin1 join2,split2\n"; got != want {
		t.Fatalf("want CSV %q, got %q", want, got)
	}

	if err := chromedp.Run(ctx,
		chromedp.Click(".aligner-row:first-child .aligner-row__icon-remove"),
		chromedp.WaitNotPresent(".aligner-row:nth-child(2)"),
		chromedp.Evaluate(getAlignerRowsAsCSVJS, &text),
	); err != nil {
		t.Fatalf("remove didn't work: %v", err)
	} else if want := "join1 join2,split2\n"; text != want {
		t.Fatalf("remove: want %q, got %q", want, text)
	}

	var title string
	if err := chromedp.Run(ctx,
		chromedp.Click(".action-import"),
		waitBSModalVisible(),
		chromedp.WaitVisible(".bootbox-input-text"),
		chromedp.SendKeys(".bootbox-input-text", "xxx"),
		chromedp.WaitEnabled(".bootbox-accept"),
		chromedp.Click(".bootbox-accept"),
		chromedp.WaitVisible(".translator"),
		chromedp.Evaluate(getTranslationRowsAsCSVJS, &text),
		chromedp.Text("h1", &title),
	); err != nil {
		t.Fatalf("import didn't work: %v", err)
	} else if want := "join1 join2,split2\n"; text != want {
		t.Fatalf("import: want %q, got %q", want, text)
	} else if want := "xxx"; title != want {
		t.Fatalf("import: want title %q, got %q", want, title)
	}
}

func fakeNotabenoidServer(w http.ResponseWriter, r *http.Request) {
	switch r.Method + " " + r.URL.Path {
	case "GET /book/1/1":
		fmt.Fprintln(w, `<table id="Tr">
<tr><td class="o"><p class="text">This is a</p></td>
<td class="t"><div><p class="text">test</p><p class="text">Yes, a test.</p></div></td></tr></table>`)
	case "GET /book/1/2":
		fmt.Fprintln(w, `<table id="Tr">
<tr><td class="o"><p class="text">This is a</p></td>
<td class="t"><div><p class="text">test</p><p class="text">Yes, a test. Updated.</p><p class="text">New translation version.</p></div></td></tr></table>`)
	case "POST /":
		if r.PostFormValue("login[login]") != "admin" ||
			r.PostFormValue("login[pass]") != "pass" {
			http.Error(w, "Not authorized", http.StatusUnauthorized)
			return
		}
		fmt.Fprintln(w, `<a href="/register/logout">`)
	}
}

func testNotabenoidInterop(t *testing.T, ctx context.Context) {
	origCtx := ctx
	nbRootURL := "http://localhost:1234"
	srv := http.Server{
		Addr:    ":1234",
		Handler: http.HandlerFunc(fakeNotabenoidServer),
	}
	defer srv.Shutdown(context.Background())
	go func() {
		if err := srv.ListenAndServe(); err != nil && err.Error() != "http: Server closed" {
			t.Fatal(err)
		}
	}()

	time.Sleep(1 * time.Second)

	ctx, _ = context.WithTimeout(ctx, 10*time.Second)

	// Import
	var location string
	var title, text string
	if err := chromedp.Run(ctx,
		chromedp.Navigate(rootURL),
		chromedp.Click(".button-add-book"),
		chromedp.Click(".link-notabenoid"),
		chromedp.WaitVisible(".form-add-book-notabenoid"),
		chromedp.SetValue("#title", "from notabenoid"),
		chromedp.SetValue("#chapter-url", nbRootURL+"/book/1/1"),
		chromedp.SetValue("#nb-user", "admin"),
		chromedp.SetValue("#nb-pass", "pass"),
		chromedp.Click(".form-add-book-notabenoid__button-import"),
		chromedp.WaitVisible(".translator"),
		chromedp.Text("h1", &title),
		chromedp.Evaluate(getTranslationRowsAsCSVJS, &text),
		chromedp.Location(&location),
	); err != nil {
		saveScreenshot(t, origCtx)
		t.Fatalf("import from notabenoid didn't work: %v", err)
	} else if want := "from notabenoid"; title != want {
		t.Fatalf("import from notabenoid: want title %q, got %q", want, title)
	} else if want := "This is a,test\tYes, a test.\n"; text != want {
		t.Fatalf("import from notabenoid: want translation %q, got %q", want, text)
	}

	// Compare
	var added1, added0 string
	var colorAdded0, colorAdded1 string
	if err := chromedp.Run(ctx,
		chromedp.Click(".button-tools"),
		chromedp.Click(".action-compare-nb"),
		waitBSModalVisible(),
		chromedp.SetValue("#chapter-url", nbRootURL+"/book/1/2"),
		chromedp.SetValue("#nb-user", "admin"),
		chromedp.SetValue("#nb-pass", "pass"),
		chromedp.Click("#nb-newer"),
		chromedp.Click(".compare-to-nb-form__button-compare"),
		chromedp.Click(".compare-to-nb-form__link-differences"),
		chromedp.WaitVisible(".diff", chromedp.ByQuery),
		chromedp.Text(".diff-row:first-child .diff__added1", &added1, chromedp.ByQuery),
		chromedp.Text(".diff-row:nth-child(2) .diff__added0", &added0, chromedp.ByQuery),
		chromedp.Evaluate("getComputedStyle(document.querySelector('.diff__added0')).backgroundColor", &colorAdded0),
		chromedp.Evaluate("getComputedStyle(document.querySelector('.diff__added1')).backgroundColor", &colorAdded1),
	); err != nil {
		saveScreenshot(t, origCtx)
		t.Fatalf("compare to notabenoid didn't work: %#v", err)
	} else if want := " Updated."; added1 != want {
		t.Fatalf("compare to notabenoid: want added %q, got %q", want, added1)
	} else if want := "New translation version."; added0 != want {
		t.Fatalf("compare to notabenoid: want added %q, got %q", want, added0)
	} else if want := "rgb(214, 255, 226)"; colorAdded0 != want {
		t.Errorf("compare to notabenoid: want first color to be %q, got %q", want, colorAdded0)
	} else if want := "rgb(172, 242, 189)"; colorAdded1 != want {
		t.Errorf("compare to notabenoid: want second color to be %q, got %q", want, colorAdded1)
	}

	// Update, but don't add new versions.
	if err := chromedp.Run(ctx,
		chromedp.Navigate(location),
		chromedp.Click(".button-tools"),
		chromedp.Click(".action-update-nb"),
		waitBSModalVisible(),
		chromedp.SetValue("#chapter-url", nbRootURL+"/book/1/2"),
		chromedp.SetValue("#nb-user", "admin"),
		chromedp.SetValue("#nb-pass", "pass"),
		chromedp.Click("#no-new"),
		chromedp.Click(".update-from-nb-form__button-update"),
		chromedp.WaitNotPresent(".modal"),
		chromedp.WaitReady(".translator"),
		chromedp.Evaluate(getTranslationRowsAsCSVJS, &text),
	); err != nil {
		saveScreenshot(t, origCtx)
		t.Fatalf("update from notabenoid didn't work: %v", err)
	} else if want := "This is a,test\tYes, a test. Updated.\n"; text != want {
		t.Fatalf("update from notabenoid: want translation %q, got %q", want, text)
	}

	// Update and add new versions.
	if err := chromedp.Run(ctx,
		chromedp.Click(".button-tools"),
		chromedp.Click(".action-update-nb"),
		waitBSModalVisible(),
		chromedp.SetValue("#chapter-url", nbRootURL+"/book/1/2"),
		chromedp.SetValue("#nb-user", "admin"),
		chromedp.SetValue("#nb-pass", "pass"),
		chromedp.Click(".update-from-nb-form__button-update"),
		chromedp.WaitNotPresent(".modal"),
		chromedp.WaitReady(".translator"),
		chromedp.Evaluate(getTranslationRowsAsCSVJS, &text),
	); err != nil {
		saveScreenshot(t, origCtx)
		t.Fatalf("update from notabenoid didn't work: %v", err)
	} else if want := "This is a,test\tYes, a test. Updated.\tNew translation version.\n"; text != want {
		t.Fatalf("update from notabenoid: want translation %q, got %q", want, text)
	}
}

func toJSON(v interface{}) string {
	var b strings.Builder
	json.NewEncoder(&b).Encode(v)
	return b.String()
}

func TestBasicFunctionality(t *testing.T) {
	os.Args = []string{"tl", "-db", testDB}
	go main()
	defer os.Remove(testDB)

	time.Sleep(1 * time.Second)

	ctx, cancel := chromedp.NewContext(
		context.Background(),
		chromedp.WithLogf(t.Logf),
	)
	defer cancel()

	var exceptions []*runtime.ExceptionDetails
	var errors []*log.Entry
	chromedp.ListenTarget(ctx, func(ev interface{}) {
		switch ev := ev.(type) {
		case *log.EventEntryAdded:
			if ev.Entry.Level == log.LevelError {
				errors = append(errors, ev.Entry)
			}
		case *runtime.EventExceptionThrown:
			exceptions = append(exceptions, ev.ExceptionDetails)
		case *runtime.EventConsoleAPICalled:
			fmt.Printf("console.%s:\n", ev.Type)
			for _, arg := range ev.Args {
				fmt.Printf("  - %s: %s\n", arg.Type, arg.Value)
			}
		}
	})

	// No timeout for the first Run. See https://github.com/chromedp/chromedp/issues/513
	if err := chromedp.Run(ctx,
		emulation.SetDeviceMetricsOverride(1338+1, 600, 1.0, false), // see css/my-new.css
	); err != nil {
		t.Fatalf("could not start the browser: %v", err)
	}

	defer func() {
		tempfiles, _ := filepath.Glob("testdata/*")
		for _, fn := range tempfiles {
			if fn == "testdata/.gitkeep" {
				continue
			}
			if err := os.Remove(fn); err != nil {
				t.Log("could not remove " + fn)
			}
		}
	}()

	testTranslator(t, ctx)
	testAligner(t, ctx)
	testNotabenoidInterop(t, ctx)

	if len(exceptions) > 0 {
		t.Error("There were exceptions:")
		for _, e := range exceptions {
			t.Error(toJSON(e))
		}
	}

	if len(errors) > 0 {
		t.Error("There were errors:")
		for _, e := range errors {
			t.Error(toJSON(e))
		}
	}
}
