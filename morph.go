// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"archive/tar"
	"compress/gzip"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strings"

	"gitlab.com/opennota/morph"
)

const pymorphy2DictsRuURL = "https://pypi.org/project/pymorphy2-dicts-ru/#files"

func downloadDictionaries(dataDir string) error {
	log.Println("downloading", pymorphy2DictsRuURL)

	resp, err := http.Get(pymorphy2DictsRuURL)
	if err != nil {
		return fmt.Errorf("failed to get %s: %w", pymorphy2DictsRuURL, err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		return fmt.Errorf("failed to get %s: HTTP error code %d", pymorphy2DictsRuURL, resp.StatusCode)
	}

	html, err := io.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("failed to read %s: %w", pymorphy2DictsRuURL, err)
	}
	m := regexp.MustCompile(`href="([^"]+/pymorphy2-dicts-ru-[0-9.]+\.tar.gz)"`).FindStringSubmatch(string(html))
	if m == nil {
		return fmt.Errorf("failed to get a download link")
	}

	log.Println("downloading", m[1])

	resp, err = http.Get(m[1])
	if err != nil {
		return fmt.Errorf("failed to get %s: %w", m[1], err)
	}
	defer resp.Body.Close()

	filename := path.Base(m[1])

	gzr, err := gzip.NewReader(resp.Body)
	if err != nil {
		return fmt.Errorf("failed to decompress %s: %w", filename, err)
	}
	defer gzr.Close()

	r := tar.NewReader(gzr)
	for {
		header, err := r.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			return fmt.Errorf("failed to decompress %s: %w", filename, err)
		}
		if header.Typeflag != tar.TypeReg || !strings.Contains(header.Name, "/data/") {
			continue
		}
		outputFilename := filepath.Join(dataDir, path.Base(header.Name))
		outf, err := os.Create(outputFilename)
		if err != nil {
			return fmt.Errorf("failed to create %s: %w", outputFilename, err)
		}
		if _, err := io.Copy(outf, r); err != nil { //nolint:gosec
			outf.Close()
			return fmt.Errorf("failed to write %s: %w", outputFilename, err)
		}
		if err := outf.Close(); err != nil {
			return fmt.Errorf("failed to write %s: %w", outputFilename, err)
		}
	}

	log.Println("the dictionaries have been saved to", dataDir)

	return nil
}

func initializeMorph() error {
	cacheDir, err := os.UserCacheDir()
	if err != nil {
		return fmt.Errorf("failed to get user cache directory: %w", err)
	}
	dataDir := filepath.Join(cacheDir, "pymorphy2-dicts-ru", "data")
	if err := morph.InitWith(dataDir); err != nil {
		if err := os.MkdirAll(dataDir, 0o700); err != nil {
			return fmt.Errorf("failed to create directories: %w", err)
		} else if err := downloadDictionaries(dataDir); err != nil {
			return fmt.Errorf("failed to download dictionaries: %w", err)
		} else if err := morph.InitWith(dataDir); err != nil {
			return fmt.Errorf("failed to load dictionary data: %w", err)
		}
	}
	return nil
}
