// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"fmt"
	"html"
	"html/template"
	"regexp"
	"sort"
	"strings"
	"time"
)

var (
	funcs = template.FuncMap{
		"requirejs_path":      func() string { return "/js/lib/require.js" },
		"pct":                 pct,
		"pct6":                pct6,
		"float2":              float2,
		"pretty":              pretty,
		"rfc3339":             rfc3339,
		"dateStr":             dateStr,
		"datetimeStr":         datetimeStr,
		"dec":                 dec,
		"inc":                 inc,
		"max":                 max,
		"seq":                 seq,
		"weekSpan":            weekSpan,
		"render":              render,
		"renderhl":            renderhl,
		"renderWithMarks":     renderWithMarks,
		"renderWithRegexp":    renderWithRegexp,
		"renderUsingGlossary": renderUsingGlossary,
	}
	indexTmpl, addTmpl, bookTmpl, readTmpl, glossaryTmpl, alignerTmpl, snrTmpl, concordanceTmpl, heatmapTmpl, fresheyeTmpl, analysisTmpl *template.Template

	rBigWords = regexp.MustCompile(`[^\s<>&;]{32,}`)
	r16Chars  = regexp.MustCompile(`.{16}`)
)

func mustLookup(t *template.Template, name string) *template.Template {
	name += ".html"
	tmpl := t.Lookup(name)
	if tmpl == nil {
		panic(fmt.Sprintf("template %s is not found", name))
	}
	return tmpl
}

func mustParseTemplates(fsys *FS) {
	modulePaths := template.JS(fsys.JSModulePaths()) //nolint:gosec
	funcs["requirejs_module_paths"] = func() template.JS { return modulePaths }
	funcs["static_path"] = fsys.Map

	tmpl, err := template.New("base").Funcs(funcs).ParseFS(fsys, "template/*.html")
	if err != nil {
		panic(fmt.Sprintf("failed to parse templates: %v", err))
	}

	indexTmpl = mustLookup(tmpl, "index")
	addTmpl = mustLookup(tmpl, "add")
	bookTmpl = mustLookup(tmpl, "book")
	readTmpl = mustLookup(tmpl, "read")
	glossaryTmpl = mustLookup(tmpl, "glossary")
	alignerTmpl = mustLookup(tmpl, "aligner")
	snrTmpl = mustLookup(tmpl, "snr")
	concordanceTmpl = mustLookup(tmpl, "concordance")
	heatmapTmpl = mustLookup(tmpl, "heatmap")
	fresheyeTmpl = mustLookup(tmpl, "fresheye")
	analysisTmpl = mustLookup(tmpl, "analysis")
}

func dec(a int) int { return a - 1 }

func inc(a int) int { return a + 1 }

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func seq(n int) []int {
	a := make([]int, n)
	for i := 0; i < n; i++ {
		a[i] = i
	}
	return a
}

func pct(a, b int) int {
	if b == 0 {
		return 0
	}
	return 100 * a / b
}

func pct6(a, b int) string {
	if b == 0 {
		return ""
	}
	return fmt.Sprintf("%.6g", 100*float64(a)/float64(b))
}

func float2(f float64) string { return fmt.Sprintf("%.2f", f) }

func rfc3339(t time.Time) string {
	return t.Format(time.RFC3339)
}

func dateStr(t time.Time) string {
	return t.Format("02-Jan-2006")
}

func datetimeStr(t time.Time) string {
	return t.Format("02-Jan-2006 15:04:05")
}

func pretty(t time.Time) string {
	seconds := time.Since(t).Nanoseconds() / 1e9
	days := seconds / (60 * 60 * 24)
	switch {
	case days < 0:
		return "somewhen in the future"
	case days == 0:
		if seconds < 60*60 {
			minutes := seconds / 60
			switch minutes {
			case 0:
				return "just now"
			case 1:
				return "1 minute ago"
			default:
				return fmt.Sprintf("%d minutes ago", minutes)
			}
		}
		hours := seconds / (60 * 60)
		if hours == 1 {
			return "1 hour ago"
		}
		return fmt.Sprintf("%d hours ago", hours)
	case days < 7:
		if days == 1 {
			return "Yesterday"
		}
		return fmt.Sprintf("%d days ago", days)
	case days < 31:
		weeks := days / 7
		if weeks == 1 {
			return "1 week ago"
		}
		return fmt.Sprintf("%d weeks ago", weeks)
	default:
		return datetimeStr(t)
	}
}

func weekSpan(year int, m int) int {
	d := time.Date(year, time.Month(m), 1, 0, 0, 0, 0, time.UTC)
	y1, w1 := d.ISOWeek()
	y2, w2 := d.AddDate(0, 1, -1).ISOWeek()
	if y1 < year {
		// Jan 01 to Jan 03 of year n might belong to week 52 or 53 of year n-1.
		w1 = 0
	}
	if y2 > year {
		// Dec 29 to Dec 31 might belong to week 1 of year n+1.
		// This is December. Get 28th.
		_, w2 = d.AddDate(0, 0, 27).ISOWeek()
		w2++
	}
	if _, w3 := d.AddDate(0, 1, 0).ISOWeek(); w3 == w2 {
		// If the next month starts at the same week, don't count it.
		return w2 - w1
	}
	return w2 - w1 + 1
}

func insertSoftBreaks(s string) string {
	return rBigWords.ReplaceAllStringFunc(s, func(s string) string {
		return r16Chars.ReplaceAllStringFunc(s, func(s string) string {
			return s + "<wbr>"
		})
	})
}

var nl2br = strings.NewReplacer("\n", "<br>\n")

func render(s string) template.HTML {
	s = html.EscapeString(s)
	s = nl2br.Replace(s)
	s = insertSoftBreaks(s)
	return template.HTML(s) //nolint:gosec
}

var rRegexpWithModifiers = regexp.MustCompile(`^/.*/[imsU]{0,4}$`)

func renderhl(s, what string) template.HTML {
	var r *regexp.Regexp
	if rRegexpWithModifiers.MatchString(what) {
		i := strings.LastIndexByte(what, '/')
		x := what[1:i]
		if i != len(what)-1 {
			x = "(?" + what[i+1:] + ")" + x
		}
		r, _ = regexp.Compile(x)
	}
	if r == nil {
		r = regexp.MustCompile("(?i)" + regexp.QuoteMeta(what))
	}
	idxs := r.FindAllStringIndex(s, -1)
	i := 0
	var buf strings.Builder
	for _, idx := range idxs {
		from, to := idx[0], idx[1]
		buf.WriteString(html.EscapeString(s[i:from]))
		buf.WriteString(`<mark class="found">`)
		buf.WriteString(html.EscapeString(s[from:to]))
		buf.WriteString("</mark>")
		i = to
	}
	buf.WriteString(html.EscapeString(s[i:]))
	s = nl2br.Replace(buf.String())
	s = insertSoftBreaks(s)
	return template.HTML(s) //nolint:gosec
}

func renderWithRegexp(s string, r *regexp.Regexp) template.HTML {
	idxs := r.FindAllStringIndex(s, -1)
	i := 0
	var buf strings.Builder
	for _, idx := range idxs {
		from, to := idx[0], idx[1]
		buf.WriteString(html.EscapeString(s[i:from]))
		buf.WriteString(`<mark class="found">`)
		buf.WriteString(html.EscapeString(s[from:to]))
		buf.WriteString("</mark>")
		i = to
	}
	buf.WriteString(html.EscapeString(s[i:]))
	s = nl2br.Replace(buf.String())
	s = insertSoftBreaks(s)
	return template.HTML(s) //nolint:gosec
}

func renderWithMarks(s string, marks []string) template.HTML {
	if len(marks) == 0 {
		return render(s)
	}
	mq := make([]string, len(marks))
	copy(mq, marks)
	sort.Slice(mq, func(i, j int) bool {
		if ll := len(mq[i]) - len(mq[j]); ll != 0 {
			return ll > 0
		}
		return mq[i] < mq[j]
	})
	for i, m := range mq {
		mq[i] = regexp.QuoteMeta(m)
	}
	r := regexp.MustCompile("(?i)" + strings.Join(mq, "|"))
	idxs := r.FindAllStringIndex(s, -1)
	i := 0
	var buf strings.Builder
	for _, idx := range idxs {
		from, to := idx[0], idx[1]
		buf.WriteString(html.EscapeString(s[i:from]))
		buf.WriteString(`<mark class="marked">`)
		buf.WriteString(html.EscapeString(s[from:to]))
		buf.WriteString("</mark>")
		i = to
	}
	buf.WriteString(html.EscapeString(s[i:]))
	s = nl2br.Replace(buf.String())
	s = insertSoftBreaks(s)
	return template.HTML(s) //nolint:gosec
}

func renderUsingGlossary(s string, r *regexp.Regexp, glossary Glossary) template.HTML {
	if r == nil {
		return render(s)
	}
	idxs := r.FindAllStringIndex(s, -1)
	i := 0
	var buf strings.Builder
	for _, idx := range idxs {
		from, to := idx[0], idx[1]
		term := s[from:to]
		def := glossary[term].Def
		if def == "" {
			def = glossary[strings.ToLower(term)].Def
		}
		buf.WriteString(html.EscapeString(s[i:from]))
		buf.WriteString(`<span class="glossary-term" title="`)
		buf.WriteString(html.EscapeString(def))
		buf.WriteString(`">`)
		buf.WriteString(html.EscapeString(term))
		buf.WriteString("</span>")
		i = to
	}
	buf.WriteString(html.EscapeString(s[i:]))
	s = nl2br.Replace(buf.String())
	s = insertSoftBreaks(s)
	return template.HTML(s) //nolint:gosec
}
